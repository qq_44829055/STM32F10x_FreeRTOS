/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

#include "port.h"
#include "../modbus/include/mbport.h"


/* ----------------------- Start implementation -----------------------------*/
__weak BOOL WHT_MB_Port_Serial_Init(void)
{
    //由用户自行配置串口 一般情况下 115200 8 N 1
    return FALSE;
}
//缓存发送
__weak void WHT_FreeModBus_Printf(const uint8_t* tx_buf, uint16_t length)
{
    //由用户自行提供
}
//缓存接收
__weak uint16_t WHT_FreeModBus_Scanf(uint8_t* rx_buf, uint16_t max_length)
{
    //由用户自行提供
    return 0;
}
