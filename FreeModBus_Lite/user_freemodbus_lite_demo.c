/*
 * FreeModbus Libary: user callback functions and buffer define in slave mode
 * Copyright (C) 2013 Armink <armink.ztl@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: user_mb_app.c,v 1.60 2013/11/23 11:49:05 Armink $
 */
#include "user_freemodbus_lite_demo.h"

/*------------------------Slave mode use these variables----------------------*/
//Slave mode:DiscreteInputs variables
/* 离散输入变量 */
#if S_DISCRETE_INPUT_NDISCRETES_Number%8
uint8_t WHT_SDiscIn_Buffer[S_DISCRETE_INPUT_NDISCRETES_Number/8+1];
#else
uint8_t WHT_SDiscIn_Buffer[S_DISCRETE_INPUT_NDISCRETES_Number/8]  ;
#endif

//Slave mode:Coils variables
/* 线圈 */
#if S_COIL_NCOILS_Number%8
uint8_t WHT_SCoil_Buffer[S_COIL_NCOILS_Number/8+1];
#else
uint8_t WHT_SCoil_Buffer[S_COIL_NCOILS_Number/8];
#endif

//Slave mode:InputRegister variables
/* 输入寄存器 */
uint16_t WHT_SRegIn_Buffer[S_REG_INPUT_NREGS_Count];

//Slave mode:HoldingRegister variables
/* 保持寄存器 */
uint16_t WHT_SRegHold_Buffer[S_REG_HOLDING_NREGS_Count];

/**
 * Modbus slave input register callback function.
 *
 * @param pucRegBuffer input register buffer
 * @param usAddress input register address
 * @param usNRegs input register number
 *
 * @return result
 */
eMBErrorCode WHT_MB_RegInput_Callback(unsigned char* pucRegBuffer, unsigned short usAddress, unsigned short usNRegs)
{
    eMBErrorCode eStatus = MB_ENOERR;
    unsigned short iRegIndex;

    /* it already plus one in modbus function method. */
    usAddress--;
    if ((usAddress >= S_REG_Input_Start_Pos) && (usAddress + usNRegs <= S_REG_Input_Start_Pos + S_REG_INPUT_NREGS_Count))
    {
        iRegIndex = usAddress - S_REG_Input_Start_Pos;
        while (usNRegs > 0)
        {
            *pucRegBuffer++ = (unsigned char) (WHT_SRegIn_Buffer[iRegIndex] >> 8);
            *pucRegBuffer++ = (unsigned char) (WHT_SRegIn_Buffer[iRegIndex] & 0xFF);
            iRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

/**
 * Modbus slave holding register callback function.
 *
 * @param pucRegBuffer holding register buffer
 * @param usAddress holding register address
 * @param usNRegs holding register number
 * @param eMode read or write
 *
 * @return result
 */
eMBErrorCode WHT_MB_RegHolding_Callback(unsigned char* pucRegBuffer, unsigned short usAddress, unsigned short usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    unsigned short iRegIndex;

    /* it already plus one in modbus function method. */
    usAddress--;
    if ((usAddress >= S_REG_HOLDING_START_Pos) && (usAddress + usNRegs <= S_REG_HOLDING_START_Pos + S_REG_HOLDING_NREGS_Count))
    {
        iRegIndex = usAddress - S_REG_HOLDING_START_Pos;
        switch (eMode)
        {
        /* read current register values from the protocol stack. */
        case MB_REG_READ:
            while (usNRegs > 0)
            {
                *pucRegBuffer++ = (unsigned char) (WHT_SRegHold_Buffer[iRegIndex] >> 8);
                *pucRegBuffer++ = (unsigned char) (WHT_SRegHold_Buffer[iRegIndex] & 0xFF);
                iRegIndex++;
                usNRegs--;
            }
            break;
        /* write current register values with new values from the protocol stack. */
        case MB_REG_WRITE:
            while (usNRegs > 0)
            {
                WHT_SRegHold_Buffer[iRegIndex] = *pucRegBuffer++ << 8;
                WHT_SRegHold_Buffer[iRegIndex] |= *pucRegBuffer++;
                iRegIndex++;
                usNRegs--;
            }
            break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

/**
 * Modbus slave coils callback function.
 *
 * @param pucRegBuffer coils buffer
 * @param usAddress coils address
 * @param usNCoils coils number
 * @param eMode read or write
 *
 * @return result
 */
eMBErrorCode WHT_MB_RegCoils_Callback(unsigned char* pucRegBuffer, unsigned short usAddress, unsigned short usNCoils, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    unsigned short iRegIndex , iRegBitIndex , iNReg;

    iNReg =  usNCoils / 8 + 1;
    /* it already plus one in modbus function method. */
    usAddress--;
    if((usAddress >= S_COIL_START_Pos) && (usAddress + usNCoils <= S_COIL_START_Pos + S_COIL_NCOILS_Number))
    {
        iRegIndex = (unsigned short) (usAddress - S_COIL_START_Pos) / 8;
        iRegBitIndex = (unsigned short) (usAddress - S_COIL_START_Pos) % 8;
        switch ( eMode )
        {
        /* read current coil values from the protocol stack. */
        case MB_REG_READ:
            while (iNReg > 0)
            {
                *pucRegBuffer++ = WHT_MB_Util_Get_Bits(&WHT_SCoil_Buffer[iRegIndex++], iRegBitIndex, 8);
                iNReg--;
            }
            pucRegBuffer--;
            /* last coils */
            usNCoils = usNCoils % 8;
            /* filling zero to high bit */
            *pucRegBuffer = *pucRegBuffer << (8 - usNCoils);
            *pucRegBuffer = *pucRegBuffer >> (8 - usNCoils);
            break;
            /* write current coil values with new values from the protocol stack. */
        case MB_REG_WRITE:
            while (iNReg > 1)
            {
                WHT_MB_Util_Set_Bits(&WHT_SCoil_Buffer[iRegIndex++], iRegBitIndex, 8, *pucRegBuffer++);
                iNReg--;
            }
            /* last coils */
            usNCoils = usNCoils % 8;
            /* WHT_MB_Util_Set_Bits has bug when ucNBits is zero */
            if (usNCoils != 0)
            {
                WHT_MB_Util_Set_Bits(&WHT_SCoil_Buffer[iRegIndex++], iRegBitIndex, usNCoils, *pucRegBuffer++);
            }
            break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }
    return eStatus;
}

/**
 * Modbus slave discrete callback function.
 *
 * @param pucRegBuffer discrete buffer
 * @param usAddress discrete address
 * @param usNDiscrete discrete number
 *
 * @return result
 */
eMBErrorCode WHT_MB_RegDiscrete_Callback(unsigned char* pucRegBuffer, unsigned short usAddress, unsigned short usNDiscrete)
{
    eMBErrorCode eStatus = MB_ENOERR;
    unsigned short iRegIndex , iRegBitIndex , iNReg;

    iNReg =  usNDiscrete / 8 + 1;
    /* it already plus one in modbus function method. */
    usAddress--;
    if ((usAddress >= S_DISCRETE_INPUT_START_Pos) && (usAddress + usNDiscrete <= S_DISCRETE_INPUT_START_Pos + S_DISCRETE_INPUT_NDISCRETES_Number))
    {
        iRegIndex = (unsigned short) (usAddress - S_DISCRETE_INPUT_START_Pos) / 8;
        iRegBitIndex = (unsigned short) (usAddress - S_DISCRETE_INPUT_START_Pos) % 8;

        while (iNReg > 0)
        {
            *pucRegBuffer++ = WHT_MB_Util_Get_Bits(&WHT_SDiscIn_Buffer[iRegIndex++], iRegBitIndex, 8);
            iNReg--;
        }
        pucRegBuffer--;
        /* last discrete */
        usNDiscrete = usNDiscrete % 8;
        /* filling zero to high bit */
        *pucRegBuffer = *pucRegBuffer << (8 - usNDiscrete);
        *pucRegBuffer = *pucRegBuffer >> (8 - usNDiscrete);
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}


__weak void WHT_FreeModBus_Lite_Init(eMBMode eMode,  uint8_t Slave_Address)
{
	/* ModBus初始化 */
    WHT_MB_Init(eMode, Slave_Address);
}
__weak void WHT_FreeModBus_Lite_Poll(void)
{
    /* ModBus无限轮询*/
    WHT_MB_Poll();
}
