#ifndef __USER_FREEMODBUS_LITE_DEMO_H__
#define __USER_FREEMODBUS_LITE_DEMO_H__


/* ----------------------- Modbus includes ----------------------------------*/
#include "./modbus/include/mb.h"
#include "./modbus/include/mbconfig.h"
#include "./modbus/include/mbframe.h"
#include "./modbus/include/mbutils.h"

/* -----------------------Slave Defines -------------------------------------*/
/*
功能码01  读线圈输出状态
功能码    1 个字节 0x01
起始地址  2 个字节 0x0000 至 0xFFFF
线圈数量  2 个字节 1 至 2000（0x7D0）
错误：
功能码 1 个字节 功能码＋0x80
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：读线圈输出状态20~38
发送
功能码       0x01
起始地址 Hi  0x00
起始地址 Lo  0x13
输出数量 Hi  0x00
输出数量 Lo  0x13
*/

/*
功能码02  读线圈输入状态
功能码    1 个字节 0x02
起始地址  2 个字节 0x0000 至 0xFFFF
输入数量  2 个字节 1 至 2000（0x7D0）
错误：
差错码 1 字节 0x82
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：读线圈输入状态197~218
发送
功能码       0x02
起始地址 Hi  0x00
起始地址 Lo  0xC4
输出数量 Hi  0x00
输出数量 Lo  0x16
*/

/*
功能码03    读保持寄存器
功能码      1 个字节 0x03
起始地址    2 个字节 0x0000 至 0xFFFF
寄存器数量  2 个字节 1 至 125（0x7D）
错误：
差错码 1 个字节 0x83
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：读保持寄存器108~110
功能码        0x03
高起始地址    0x00
低起始地址    0x6B
高寄存器编号  0x00
低寄存器编号  0x03
*/

/*
功能码04        读输入寄存器
功能码          1 个字节 0x04
起始地址        2 个字节 0x0000 至 0xFFFF
输入寄存器数量  2 个字节 0x0001 至 0x007D
错误：
差错码 1 个字节 0x84
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：读保持寄存器9
功能码            0x04
起始地址 Hi       0x00
起始地址 Lo       0x08
输入寄存器数量 Hi  0x00
输入寄存器数量 Lo  0x01
*/

/*
功能码05    写单个线圈
功能码      1 个字节 0x05
输出地址    2 个字节 0x0000 至 0xFFFF
输出值      2 个字节 0x0000 至 0x00
错误：
差错码 1 个字节 0x85
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：写线圈173
功能码        0x05
输出地址 Hi   0x00
输出地址 Lo   0xAC
输出值 Hi     0xFF
输出值 Lo     0x00
*/

/*
功能码06    写单个寄存器
功能码      1 个字节 0x06
寄存器地址  2 个字节 0x0000 至 0xFFFF
寄存器值    2 个字节 0x0000 至 0xFFFF
错误：
差错码 1 个字节 0x86
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：写入单个寄存器
功能码          0x06
寄存器地址 Hi   0x00
寄存器地址 Lo   0x01
寄存器值 Hi     0x00
寄存器值 Lo     0x03
*/

/*
功能码0F   写多个线圈
功能码     1 个字节 0x0F
起始地址   2 个字节 0x0000 至 0xFFFF
输出数量   2 个字节 0x0001 至 0x07B0
字节数     1 个字节 N*
输出值     N*×1 个字节
错误：
差错码 1 个字节 0x8F
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：写多个线圈从20开始 写入10个线圈
功能码         0x0F
起始地址 Hi    0x00
起始地址 Lo    0x13
输出数量 Hi    0x00
输出数量 Lo    0x0A
字节数         0x02
输出值 Hi      0xCD
输出值 Lo      0x01
*/

/*
功能码10    写多个寄存器
功能码      1 个字节 0x10
起始地址    2 个字节 0x0000 至 0xFFFF
寄存器数量  2 个字节 0x0001 至 0x0078
字节数      1 个字节 2×N
寄存器值    N*×2 个字节 值
错误：
差错码 1 个字节 0x90
异常码 1 个字节 01（不支持此功能码） 或 02（访问地址错误） 或 03（访问数量错误） 或 04（设备故障）

示例：写2个寄存器
功能码           0x10
起始地址 Hi      0x00
起始地址 Lo      0x01
寄存器数量 Hi    0x00
寄存器数量 Lo    0x02
字节数           0x04
寄存器值 Hi      0x00
寄存器值 Lo      0x0A
寄存器值 Hi      0x01
寄存器值 Lo      0x02
*/

/*
功能码14 读文件记录
功能码15 写文件记录
功能码16 屏蔽写寄存器
功能码17 读和写多个寄存器
功能码2B 读设备识别码

*/

/* 测试功能参数 */
#define MB_SAMPLE_TEST_SLAVE_ADDR		          1	 //从机设备地址

#define S_DISCRETE_INPUT_START_Pos                0  //离散输入起始位置
#define S_DISCRETE_INPUT_NDISCRETES_Number        16 //离散输入位个数
#define S_COIL_START_Pos                          0  //线圈输出起始位置
#define S_COIL_NCOILS_Number                      64 //线圈个数
#define S_REG_Input_Start_Pos                     0  //输入寄存器起始位置
#define S_REG_INPUT_NREGS_Count                   100//输入寄存器个数
#define S_REG_HOLDING_START_Pos                   1  //保持寄存器起始位置
#define S_REG_HOLDING_NREGS_Count                 100//保持寄存器个数


/* 更新保持寄存器值 */
extern uint16_t WHT_SRegHold_Buffer[];
/* 更新输入寄存器值 */
extern uint16_t WHT_SRegIn_Buffer[];
/* 更新线圈 */
extern uint8_t WHT_SCoil_Buffer[];
/* 离散输入变量 */
extern uint8_t WHT_SDiscIn_Buffer[];

extern void WHT_FreeModBus_Lite_Init(eMBMode eMode,  uint8_t Slave_Address);
extern void WHT_FreeModBus_Lite_Poll(void);

#endif
