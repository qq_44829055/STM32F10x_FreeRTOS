/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

/* ----------------------- Platform includes --------------------------------*/
#include "../../port/port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "../include/mb.h"
#include "../include/mbport.h"
#include "../include/mbframe.h"
#include "mbrtu.h"
#include "mbcrc.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_SER_PDU_SIZE_MIN     4       /*!< Minimum size of a Modbus RTU frame. */
#define MB_SER_PDU_SIZE_MAX     256     /*!< Maximum size of a Modbus RTU frame. */
#define MB_SER_PDU_SIZE_CRC     2       /*!< Size of CRC field in PDU. */
#define MB_SER_PDU_ADDR_OFF     0       /*!< Offset of slave address in Ser-PDU. */
#define MB_SER_PDU_PDU_OFF      1       /*!< Offset of Modbus-PDU in Ser-PDU. */


/* ----------------------- Static variables ---------------------------------*/
uint8_t  WHT_RTU_Buffer[MB_SER_PDU_SIZE_MAX];

/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode WHT_MB_RTU_Init(uint8_t ucSlaveAddress)
{
    eMBErrorCode    eStatus = MB_ENOERR;

    /* Modbus RTU uses 8 Databits. */
    if(WHT_MB_Port_Serial_Init() != TRUE )
    {
        eStatus = MB_EPORTERR;
    }
    return eStatus;
}
eMBErrorCode WHT_MB_RTU_Receive( uint8_t * pucRcvAddress, uint8_t ** pucFrame, uint16_t * pusLength )
{
    uint16_t usRcvBufferPos;
    eMBErrorCode    eStatus = MB_ENOERR;

    usRcvBufferPos = WHT_FreeModBus_Scanf(WHT_RTU_Buffer, sizeof(WHT_RTU_Buffer));
    assert( usRcvBufferPos < MB_SER_PDU_SIZE_MAX );

    /* Length and CRC check */
    if( (usRcvBufferPos >= MB_SER_PDU_SIZE_MIN ) && (WHT_MB_CRC16(WHT_RTU_Buffer, usRcvBufferPos) == 0))
    {
        /* Save the address field. All frames are passed to the upper layed
         * and the decision if a frame is used is done there.
         */
        *pucRcvAddress = WHT_RTU_Buffer[MB_SER_PDU_ADDR_OFF];

        /* Total length of Modbus-PDU is Modbus-Serial-Line-PDU minus
         * size of address field and CRC checksum.
         */
        *pusLength = ( uint16_t )( usRcvBufferPos - MB_SER_PDU_PDU_OFF - MB_SER_PDU_SIZE_CRC );

        /* Return the start of the Modbus PDU to the caller. */
        *pucFrame = ( uint8_t * ) & WHT_RTU_Buffer[MB_SER_PDU_PDU_OFF];
    }
    else
    {
        printf("MB RTU CRC ERROR!!!\r\n");
        printf("CRC=0x%02x\r\n", WHT_MB_CRC16(WHT_RTU_Buffer, usRcvBufferPos - MB_SER_PDU_SIZE_CRC));
        eStatus = MB_EIO;
    }
    return eStatus;
}
eMBErrorCode WHT_MB_RTU_Send( uint8_t ucSlaveAddress, const uint8_t * pucFrame, uint16_t usLength )
{
    volatile uint8_t *pucSndBufferCur;
    uint16_t usSndBufferCount;
    uint16_t          usCRC16;

    /* First byte before the Modbus-PDU is the slave address. */
    pucSndBufferCur = ( uint8_t * ) pucFrame - 1;
    usSndBufferCount = 1;

    /* Now copy the Modbus-PDU into the Modbus-Serial-Line-PDU. */
    pucSndBufferCur[MB_SER_PDU_ADDR_OFF] = ucSlaveAddress;
    usSndBufferCount += usLength;

    /* Calculate CRC16 checksum for Modbus-Serial-Line-PDU. */
    usCRC16 = WHT_MB_CRC16( ( uint8_t * ) pucSndBufferCur, usSndBufferCount );
    WHT_RTU_Buffer[usSndBufferCount++] = ( uint8_t )( usCRC16 & 0xFF );
    WHT_RTU_Buffer[usSndBufferCount++] = ( uint8_t )( usCRC16 >> 8 );

    /* Activate the transmitter. */
    WHT_FreeModBus_Printf((uint8_t*)pucSndBufferCur, usSndBufferCount);
    return MB_ENOERR;
}
