/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"

/* ----------------------- Platform includes --------------------------------*/
#include "../port/port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "./include/mb.h"
#include "./include/mbconfig.h"
#include "./include/mbframe.h"
#include "./include/mbproto.h"
#include "./include/mbfunc.h"

#include "./include/mbport.h"
#if MB_RTU_ENABLED == 1
#include "./rtu/mbrtu.h"
#endif
#if MB_ASCII_ENABLED == 1
#include "./ascii/mbascii.h"
#endif
#if MB_TCP_ENABLED == 1
#include "./tcp/mbtcp.h"
#endif

#ifndef MB_PORT_HAS_CLOSE
#define MB_PORT_HAS_CLOSE 0
#endif

/* ----------------------- Static variables ---------------------------------*/

static uint8_t WHT_MB_Addr;
static eMBMode WHT_MB_Work_Mode;

/* Functions pointer which are initialized in WHT_MB_Init( ). Depending on the
 * mode (RTU or ASCII) the are set to the correct implementations.
 */
static WHT_MB_Send_Buffer_t WHT_MB_Send_Buffer;
static WHT_MB_Receive_Buffer_t WHT_MB_Receive_Buffer;


/* An array of Modbus functions handlers which associates Modbus function
 * codes with implementing functions.
 */
static xMBFunctionHandler WHT_Func_Handlers[MB_FUNC_HANDLERS_MAX] =
{
#if MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0
    {MB_FUNC_OTHER_REPORT_SLAVEID, WHT_MB_Func_Report_Slave_ID},
#endif
#if MB_FUNC_READ_INPUT_ENABLED > 0
    {MB_FUNC_READ_INPUT_REGISTER, WHT_MB_Func_Read_Input_Register},
#endif
#if MB_FUNC_READ_HOLDING_ENABLED > 0
    {MB_FUNC_READ_HOLDING_REGISTER, WHT_MB_Func_Read_Holding_Register},
#endif
#if MB_FUNC_WRITE_MULTIPLE_HOLDING_ENABLED > 0
    {MB_FUNC_WRITE_MULTIPLE_REGISTERS, WHT_MB_Func_Write_Multiple_Holding_Register},
#endif
#if MB_FUNC_WRITE_HOLDING_ENABLED > 0
    {MB_FUNC_WRITE_REGISTER, WHT_MB_Func_Write_Holding_Register},
#endif
#if MB_FUNC_READWRITE_HOLDING_ENABLED > 0
    {MB_FUNC_READWRITE_MULTIPLE_REGISTERS, WHT_MB_Func_Read_Write_Multiple_Holding_Register},
#endif
#if MB_FUNC_READ_COILS_ENABLED > 0
    {MB_FUNC_READ_COILS, WHT_MB_Func_Read_Coils},
#endif
#if MB_FUNC_WRITE_COIL_ENABLED > 0
    {MB_FUNC_WRITE_SINGLE_COIL, WHT_MB_Func_Write_Coil},
#endif
#if MB_FUNC_WRITE_MULTIPLE_COILS_ENABLED > 0
    {MB_FUNC_WRITE_MULTIPLE_COILS, WHT_MB_Func_Write_Multiple_Coils},
#endif
#if MB_FUNC_READ_DISCRETE_INPUTS_ENABLED > 0
    {MB_FUNC_READ_DISCRETE_INPUTS, WHT_MB_Func_Read_Discrete_Inputs},
#endif
};

/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode WHT_MB_Init(eMBMode eMode, uint8_t Slave_Address)
{
    eMBErrorCode eStatus = MB_ENOERR;

    /* check preconditions */
    if((Slave_Address == MB_ADDRESS_BROADCAST) || (Slave_Address < MB_ADDRESS_MIN) || (Slave_Address > MB_ADDRESS_MAX))
    {
        eStatus = MB_EINVAL;
    }
    else
    {
        WHT_MB_Addr = Slave_Address;
        switch ( eMode )
        {
#if MB_RTU_ENABLED > 0
        case MB_RTU:
            WHT_MB_Send_Buffer = WHT_MB_RTU_Send;
            WHT_MB_Receive_Buffer = WHT_MB_RTU_Receive;
            eStatus = WHT_MB_RTU_Init(WHT_MB_Addr);
            break;
#endif
#if MB_ASCII_ENABLED > 0
        case MB_ASCII:
            WHT_MB_Send_Buffer = WHT_MB_ASCII_Send;
            WHT_MB_Receive_Buffer = WHT_MB_ASCII_Receive;
            eStatus = WHT_MB_ASCII_Init(WHT_MB_Addr);
            break;
#endif
        default:
            eStatus = MB_EINVAL;
        }
    }
    if( eStatus == MB_ENOERR )
    {
        WHT_MB_Work_Mode = eMode;
    }
    return eStatus;
}

#if MB_TCP_ENABLED > 0
eMBErrorCode WHT_MB_TCP_Init(uint16_t ucTCPPort)
{
    eMBErrorCode eStatus = MB_ENOERR;

    if((eStatus = eMBTCPDoInit( ucTCPPort )) != MB_ENOERR)
    {
    }
    else
    {
        //////////pvMBFrameStopCur = eMBTCPStop;
        WHT_MB_Receive_Buffer = eMBTCPReceive;
        WHT_MB_Send_Buffer = eMBTCPSend;
        WHT_MB_Addr = MB_TCP_PSEUDO_ADDRESS;
        WHT_MB_Work_Mode = MB_TCP;
    }
    return eStatus;
}
#endif

/*手动注册相关功能码与解析函数*/
eMBErrorCode WHT_MB_Register_Callback(uint8_t ucFunctionCode, WHT_MB_Function_Handler_t pxHandler)
{
    eMBErrorCode eStatus;
    uint8_t i;

    if((0 < ucFunctionCode) && (ucFunctionCode <= 127))
    {
        if( pxHandler != NULL )//注册功能码与解析函数
        {
            for(i = 0; i < MB_FUNC_HANDLERS_MAX; i++)//最大 MB_FUNC_HANDLERS_MAX 个功能码支持
            {
                if( (WHT_Func_Handlers[i].pxHandler == NULL ) || (WHT_Func_Handlers[i].pxHandler == pxHandler ) )
                {
                    WHT_Func_Handlers[i].ucFunctionCode = ucFunctionCode;
                    WHT_Func_Handlers[i].pxHandler = pxHandler;
                    break;
                }
            }
            eStatus = ( i != MB_FUNC_HANDLERS_MAX ) ? MB_ENOERR : MB_ENORES;
        }
        else//注销功能码与解析函数
        {
            for(i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
            {
                if(WHT_Func_Handlers[i].ucFunctionCode == ucFunctionCode )
                {
                    WHT_Func_Handlers[i].ucFunctionCode = 0;
                    WHT_Func_Handlers[i].pxHandler = NULL;
                    break;
                }
            }
            eStatus = MB_ENOERR;
        }
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    return eStatus;
}

eMBErrorCode WHT_MB_Poll( void )
{
    static uint8_t   *ucMBFrame;     //缓存
    static uint8_t    ucRcvAddress;  //地址
    static uint8_t    ucFunctionCode;//功能码
    static uint16_t   usLength;      //缓存长度
    static eMBException eException;
    eMBErrorCode    eStatus = MB_ENOERR;

    eStatus = WHT_MB_Receive_Buffer(&ucRcvAddress, &ucMBFrame, &usLength);//非阻塞获取缓存数据//得到有效缓存数据
    if(eStatus == MB_ENOERR)//开始解析和处理
    {
        /* 检查是否自己的地址，是否为广播地址 */
        if((ucRcvAddress == WHT_MB_Addr) || (ucRcvAddress == MB_ADDRESS_BROADCAST))
        {
            ucFunctionCode = ucMBFrame[MB_PDU_FUNC_OFF];
            eException = MB_EX_ILLEGAL_FUNCTION;
            for(uint8_t i = 0; i < MB_FUNC_HANDLERS_MAX; i++ )//检查功能码是否有支持
            {
                /* 无功能码支持 */
                if(WHT_Func_Handlers[i].ucFunctionCode == 0 )
                {
                    break;
                }
                else if(WHT_Func_Handlers[i].ucFunctionCode == ucFunctionCode )//有功能吗并且找到有对应的功能码
                {
                    eException = WHT_Func_Handlers[i].pxHandler(ucMBFrame, &usLength);//负载与负载长度的解析处理
                    break;
                }
            }

            /* If the request was not sent to the broadcast address we
             * return a reply. */
            if( ucRcvAddress != MB_ADDRESS_BROADCAST )//如果不是广播数据
            {
                if( eException != MB_EX_NONE )//解析错误
                {
                    /* An exception occured. Build an error frame. */
                    usLength = 0;
                    ucMBFrame[usLength++] = ( uint8_t )( ucFunctionCode | MB_FUNC_ERROR );
                    ucMBFrame[usLength++] = eException;
                }                
                eStatus = WHT_MB_Send_Buffer(WHT_MB_Addr, ucMBFrame, usLength);//发送报文
            }
        }
    }
    return MB_ENOERR;
}
