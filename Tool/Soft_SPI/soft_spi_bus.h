#ifndef __SOFT_SPI_BUS_H__
#define __SOFT_SPI_BUS_H__

#include "soft_SPI_bsp.h"

extern WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS1;//总线1
extern WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS2;//总线2
extern WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS3;//总线3
extern char WHT_Soft_SPI_BUS1_Init(void);//返回值非0错误
extern char WHT_Soft_SPI_BUS2_Init(void);//返回值非0错误
extern char WHT_Soft_SPI_BUS3_Init(void);//返回值非0错误

#endif // !__SOFT_SPI_BUS_H__
