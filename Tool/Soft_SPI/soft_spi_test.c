//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/*
    这个是一个简单的SPI读写测试示例接口。
*/
#include "soft_spi.h"
#include "stdio.h"

#include "../../User/BSP/GPIO_BSP/gpio_bsp.h"


static void WHT_SPI_Write(unsigned char* buffer, unsigned char length)
{
    while (WHT_Cache.Info->Mutex_State == Soft_SPI_Lock)
    {
        //vtaskdelay(1);
    }
    WHT_Cache.Info->Mutex_State = Soft_SPI_Lock;
    WHT_Cache.TRx_Count = length;
    WHT_Cache.Tx_Buffer = buffer;
    WHT_Cache.Rx_Buffer = NULL;//是可以支持全双工 这里先只写吧
    WHT_Cache.Mode = Soft_SPI_WO;
    WHT_Soft_SPI_Handle.WHT_Send_Receive(&WHT_Cache);
}
static void WHT_SPI_Read(unsigned char* buffer, unsigned char length)
{
    while (WHT_Cache.Info->Mutex_State == Soft_SPI_Lock)
    {
        //vtaskdelay(1);
    }
    WHT_Cache.Info->Mutex_State = Soft_SPI_Lock;
    WHT_Cache.Info->Mutex_State = Soft_SPI_Lock;
    WHT_Cache.TRx_Count = length;
    WHT_Cache.Tx_Buffer = NULL;//是可以支持全双工 这里先只写吧
    WHT_Cache.Rx_Buffer = buffer;
    WHT_Cache.Mode = Soft_SPI_RO;
    WHT_Soft_SPI_Handle.WHT_Send_Receive(&WHT_Cache);
}


void WHT_Test_main(void)
{
    static unsigned char Buffer[16];
    WHT_Soft_SPI_Init();
    WHT_Delay_ms(500);
    for (unsigned char i = 0; i < sizeof(Buffer) / 2; i++)
    {
        Buffer[i*2] = i;
    }
    //1 <= WHT_Config.Bit_Wide <= 8bit下面要除以1
    //9 <= WHT_Config.Bit_Wide <= 16bit下面要除以2
    //17 <= WHT_Config.Bit_Wide <= 32bit下面要除以4
    WHT_SPI_Write(Buffer, sizeof(Buffer) / 2);//本人使用逻辑分析仪直接看了
}
