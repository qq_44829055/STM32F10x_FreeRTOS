//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.2.0
/********************************************************
    说明：
    1、先注册个总线，最大3个。每个总线上可挂多个SPI设备。每个总线的速率可动态调整。(挂在一起的设备引脚必须一样)
    2、每个外挂的SPI设备自行提供收发缓存和片选。
    3、每个总线均会自动提供给SPI实时状态和互斥锁状态。
    4、只支持普通收发数据。
    5、只支持Master模式。
    6、由于MCU的IO反转速率很慢，会导致设置的波特率达不到预期，用户自行提高设置的波特率值。
**********************************************************/

#ifndef __SOFT_SPI_BSP_H__
#define __SOFT_SPI_BSP_H__

#define WHT_Soft_SPI1_Name    "WHT_Soft_SPI1"
#define WHT_Soft_SPI2_Name    "WHT_Soft_SPI2"
#define WHT_Soft_SPI3_Name    "WHT_Soft_SPI3"

/*软SPI发送或接收枚举*/
typedef enum
{
    Soft_SPI_WO = 0,//只发送模式
    Soft_SPI_RO = 1,//只接收模式
    Soft_SPI_RW = 2,//收发模式
}WHT_Soft_SPI_Mode_enum;

/*软SPI状态枚举*/
typedef enum
{
    Soft_SPI_No_Error  = 0,//无错误码
    Soft_SPI_BUS_Error = 1,//总线错误
}WHT_Soft_SPI_BUS_State_enum;

/*软SPI锁枚举*/
typedef enum
{
    Soft_SPI_Unlock = 0,
    Soft_SPI_Lock   = 1,
}WHT_Soft_SPI_BUS_Lock_enum;

/*软SPI IO状态枚举*/
typedef enum
{
    Soft_SPI_IO_Low = 0,
    Soft_SPI_IO_Hig = 1,
}WHT_Sort_SPI_BUS_IO_State_enum;

/*软SPI IO选择枚举*/
typedef enum
{
    Soft_SPI_SCLK = 0,
    Soft_SPI_MOSI,
    Soft_SPI_MISO,
}WHT_Soft_SPI_BUS_GPIO_enum;

/*软SPI 工作模式枚举*/
typedef enum
{
    Sort_SPI_Work_Mode0 = 0,
    Sort_SPI_Work_Mode1 = 1,
    Sort_SPI_Work_Mode2 = 2,
    Sort_SPI_Work_Mode3 = 3,
}WHT_Soft_SPI_BUS_Work_Mode_enum;

/*软SPI总线配置结构体*/
typedef struct
{
    unsigned int Sys_Rate_Hz;//系统频率
    unsigned int SPI_Rate_Hz;//SPI频率
    WHT_Soft_SPI_BUS_Work_Mode_enum Work_Mode;//工作模式
    unsigned char MSB_LSB;   //0=MSB 1=LSB，当前MSB不支持修改
    unsigned char Bit_Wide;  //1~32bit
}WHT_Soft_SPI_BUS_Config_t;

/*软SPI总线结构体*/
typedef struct
{
    char* Name;                             //总线名字
    WHT_Soft_SPI_BUS_Config_t Config;       //总线配置信息
    const WHT_Soft_SPI_BUS_Lock_enum Mutex; //驱动锁,自动上锁与解锁
    const WHT_Soft_SPI_BUS_State_enum State;//错误标志,自动清零
}WHT_Soft_SPI_BUS_t;

/*软SPI缓存结构体*/
typedef struct
{
    WHT_Soft_SPI_Mode_enum Dir;//读写方向
    unsigned int Buffer_Count; //个数
    unsigned char* Tx_Buffer;  //缓存
    unsigned char* Rx_Buffer;  //接收缓存，不足8bit按8bit，不足16bit按16bit，不足32bit按32bit
    void (*Set_NSS_State_CB)(WHT_Sort_SPI_BUS_IO_State_enum state);
}WHT_Soft_SPI_Cache_t;

/*软SPI回调函数结构体*/
typedef struct
{
    WHT_Soft_SPI_BUS_t* (*Register)(char* name, WHT_Soft_SPI_BUS_Config_t* config, void (*gpio_init_callback)(void),//引脚初始化 SCL=PP CS=PP MOSI=PP MISO=IPU 速率建议最高
                                    void (*gpio_set_callback)(WHT_Soft_SPI_BUS_GPIO_enum io, WHT_Sort_SPI_BUS_IO_State_enum state),
                                    WHT_Sort_SPI_BUS_IO_State_enum (*gpio_get_callback)(WHT_Soft_SPI_BUS_GPIO_enum io));
    void (*Unregister)(WHT_Soft_SPI_BUS_t* bus);
    void (*Set_Rate)(WHT_Soft_SPI_BUS_t* bus);
    void (*Read_Write)(WHT_Soft_SPI_BUS_t* bus, WHT_Soft_SPI_Cache_t* cache);
}WHT_Soft_SPI_BUS_OPS_t;

/*全局常量*/
extern const WHT_Soft_SPI_BUS_OPS_t WHT_Soft_SPI_BUS_OPS;

#endif // !__SOFT_SPI_BSP_H__
