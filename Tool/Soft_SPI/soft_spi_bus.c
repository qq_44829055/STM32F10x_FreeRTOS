//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/*
    这个是一个示例总线驱动。
    可以删减总线个数和调整总线GPIO来适配硬件板子。
*/
#include "soft_spi_bus.h"
#include "../../User/BSP/GPIO_BSP/gpio_bsp.h"


WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS1;//总线1
WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS2;//总线2
WHT_Soft_SPI_BUS_t* WHT_Soft_SPI_BUS3;//总线3

static const WHT_Soft_SPI_BUS_Config_t WHT_Soft_SPI_BUS_Config =
{
    .Sys_Rate_Hz = 72000000,
    .SPI_Rate_Hz = 2000000,
    .Work_Mode   = Sort_SPI_Work_Mode0,
    .MSB_LSB     = 0,
    .Bit_Wide    = 21,
};

//引脚初始化 SCL=PP MOSI=PP MISO=IPU 速率建议最高
static volatile unsigned int* WHT_SPI_BUS1_GPIO_STATE[3];
static volatile unsigned int* WHT_SPI_BUS2_GPIO_STATE[3];
static volatile unsigned int* WHT_SPI_BUS3_GPIO_STATE[3];

static void WHT_SPI_BUS1_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortE, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortE, Pin0, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortE, Pin2, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortE, Pin3, Mode_IPU);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortE, Pin0, &WHT_SPI_BUS1_GPIO_STATE[0]);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortE, Pin2, &WHT_SPI_BUS1_GPIO_STATE[1]);
    WHT_GPIO_BSP.WHT_Config_Bit_Input(PortE, Pin3, &WHT_SPI_BUS1_GPIO_STATE[2]);

}
static void WHT_SPI_BUS2_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortA, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin4, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin5, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin6, Mode_IPU);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortA, Pin4, &WHT_SPI_BUS2_GPIO_STATE[0]);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortA, Pin5, &WHT_SPI_BUS2_GPIO_STATE[1]);
    WHT_GPIO_BSP.WHT_Config_Bit_Input(PortA, Pin6, &WHT_SPI_BUS2_GPIO_STATE[2]);
}
static void WHT_SPI_BUS3_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortA, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin4, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin5, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin6, Mode_IPU);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortA, Pin4, &WHT_SPI_BUS3_GPIO_STATE[0]);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(PortA, Pin5, &WHT_SPI_BUS3_GPIO_STATE[1]);
    WHT_GPIO_BSP.WHT_Config_Bit_Input(PortA, Pin6, &WHT_SPI_BUS3_GPIO_STATE[2]);
}

static void WHT_SPI_BUS1_GPIO_Set_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io, WHT_Sort_SPI_BUS_IO_State_enum state)
{
    if (io == Soft_SPI_SCLK)
        *WHT_SPI_BUS1_GPIO_STATE[0] = (WHT_GPIO_State_enum)state;
    else
        *WHT_SPI_BUS1_GPIO_STATE[1] = (WHT_GPIO_State_enum)state;
}
static void WHT_SPI_BUS2_GPIO_Set_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io, WHT_Sort_SPI_BUS_IO_State_enum state)
{
    if (io == Soft_SPI_SCLK)
        *WHT_SPI_BUS2_GPIO_STATE[0] = (WHT_GPIO_State_enum)state;
    else
        *WHT_SPI_BUS2_GPIO_STATE[1] = (WHT_GPIO_State_enum)state;
}
static void WHT_SPI_BUS3_GPIO_Set_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io, WHT_Sort_SPI_BUS_IO_State_enum state)
{
    if (io == Soft_SPI_SCLK)
        *WHT_SPI_BUS3_GPIO_STATE[0] = (WHT_GPIO_State_enum)state;
    else
        *WHT_SPI_BUS3_GPIO_STATE[1] = (WHT_GPIO_State_enum)state;
}

static WHT_Sort_SPI_BUS_IO_State_enum WHT_SPI_BUS1_GPIO_Get_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io)
{
    if (io == Soft_SPI_MISO)
        return (WHT_Sort_SPI_BUS_IO_State_enum)*WHT_SPI_BUS1_GPIO_STATE[2];
    else
        return Soft_SPI_IO_Hig;
}
static WHT_Sort_SPI_BUS_IO_State_enum WHT_SPI_BUS2_GPIO_Get_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io)
{
    if (io == Soft_SPI_MISO)
        return (WHT_Sort_SPI_BUS_IO_State_enum)*WHT_SPI_BUS2_GPIO_STATE[2];
    else
        return Soft_SPI_IO_Hig;
}
static WHT_Sort_SPI_BUS_IO_State_enum WHT_SPI_BUS3_GPIO_Get_State_Callback(WHT_Soft_SPI_BUS_GPIO_enum io)
{
    if (io == Soft_SPI_MISO)
        return (WHT_Sort_SPI_BUS_IO_State_enum)*WHT_SPI_BUS3_GPIO_STATE[2];
    else
        return Soft_SPI_IO_Hig;
}



char WHT_Soft_SPI_BUS1_Init(void)
{
    WHT_Soft_SPI_BUS1 = WHT_Soft_SPI_BUS_OPS.Register(WHT_Soft_SPI1_Name, (WHT_Soft_SPI_BUS_Config_t*)&WHT_Soft_SPI_BUS_Config, WHT_SPI_BUS1_GPIO_Init_Callback,
                                                        WHT_SPI_BUS1_GPIO_Set_State_Callback, WHT_SPI_BUS1_GPIO_Get_State_Callback);
    return WHT_Soft_SPI_BUS1 == (void*)0 ? -1 : 0;
}
char WHT_Soft_SPI_BUS2_Init(void)
{
    WHT_Soft_SPI_BUS2 = WHT_Soft_SPI_BUS_OPS.Register(WHT_Soft_SPI2_Name, (WHT_Soft_SPI_BUS_Config_t*)&WHT_Soft_SPI_BUS_Config, WHT_SPI_BUS2_GPIO_Init_Callback,
                                                        WHT_SPI_BUS2_GPIO_Set_State_Callback, WHT_SPI_BUS2_GPIO_Get_State_Callback);
    return WHT_Soft_SPI_BUS2 == (void*)0 ? -1 : 0;
}
char WHT_Soft_SPI_BUS3_Init(void)
{
    WHT_Soft_SPI_BUS3 = WHT_Soft_SPI_BUS_OPS.Register(WHT_Soft_SPI3_Name, (WHT_Soft_SPI_BUS_Config_t*)&WHT_Soft_SPI_BUS_Config, WHT_SPI_BUS3_GPIO_Init_Callback,
                                                        WHT_SPI_BUS3_GPIO_Set_State_Callback, WHT_SPI_BUS3_GPIO_Get_State_Callback);
    return WHT_Soft_SPI_BUS3 == (void*)0 ? -1 : 0;
}
