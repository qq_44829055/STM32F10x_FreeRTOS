//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V1.0.0
#include "search.h"


#define Search_Binary(Func_Name, Buffer_Type)                                                                         \
unsigned int Search_Binary_##Func_Name(const Buffer_Type* buffer, unsigned int element_count, Buffer_Type search_data)\
{                                                                       \
	unsigned int Min_Pos;                                               \
	unsigned int Mid_Pos;                                               \
	unsigned int Max_Pos;                                               \
                                                                        \
    Min_Pos = 0;                                                        \
    Max_Pos = element_count - 1;                                        \
    Mid_Pos = Max_Pos >> 1;                                             \
    do                                                                  \
    {                                                                   \
		if(search_data >= buffer[Mid_Pos])                              \
        {                                                               \
            Min_Pos = Mid_Pos;                                          \
        }                                                               \
		else                                                            \
        {                                                               \
            Max_Pos = Mid_Pos;                                          \
        }                                                               \
        Mid_Pos = Min_Pos + ((Max_Pos - Min_Pos) >> 1);                 \
                                                                        \
		if(Min_Pos == Mid_Pos)                                          \
		{                                                               \
            return search_data >= buffer[Max_Pos] ? Max_Pos : Min_Pos;  \
		}                                                               \
    } while (1);                                                        \
    return (unsigned int)-1;                                            \
}

/*二分查找法 int8_t*/
Search_Binary(int8_t, char)
/*二分查找法 int16_t*/
Search_Binary(int16_t, short)
/*二分查找法 int32_t*/
Search_Binary(int32_t, int)
/*二分查找法 float*/
Search_Binary(float, float)
/*二分查找法 uint8_t*/
Search_Binary(uint8_t, unsigned char)
/*二分查找法 uint16_t*/
Search_Binary(uint16_t, unsigned short)
/*二分查找法 uint32_t*/
Search_Binary(uint32_t, unsigned int)

/*此函数用于线性变化如温度等*/
#define Search_Binary_Quick(Func_Name, Buffer_Type)                                                                                                                              \
unsigned int Search_Binary_Quick_##Func_Name(const Buffer_Type* buffer, unsigned int element_count, Buffer_Type search_data, unsigned int last_pos, unsigned int deviation_count)\
{                                                                         \
	unsigned int Min_Pos;                                                 \
	unsigned int Mid_Pos;                                                 \
	unsigned int Max_Pos;                                                 \
                                                                          \
	Mid_Pos = last_pos;                                                   \
	Min_Pos = Mid_Pos > deviation_count ? (Mid_Pos - deviation_count) : 0;\
	if (Mid_Pos > (element_count - deviation_count - 1))                  \
		Max_Pos = element_count - 1;                                      \
	else                                                                  \
		Max_Pos = Mid_Pos + deviation_count;                              \
                                                                          \
    do                                                                    \
    {                                                                     \
		if(search_data >= buffer[Mid_Pos])                                \
        {                                                                 \
            Min_Pos = Mid_Pos;                                            \
        }                                                                 \
		else                                                              \
        {                                                                 \
            Max_Pos = Mid_Pos;                                            \
        }                                                                 \
        Mid_Pos = Min_Pos + ((Max_Pos - Min_Pos) >> 1);                   \
                                                                          \
		if(Min_Pos == Mid_Pos)                                            \
		{                                                                 \
            return search_data >= buffer[Max_Pos] ? Max_Pos : Min_Pos;    \
		}                                                                 \
    } while (1);                                                          \
    return (unsigned int)-1;                                              \
}

