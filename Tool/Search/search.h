//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V1.0.0
/********************************************************
    说明：
    1、以下是查找算法。
    2、二分查找法支持char型、unsigned char型、short型、unsigned short型、int型、unsigned int型、float型。
**********************************************************/
#ifndef __SEARCH_H__
#define __SEARCH_H__


/*二分查找法 int8_t*/
extern unsigned int Search_Binary_int8_t(const char* buffer, unsigned int element_count, char search_data);
/*二分查找法 int16_t*/
extern unsigned int Search_Binary_int16_t(const short* buffer, unsigned int element_count, short search_data);
/*二分查找法 int32_t*/
extern unsigned int Search_Binary_int32_t(const int* buffer, unsigned int element_count, int search_data);
/*二分查找法 float*/
extern unsigned int Search_Binary_float(const float* buffer, unsigned int element_count, float search_data);
/*二分查找法 uint8_t*/
extern unsigned int Search_Binary_uint8_t(const unsigned char* buffer, unsigned int element_count, unsigned char search_data);
/*二分查找法 uint16_t*/
extern unsigned int Search_Binary_uint16_t(const unsigned short* buffer, unsigned int element_count, unsigned short search_data);
/*二分查找法 uint32_t*/
extern unsigned int Search_Binary_uint32_t(const unsigned int* buffer, unsigned int element_count, unsigned int search_data);


#endif /*__SEARCH_H__*/
