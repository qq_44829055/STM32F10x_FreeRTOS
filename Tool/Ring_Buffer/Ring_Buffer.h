//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/********************************************************
    说明：
    1、以下是环形缓冲区。
    2、环形缓冲区必须大于16字节长度。
    3、环形缓冲区可以用于Debug串口调试打印信息等。
    4、环形缓冲区可以达到零存整取。
**********************************************************/
#ifndef __RING_BUFFER_H__
#define __RING_BUFFER_H__

/*环形缓冲区句柄*/
typedef void* WHT_Ring_Buffer_Handle_t;

/*环形缓冲区回调函数结构体*/
typedef struct
{
    /*handle != NULL表示注册成功。buffer_size必须大于16个字节长度*/
    void (*WHT_Register)(WHT_Ring_Buffer_Handle_t* handle, unsigned char* buffer, unsigned short buffer_size);//初始化环形缓冲区
    unsigned short (*WHT_Get_Data_Size)(const WHT_Ring_Buffer_Handle_t handle);                                //获取环形缓冲区有效数据个数
    unsigned short (*WHT_Get_Idle_Size)(const WHT_Ring_Buffer_Handle_t handle);                                //获取环形缓冲区空闲大小
    unsigned short (*WHT_Write)(const WHT_Ring_Buffer_Handle_t handle, const unsigned char* input_buffer, unsigned short length);//FIFO写环形缓冲区,返回写入的个数
    unsigned short (*WHT_Read)(const WHT_Ring_Buffer_Handle_t handle, unsigned char* ontput_buffer, unsigned short length);      //FIFO读环形缓冲区,返回读取的个数
}WHT_Ring_Buffer_t;

/*全局常量*/
extern const WHT_Ring_Buffer_t WHT_Ring_Buffer;

#endif /*__RING_BUFFER_H__*/
