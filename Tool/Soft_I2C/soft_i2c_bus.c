//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/*
    这个是一个示例总线驱动。
    可以删减总线个数和调整总线GPIO来适配硬件板子。
*/
#include "soft_i2c_bus.h"
#include "../../User/BSP/GPIO_BSP/gpio_bsp.h"


WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS1;//总线1
WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS2;//总线2
WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS3;//总线3

static const WHT_Soft_I2C_BUS_Config_t WHT_Soft_I2C_BUS_Config =
{
    .Sys_Rate_Hz = 72000000,
    .I2C_Rate_Hz = 400000,
};


static void WHT_I2C_BUS1_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin6, Mode_Out_OD);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin7, Mode_Out_OD);
}
static void WHT_I2C_BUS2_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin6, Mode_Out_OD);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin7, Mode_Out_OD);
}
static void WHT_I2C_BUS3_GPIO_Init_Callback(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin6, Mode_Out_OD);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin7, Mode_Out_OD);
}

static void WHT_I2C_BUS1_GPIO_Set_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io, WHT_Sort_I2C_BUS_IO_State_enum state)
{
    if (io == Soft_I2C_SCL)
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin6, (WHT_GPIO_State_enum)state);
    else
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin7, (WHT_GPIO_State_enum)state);
}
static void WHT_I2C_BUS2_GPIO_Set_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io, WHT_Sort_I2C_BUS_IO_State_enum state)
{
    if (io == Soft_I2C_SCL)
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin6, (WHT_GPIO_State_enum)state);
    else
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin7, (WHT_GPIO_State_enum)state);
}
static void WHT_I2C_BUS3_GPIO_Set_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io, WHT_Sort_I2C_BUS_IO_State_enum state)
{
    if (io == Soft_I2C_SCL)
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin6, (WHT_GPIO_State_enum)state);
    else
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin7, (WHT_GPIO_State_enum)state);
}

static WHT_Sort_I2C_BUS_IO_State_enum WHT_I2C_BUS1_GPIO_Get_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io)
{
    if (io == Soft_I2C_SCL)
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin6);
    else
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin7);
}
static WHT_Sort_I2C_BUS_IO_State_enum WHT_I2C_BUS2_GPIO_Get_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io)
{
    if (io == Soft_I2C_SCL)
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin6);
    else
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin7);
}
static WHT_Sort_I2C_BUS_IO_State_enum WHT_I2C_BUS3_GPIO_Get_State_Callback(WHT_Soft_I2C_BUS_GPIO_enum io)
{
    if (io == Soft_I2C_SCL)
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin6);
    else
        return (WHT_Sort_I2C_BUS_IO_State_enum)WHT_GPIO_BSP.WHT_Get_State(PortB, Pin7);
}



char WHT_Soft_I2C_BUS1_Init(void)
{
    WHT_Soft_I2C_BUS1 = WHT_Soft_I2C_BUS_OPS.Register(WHT_Soft_I2C1_Name, (WHT_Soft_I2C_BUS_Config_t*)&WHT_Soft_I2C_BUS_Config, WHT_I2C_BUS1_GPIO_Init_Callback,
                                                      WHT_I2C_BUS1_GPIO_Set_State_Callback, WHT_I2C_BUS1_GPIO_Get_State_Callback);
    return WHT_Soft_I2C_BUS1 == (void*)0 ? -1 : 0;
}
char WHT_Soft_I2C_BUS2_Init(void)
{
    WHT_Soft_I2C_BUS2 = WHT_Soft_I2C_BUS_OPS.Register(WHT_Soft_I2C2_Name, (WHT_Soft_I2C_BUS_Config_t*)&WHT_Soft_I2C_BUS_Config, WHT_I2C_BUS2_GPIO_Init_Callback,
                                                      WHT_I2C_BUS2_GPIO_Set_State_Callback, WHT_I2C_BUS2_GPIO_Get_State_Callback);
    return WHT_Soft_I2C_BUS2 == (void*)0 ? -1 : 0;
}
char WHT_Soft_I2C_BUS3_Init(void)
{
    WHT_Soft_I2C_BUS3 = WHT_Soft_I2C_BUS_OPS.Register(WHT_Soft_I2C3_Name, (WHT_Soft_I2C_BUS_Config_t*)&WHT_Soft_I2C_BUS_Config, WHT_I2C_BUS3_GPIO_Init_Callback,
                                                      WHT_I2C_BUS3_GPIO_Set_State_Callback, WHT_I2C_BUS3_GPIO_Get_State_Callback);
    return WHT_Soft_I2C_BUS3 == (void*)0 ? -1 : 0;
}
