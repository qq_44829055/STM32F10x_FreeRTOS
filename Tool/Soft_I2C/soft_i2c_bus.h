#ifndef __SOFT_I2C_BUS_H__
#define __SOFT_I2C_BUS_H__

#include "soft_i2c_bsp.h"

extern WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS1;//总线1
extern WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS2;//总线2
extern WHT_Soft_I2C_BUS_t* WHT_Soft_I2C_BUS3;//总线3
extern char WHT_Soft_I2C_BUS1_Init(void);//返回值非0错误
extern char WHT_Soft_I2C_BUS2_Init(void);//返回值非0错误
extern char WHT_Soft_I2C_BUS3_Init(void);//返回值非0错误

#endif // !__SOFT_I2C_BUS_H__
