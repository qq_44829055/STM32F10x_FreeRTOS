//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.2.1
/********************************************************
    说明：
    1、先注册个总线，最大3个。每个总线上可挂多个I2C设备。每个总线的速率可动态调整。(挂在一起的设备引脚必须一样)
    2、每个外挂的IIC设备自行提供收发缓存。
    3、每个总线均会自动提供给I2C实时状态和互斥锁状态。
    4、只支持普通收发数据。
    5、只支持Master模式。
    6、由于MCU的IO反转速率很慢，会导致设置的波特率达不到预期，用户自行提高设置的波特率值。
**********************************************************/

#ifndef __SOFT_I2C_BSP_H__
#define __SOFT_I2C_BSP_H__

#define WHT_Soft_I2C1_Name    "WHT_Soft_I2C1"
#define WHT_Soft_I2C2_Name    "WHT_Soft_I2C2"
#define WHT_Soft_I2C3_Name    "WHT_Soft_I2C3"

/*I2C发送或接收枚举*/
typedef enum
{
    Soft_I2C_Write = 0,//写
    Soft_I2C_Read  = 1,//读
}WHT_Soft_I2C_RW_enum;

/*软I2C状态枚举*/
typedef enum
{
    Soft_I2C_No_Error     = 0,//无错误码
    Soft_I2C_Slave_No_ACK = 1,//从设备无应答
    Soft_I2C_BUS_Error    = 2,//总线错误
}WHT_Soft_I2C_BUS_State_enum;

/*软I2C锁枚举*/
typedef enum
{
    Soft_I2C_Unlock = 0,
    Soft_I2C_Lock   = 1,
}WHT_Soft_I2C_BUS_Lock_enum;

/*软I2C IO状态枚举*/
typedef enum
{
    Soft_I2C_IO_Low = 0,
    Soft_I2C_IO_Hig = 1,
}WHT_Sort_I2C_BUS_IO_State_enum;

/*软I2C IO选择枚举*/
typedef enum
{
    Soft_I2C_SCL = 0,
    Soft_I2C_SDA = 1,
}WHT_Soft_I2C_BUS_GPIO_enum;

/*软I2C总线配置结构体*/
typedef struct
{
    unsigned int Sys_Rate_Hz;//系统频率
    unsigned int I2C_Rate_Hz;//I2C频率
}WHT_Soft_I2C_BUS_Config_t;

/*软I2C总线结构体*/
typedef struct
{
    char* Name;                             //总线名字
    WHT_Soft_I2C_BUS_Config_t Config;       //总线配置信息
    const WHT_Soft_I2C_BUS_Lock_enum Mutex; //驱动锁,自动上锁与解锁
    const WHT_Soft_I2C_BUS_State_enum State;//错误标志,自动清零
}WHT_Soft_I2C_BUS_t;

/*软I2C缓存结构体*/
typedef struct
{
    unsigned char Addr_7Bit;  //从设备7位地址
    WHT_Soft_I2C_RW_enum Dir; //读写方向
    unsigned char Cmd_Count;  //命令个数
    unsigned char Cmd[4];     //从设备命令,配合WHT_I2C_Slave_nByte_Cmd_enum
    unsigned int Buffer_Count;//个数
    unsigned char* Buffer;    //缓存
}WHT_Soft_I2C_Cache_t;

/*软I2C回调函数结构体*/
typedef struct
{
    WHT_Soft_I2C_BUS_t* (*Register)(char* name, WHT_Soft_I2C_BUS_Config_t* config, void (*gpio_init_callback)(void),
                                    void (*gpio_set_callback)(WHT_Soft_I2C_BUS_GPIO_enum io, WHT_Sort_I2C_BUS_IO_State_enum state),
                                    WHT_Sort_I2C_BUS_IO_State_enum (*gpio_get_callback)(WHT_Soft_I2C_BUS_GPIO_enum io));
    void (*Unregister)(WHT_Soft_I2C_BUS_t* bus);
    void (*Set_Rate)(WHT_Soft_I2C_BUS_t* bus);
    char (*Scan_Mount_Count)(WHT_Soft_I2C_BUS_t* bus);
    void (*Read_Write)(WHT_Soft_I2C_BUS_t* bus, WHT_Soft_I2C_Cache_t* cache);
}WHT_Soft_I2C_BUS_OPS_t;

/*全局常量*/
extern const WHT_Soft_I2C_BUS_OPS_t WHT_Soft_I2C_BUS_OPS;

#endif // !__SOFT_I2C_BSP_H__
