//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/********************************************************
    说明：
    1、支持CRC8和CRC16但不够全面。
**********************************************************/
#ifndef __CRC_H__
#define __CRC_H__

/***************************CRC8*********************************/
/*查找法与计算法*/
/*多项式:x8+x2+x+1*/
extern unsigned char CRC8_Lookup(const unsigned char *buf, unsigned int length);
extern unsigned char CRC8_Calculate(const unsigned char *buf, unsigned int length);

/*多项式:x8+x5+x4+1*/
extern unsigned char CRC8_MAXIM_Lookup(const unsigned char* buf, unsigned int length);
extern unsigned char CRC8_MAXIM_Calculate(const unsigned char* buf, unsigned int length);

/*求和法*/
extern unsigned char CRC8_SUM(const unsigned char* buf, unsigned int length);


/***************************CRC16*********************************/
/*查找法与计算法*/
extern unsigned short CRC16_MODBUS_Lookup(const unsigned char* buf, unsigned int length);
extern unsigned short CRC16_MODBUS_Calculate(const unsigned char* buf, unsigned int length);

/*求和法*/
extern unsigned short CRC16_SUM(const unsigned char* buf, unsigned int length);



#endif // !__CRC_H__
