//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/********************************************************
    说明：
    1、以下这些接口主要用于RGB888与RGB565及大小端之间的格式转换
    2、LE是小端数据 BE是大端数据
    3、RGB888小端格式转换RGB565小端格式
    4、RGB888小端格式转换RGB565大端格式
    5、RGB888大端格式转换RGB565小端格式
    6、RGB888大端格式转换RGB565大端格式
    7、RGB565小端格式转换RGB888小端格式
    8、RGB565小端格式转换RGB888大端格式
    9、RGB565大端格式转换RGB888小端格式
    10、RGB565大端格式转换RGB888大端格式
**********************************************************/
#ifndef __RGB_CONVERT_H__
#define __RGB_CONVERT_H__

/*uint32_t RGB888_LE To uint16_t RGB565_LE*/
extern unsigned short RGB888_LE_To_RGB565_LE(unsigned int RGB888_LE);
/*uint32_t RGB888_LE To uint16_t RGB565_BE*/
extern unsigned short RGB888_LE_To_RGB565_BE(unsigned int RGB888_LE);
/*uint32_t RGB888_BE To uint16_t RGB565_LE*/
extern unsigned short RGB888_BE_To_RGB565_LE(unsigned int RGB888_BE);
/*uint32_t RGB888_BE To uint16_t RGB565_BE*/
extern unsigned short RGB888_BE_To_RGB565_BE(unsigned int RGB888_BE);


/*uint16_t RGB565_LE To uint32_t RGB888_LE*/
extern unsigned int RGB565_LE_To_RGB888_LE(unsigned int RGB565_LE);
/*uint16_t RGB565_LE To uint32_t RGB888_BE*/
extern unsigned int RGB565_LE_To_RGB888_BE(unsigned int RGB565_LE);
/*uint16_t RGB565_BE To uint32_t RGB888_LE*/
extern unsigned int RGB565_BE_To_RGB888_LE(unsigned int RGB565_BE);
/*uint16_t RGB565_BE To uint32_t RGB888_BE*/
extern unsigned int RGB565_BE_To_RGB888_BE(unsigned int RGB565_BE);


#endif /*__RGB_CONVERT_H__*/
