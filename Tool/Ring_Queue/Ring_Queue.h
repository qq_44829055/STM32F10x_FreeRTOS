//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
/********************************************************
    说明：
    1、以下是环形队列。
    2、环形队列必须大于24字节长度，队列个数至少1个。
    3、相比FreeRTOS的环形队列更节省内存。
    4、写基于FIFO先进先发原则。
    5、读基于FIFO先进先读原则。
    6、环形队列是整存整取的。
**********************************************************/
#ifndef __RING_QUEUE_H__
#define __RING_QUEUE_H__

/*环形缓冲区句柄*/
typedef void* WHT_Ring_Queue_Handle_t;

/*环形缓冲区回调函数结构体*/
typedef struct
{
    /*handle != NULL表示注册成功。buffer_size必须大于24 + queue_count * 2个字节长度*/
    void (*WHT_Register)(WHT_Ring_Queue_Handle_t* handle, unsigned char* buffer, unsigned short buffer_size, unsigned short queue_count);//初始化环形队列
    unsigned short (*WHT_Get_Empty_State)(const WHT_Ring_Queue_Handle_t handle);                                                   //获取环形队列空状态
    unsigned short (*WHT_Get_Idle_Size)(const WHT_Ring_Queue_Handle_t handle);                                                     //获取环形队列空闲大小
    unsigned short (*WHT_Write)(const WHT_Ring_Queue_Handle_t handle, const unsigned char* input_buffer, unsigned short length);   //FIFO写环形队列,返回写入的个数
    unsigned short (*WHT_Read)(const WHT_Ring_Queue_Handle_t handle, unsigned char* ontput_buffer, unsigned short expected_length);//FIFO读环形队列,返回读取的个数
}WHT_Ring_Queue_t;

/*全局常量*/
extern const WHT_Ring_Queue_t WHT_Ring_Queue;

#endif /*__RING_QUEUE_H__*/
