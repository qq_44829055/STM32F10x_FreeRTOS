#include "FreeRTOS.h"        //RTOS主文件
#include "task.h"            //RTOS任务管理
#include "queue.h"           //RTOS队列管理
#include "semphr.h"          //RTOS消息管理
#include "event_groups.h"    //RTOS事件管理
#include "timers.h"          //RTOS定时管理

#include "./Driver/Debug_Driver/debug_driver.h"

#include "./APP/App_led/app_led.h"
#include "./APP/App_button/app_button.h"
#include "./APP/App_gui/app_oled.h"
#include "./BSP/WWDG_BSP/wwdg_bsp.h"

#include "./APP/App_ModBus/app_modbus.h"
#include "./APP/App_lvgl/app_lvgl.h"

WHT_WWDG_Config_t WWDG_Config = {36000, 10, 50, ENABLE};

/*
* STM32 M3中断优先级分组为4，即4bit都用来表示抢占优先级，范围为：0~15
* 优先级分组只需要分组一次即可，以后如果有其他的任务需要用到中断，
* 都统一用这个优先级分组，千万不要再分组，切忌。
*/
static __inline void Hardware_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO ,ENABLE);//重映射使能AFIO时钟
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);//只关闭JTAG而保留SWD
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO ,DISABLE);//重映射失能AFIO时钟
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
    WHT_Debug_Driver.WHT_Init();//debug串口初始化

    //WHT_WWDG_BSP.WHT_Config(&WWDG_Config);
}



static void App_Start_Task(void* parameter);//启动其它任务的启动任务
static void Admin_Task(void* parameter);    //管理其它任务的管理任务
static void WHT_Test_Task(void* parameter); //测试任务

/**********************************************************************/
int main(void)
{
    Hardware_Init();//先初始化硬件
    
    xTaskCreate((TaskFunction_t)App_Start_Task,      //任务函数
				(char *        )"App_Start_Task",    //任务名字
				(uint16_t      )256,                 //任务堆栈大小
				(void *        )NULL,                //任务传参
				(UBaseType_t   )configMAX_PRIORITIES,//任务优先级
				(TaskHandle_t *)NULL);               //任务控制块

    vTaskStartScheduler();
    while(1);
}
/**********************************************************************/
static void App_Start_Task(void *parameter)
{
    printf("SystemCoreClock = %uMHz\r\n", SystemCoreClock / 1000000);
    for (uint8_t Start_Countdown_Time = 4; Start_Countdown_Time > 0; Start_Countdown_Time--)
    {
        printf("STM32F103 Start Countdown is %d Sec\r\n", Start_Countdown_Time - 1);
        vTaskDelay(1000 / portTICK_RATE_MS);
    }
    printf("*************************************\r\n");
    printf("进入临界区保护\r\n");
    taskENTER_CRITICAL();
    
    xTaskCreate((TaskFunction_t)Admin_Task,
			    (char *        )"Admin_Task",
			    (uint16_t      )320,
			    (void *        )NULL,
			    (UBaseType_t   )1,
			    (TaskHandle_t *)NULL);

    xTaskCreate((TaskFunction_t)WHT_Test_Task,
				(char *        )"WHT_Test_Task",
				(uint16_t      )256,
			    (void *        )NULL,
			    (UBaseType_t   )3,
				(TaskHandle_t *)NULL);
    
    /*************添加其它任务代码**************/
    //WHT_Button_t WHT_Button;
    //WHT_Button_Init(5, &WHT_Button);
    //WHT_OLED_Install(3);
    //WHT_ModBus_Install(6);
    //WHT_LVGL_Install(0);
    /*************添加其它任务代码**************/

    taskEXIT_CRITICAL();
	printf("退出临界区保护\r\n");
	vTaskDelete(NULL);//删除启动任务
}
/**********************************************************************/


/*RTOS任务实时状态任务*/
static void Admin_Task(void* parameter)
{
    char Task_buf[512];

    for(;;)
    {
        vTaskList(Task_buf);
        WHT_Debug_Driver.WHT_Printf("Task_Name     State  Priority  Stack  Number\r\n", 46);//格局已调试OK
        WHT_Debug_Driver.WHT_Printf(Task_buf, strlen(Task_buf));
        WHT_Debug_Driver.WHT_Printf("\r\n", 2);
        vTaskDelay(3000 / portTICK_RATE_MS);
    }
}



/*Debug调试任务*/
static void WHT_Test_Task(void* parameter)
{
    uint8_t receive_buff[128];
    uint16_t Length;

    for (; ;)
    {
        Length = WHT_Debug_Driver.WHT_Scanf(receive_buff, sizeof(receive_buff), portMAX_DELAY);
        printf("\r\nreceive %d\r\n", Length);
        WHT_Debug_Driver.WHT_Printf((char*)receive_buff, Length);//原样发出
    }
}
