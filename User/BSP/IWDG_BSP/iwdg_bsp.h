//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、注意最大喂狗时间要小于26秒。
    2、支持动态调整喂狗重装时间，即动态调整复位倒计时。
    3、提供手动喂狗。注意超时将复位系统。
    4、目前不支持暂停看门狗。
    5、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __IWGD_BSP_H__
#define __IWGD_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_iwdg.h"
/*****************************************************/


#define WHT_IWDG_BSP_LSI_CLK_HZ           40000//STM32F10x的IWDG时钟源固定40Khz
#define WHT_IWDG_BSP_MAX_RESET_TIME_MS    26214//计算公式(4096/(WHT_IWDG_BSP_LSI_CLK_HZ/256))秒


/*看门狗回调函数结构体*/
typedef struct
{
    unsigned int WHT_Reset_Time_ms;//for 复位时间 <= WHT_IWDG_BSP_MAX_RESET_TIME_MS
    ErrorStatus (*WHT_Reset_Reload)(unsigned int reset_time_ms);//for  <= WHT_IWDG_BSP_MAX_RESET_TIME_MS
    void (*WHT_Start)(void);       //开启看门狗
    void (*WHT_Stop)(void);        //手册上说一旦开启无法停止，除非复位
    void (*WHT_Feed_Dog)(void);    //喂狗
}WHT_IWDG_BSP_t;

/*全局常量*/
extern const WHT_IWDG_BSP_t WHT_IWDG_BSP;

#endif /*__IWGD_BSP_H__*/
