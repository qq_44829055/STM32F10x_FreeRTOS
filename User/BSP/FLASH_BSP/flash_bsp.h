//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、单片机内部flash的擦除与编程。
    2、擦除函数自带查空操作。
    3、编程函数自带校验操作。
    4、注意内部flash擦除的时候所有中断会暂停，小心看门狗导致重启现象。
    5、基于STM32F10X标准库V3.6.0
**********************************************************/
#ifndef __FLASH_BSP_H__
#define __FLASH_BSP_H__

#include "stm32f10x.h"
#include "stm32f10x_flash.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_crc.h"
#include "../WWDG_BSP/wwdg_bsp.h"
#include "../IWDG_BSP/iwdg_bsp.h"

/*是否开启窗口看门狗*/
#define WWDG_FUNC_ENABLE

/*
STM32   F        103     V      E                 T     6
32bit   基础型号  基础款  引脚数  4:16KB LD小容量   封装  温度等级
                                6:32KB LD小容量
                                8:64KB MD中容量
                                B:128KB MD中容量
                                C:256KB HD大容量
                                E:512KB HD大容量
                                F:768KB XL超大容量
                                G:1024KB XL超大容量
*/

 /* STM32 大容量产品每页大小 2KByte，中、小容量产品每页大小 1KByte */
#if defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_CL) || defined (STM32F10X_XL)
#define FLASH_PAGE_SIZE ((unsigned short)0x800)//2048字节
#else
#define FLASH_PAGE_SIZE ((unsigned short)0x400)//1024字节
#endif

#define FLASH_BASE_ADDR  ((unsigned int)0x08000000)


/*擦除内部FLASH*/
extern ErrorStatus WHT_Flash_BSP_Erase_And_Check(unsigned int start_page, unsigned int page_count);
/*读取内部FLASH,start_addr是真实地址*/
extern void WHT_Flash_BSP_Read(unsigned int start_addr, unsigned char* out_buf, unsigned int size);
/*写入内部FLASH，start_addr是真实地址*/
extern ErrorStatus WHT_Flash_BSP_Write(unsigned int start_addr, const unsigned char* in_buf, unsigned int size);


#endif // !__FLASH_BSP_H__
