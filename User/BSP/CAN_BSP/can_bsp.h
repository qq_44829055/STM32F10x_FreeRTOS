//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、每个外挂的CAN模块自行配置CAN参数。
    2、每个外挂的CAN模块自行提供收发缓存。
    3、MCU自动提供给外挂模块的CAN实时状态。
    4、支持普通发数据、中断接收数据。
    5、CAN1接口为PB8/PB9。CAN2的IO当前未支持。
    6、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __CAN_BSP_H__
#define __CAN_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_can.h"
#include "misc.h"
#include "../GPIO_BSP/gpio_bsp.h"
/*****************************************************/


/*CAN号枚举*/
typedef enum
{
    CAN_1 = CAN1_BASE,//CAN1
    CAN_2 = CAN2_BASE,//CAN2
}WHT_CAN_enum;

/*CAN状态枚举*/
typedef enum
{
    CAN_TRx_Idle = 0,//空闲
    CAN_TRx_Busy = 1,//忙碌

    CAN_No_Error = 0,//无错误码
    CAN_Rx_Over_Error = 2,//接收溢出
    CAN_CRC_Error = 3,//CRC校验错误
}WHT_CAN_State_enum;

/*CAN信息结构体*/
typedef struct//后期将cache地址用(void*)类型的指针变量代替更好看些
{
    FlagStatus BSP_Mutex;              //驱动锁
    WHT_CAN_State_enum TRx_State;      //忙碌标志,自动清零
    WHT_CAN_State_enum TRx_Error_State;//错误标志,自动清零
    unsigned char Mount_Count;         //设备个数
    void* Cache_New_Addr;              //最新缓存
}WHT_CAN_Info_t;

/*CAN配置结构体*/
typedef struct
{
    WHT_CAN_enum Name;        //CAN号
    unsigned char PreemptionPriority;//抢占优先级
    unsigned char SubPriority;//子优先级
    unsigned int ID;          //CAN ID 高3Bit不要用
}WHT_CAN_Config_t;

/*CAN缓存结构体*/
typedef struct
{
    unsigned int Rx_Count;        //接收个数
    unsigned int Tx_Count;        //发送个数
    unsigned char* Rx_Buf;        //接收缓存
    unsigned char* Tx_Buf;        //发送缓存
    volatile WHT_CAN_Info_t* Info;//状态信息
    void* Private_Data;           //私有数据
}WHT_CAN_Cache_t;

/*CAN回调函数结构体*/
typedef struct
{
    ErrorStatus (*WHT_Register)(const WHT_CAN_Config_t* config, const WHT_CAN_Cache_t* cache);//注册
    void (*WHT_Config)(const WHT_CAN_Config_t* config, const WHT_CAN_Cache_t* cache);//每个设备可自行配置
    void (*WHT_TxMessage)(const WHT_CAN_Cache_t* cache);          //普通发送
    void (*WHT_Interrupt_TxMessage)(const WHT_CAN_Cache_t* cache);//中断发送
    void (*WHT_RxMessage)(const WHT_CAN_Cache_t* cache);          //普通接收
    void (*WHT_Interrupt_RxMessage)(const WHT_CAN_Cache_t* cache);//中断接收
}WHT_CAN_BSP_t;

/*全局常量*/
extern const WHT_CAN_BSP_t WHT_CAN_BSP;

#endif /*__CAN_BSP_H__*/
