#include "spi_bus.h"
#include "../../User/BSP/GPIO_BSP/gpio_bsp.h"


WHT_SPI_BUS_t* WHT_SPI_BUS1;//总线1
WHT_SPI_BUS_t* WHT_SPI_BUS2;//总线2
WHT_SPI_BUS_t* WHT_SPI_BUS3;//总线3

static const WHT_SPI_BUS_Config_t WHT_SPI_BUS_Config =
{
	.Master_Slave = 0,
	.Work_Mode    = SPI_Work_Mode3,
	.Clock_Div    = APBx_DIV4,
	.MSB_LSB      = 0,
	.Bit_Wide     = SPI_8Bit_Wide,
	.NSS          = Hard_NSS,
	.IT_PreemptionPriority = 15,
	.IT_SubPriority        = 0,
};


static void WHT_SPI_BUS1_GPIO_Init_Callback(void)
{
	WHT_GPIO_BSP.WHT_Set_Clock(PortA, ENABLE);
	WHT_GPIO_BSP.WHT_Set_State(PortA, Pin5 | Pin6 | Pin7, Hig);
#ifdef SPI1_MASTER_MODE
	if (WHT_SPI_BUS_Config.NSS == Hard_NSS)//GPIO_CS_NSS
	{
		WHT_GPIO_BSP.WHT_Set_State(PortA, Pin4, Hig);
		WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin4, Mode_AF_PP);
	}
	WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin5 | Pin7, Mode_AF_PP);
	WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin6, Mode_IN_FLOATING);
#else
	WHT_GPIO_BSP.WHT_Set_State(PortA, Pin4), Hig);
	WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin4 | Pin5 | Pin7, Mode_IN_FLOATING);
	WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin6, Mode_AF_PP);
#endif /*! SPI1_MASTER_MODE*/
}
static void WHT_SPI_BUS2_GPIO_Init_Callback(void)
{
	WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
	WHT_GPIO_BSP.WHT_Set_State(PortB, Pin13 | Pin14 | Pin15, Hig);
#ifdef SPI2_MASTER_MODE
	if (WHT_SPI_BUS_Config.NSS == Hard_NSS)//GPIO_CS_NSS
	{
		WHT_GPIO_BSP.WHT_Set_State(PortB, Pin12, Hig);
		WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin12, Mode_AF_PP);
	}
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin13 | Pin15, Mode_AF_PP);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin14, Mode_IN_FLOATING);
#else
	WHT_GPIO_BSP.WHT_Set_State(PortB, Pin12, Hig);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin12 | Pin13 | Pin14, Mode_IN_FLOATING);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin15, Mode_AF_PP);
#endif /*! SPI2_MASTER_MODE*/
}
static void WHT_SPI_BUS3_GPIO_Init_Callback(void)
{
	WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
	WHT_GPIO_BSP.WHT_Set_State(PortB, Pin3 | Pin4 | Pin5, Hig);
#ifdef SPI3_MASTER_MODE
	if (WHT_SPI_BUS_Config.NSS == Hard_NSS)//GPIO_CS_NSS
	{
		WHT_GPIO_BSP.WHT_Set_Clock(PortA, ENABLE);
		WHT_GPIO_BSP.WHT_Set_State(PortA, Pin15, Hig);
		WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin15, Mode_AF_PP);
	}
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin3 | Pin5, Mode_AF_PP);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin4, Mode_IN_FLOATING);
#else
	WHT_GPIO_BSP.WHT_Set_Clock(PortA, ENABLE);
	WHT_GPIO_BSP.WHT_Set_State(PortA, Pin15, Hig);
	WHT_GPIO_BSP.WHT_Set_Mode(PortA, Pin15, Mode_IN_FLOATING);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin12 | Pin3 | Pin5, Mode_IN_FLOATING);
	WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin4, Mode_AF_PP);
#endif /*! SPI3_MASTER_MODE*/
}



char WHT_SPI_BUS1_Init(void)
{
    WHT_SPI_BUS1 = WHT_SPI_BUS_OPS.Register(WHT_SPI1_Name, (WHT_SPI_BUS_Config_t*)&WHT_SPI_BUS_Config, WHT_SPI_BUS1_GPIO_Init_Callback);
    return WHT_SPI_BUS1 == (void*)0 ? -1 : 0;
}
char WHT_SPI_BUS2_Init(void)
{
    WHT_SPI_BUS2 = WHT_SPI_BUS_OPS.Register(WHT_SPI2_Name, (WHT_SPI_BUS_Config_t*)&WHT_SPI_BUS_Config, WHT_SPI_BUS2_GPIO_Init_Callback);
    return WHT_SPI_BUS2 == (void*)0 ? -1 : 0;
}
char WHT_SPI_BUS3_Init(void)
{
    WHT_SPI_BUS3 = WHT_SPI_BUS_OPS.Register(WHT_SPI3_Name, (WHT_SPI_BUS_Config_t*)&WHT_SPI_BUS_Config, WHT_SPI_BUS3_GPIO_Init_Callback);
    return WHT_SPI_BUS3 == (void*)0 ? -1 : 0;
}
