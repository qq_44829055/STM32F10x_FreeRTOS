#ifndef __SPI_BUS_H__
#define __SPI_BUS_H__

#include "spi_bsp.h"

#define SPI1_MASTER_MODE//主模式
#define SPI2_MASTER_MODE//主模式
#define SPI3_MASTER_MODE//主模式

extern WHT_SPI_BUS_t* WHT_SPI_BUS1;//总线1
extern WHT_SPI_BUS_t* WHT_SPI_BUS2;//总线2
extern WHT_SPI_BUS_t* WHT_SPI_BUS3;//总线3
extern char WHT_SPI_BUS1_Init(void);//返回值非0错误
extern char WHT_SPI_BUS2_Init(void);//返回值非0错误
extern char WHT_SPI_BUS3_Init(void);//返回值非0错误


#endif // !__SPI_BUS_H__
