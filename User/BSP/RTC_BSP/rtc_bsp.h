//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、支持内部时钟和外部时钟源。
    2、支持秒中断、闹钟中断、计数器溢出中断。
    3、支持检测上次配置是否被清掉。
    4、支持动态配置闹钟时间、动态配置当前时间。
    5、提供系统32位计数器值，不提供万年历功能。
    6、万年历属于APP范畴，后期在提供或不提供。
    7、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __RTC_BSP_H__
#define __RTC_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_pwr.h"
#include "stm32f10x_bkp.h"
#include "misc.h"
/*****************************************************/


/* 如果定义了下面这个宏的话,PC13就会输出频率为RTC Clock/64的时钟 */
//#define RTCClockOutput_Enable  /* RTC Clock/64 is output on tamper pin(PC.13) */

/*RTC时钟源枚举*/
typedef enum
{
    NO_Clk          = 0,                          //无时钟源
    LSE_OSC_32768Hz = RCC_RTCCLKSource_LSE,       //外部32768hz
    LSI_RC_40000Hz  = RCC_RTCCLKSource_LSI,       //内部40000hz  //IWDG的40khz好像未等待稳定
    HSE_OSC_Div128  = RCC_RTCCLKSource_HSE_Div128,//外部8000000hz的128分频
}WHT_RTC_Clk_enum;

/*RTC运行状态枚举*/
typedef enum
{
    RTC_Running_Well = 0,//运行正常
    RTC_Running_Stop = 1,//运行停止
}WHT_RTC_State_enum;

/*RTC配置结构体*/
typedef struct
{
    unsigned int HSE_OSC_Hz;        //当RTC_Clock_Source = HSE_OSC_Div128时有效
    WHT_RTC_Clk_enum Clock_Source;  //当使用LSE_OSC_32768Hz如果LSE异常默认自动切换到LSI_RC_40000Hz
    FunctionalState Over_Interrupt; //计数器溢出
    FunctionalState Alarm_Interrupt;//闹钟//特殊备注:RTC的闹钟可以进入RTC中断，可以进入EXTI RTC闹钟中断
    FunctionalState Sec_Interrupt;  //秒中断
    void (*Over_Callback)(void);    //计数器溢出回调函数
    void (*Alarm_Callback)(void);   //闹钟回调函数
    void (*Sec_Callback)(void);     //秒回调函数
}WHT_RTC_Config_t;

/*RTC回调函数结构体*/
typedef struct
{
    WHT_RTC_State_enum (*WHT_Get_State)(const WHT_RTC_Config_t* config);//获取当前RTC运行状态
    WHT_RTC_Clk_enum (*WHT_Get_Clock_Source)(void);                     //获取当前RTC时钟源
    void (*WHT_Config)(const WHT_RTC_Config_t* config);                 //RTC时钟配置
    void (*WHT_Set_Start_Time)(unsigned int time);                      //设置RTC时间并运行
    void (*WHT_Set_Alarm_Time)(unsigned int time);                      //设置闹钟时间
    unsigned int (*WHT_Get_New_Time)(void);                             //获取当前RTC时间
}WHT_RTC_BSP_t;

/*全局常量*/
extern const WHT_RTC_BSP_t WHT_RTC_BSP;


/*地区时差偏移枚举*/
typedef enum
{
    Washington_Time_Offset = (-5*60*60),//华盛顿时间偏移
    London_Time_Offset     = ( 0*60*60),//伦敦时间偏移
    Berlin_Time_Offset     = ( 1*60*60),//柏林时间偏移
    Moscow_Time_Offset     = ( 3*60*60),//莫斯科时间偏移
    Beijing_Time_Offset    = ( 8*60*60),//北京时间偏移
    Tokyo_Time_Offset      = ( 9*60*60),//东京时间偏移
    
}WHT_Time_Offset_enum;

/*万年历时间结构体*/
typedef struct
{
    unsigned char Wday; //星期
	unsigned char Sec;  //秒
	unsigned char Min;  //分
	unsigned char Hour; //时
	unsigned char Mday; //天
	unsigned char Month;//月
	unsigned short Year;//年
}WHT_RTC_Calendar_t;//本驱动不提供处理

#endif /*__RTC_BSP_H__*/
