//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、对于STM32F10x WWDG挂载在APB1，Max Clk=36Mhz。
    2、支持手动控制WWDG让系统重启。
    3、支持动态调整窗口时间或者起始时间。
    4、支持手动喂狗。（切记提前喂狗会导致系统重启，如果不确定建议使用动态调整起始时间变相喂狗）
    5、支持将WWDG通过计数进行控制。默认配置的WWDG参数是时间。
    6、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __WWDG_BSP_H__
#define __WWDG_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_wwdg.h"
#include "misc.h"
/*****************************************************/


//#define WHT_WWDG_Count_Mode  //窗口看门狗计数模式


#ifdef WHT_WWDG_Count_Mode
/*WWDG时钟分频枚举*/
typedef enum
{
    WWDG_Div_1 = WWDG_Prescaler_1,//时钟源的1分频
    WWDG_Div_2 = WWDG_Prescaler_2,//时钟源的2分频
    WWDG_Div_4 = WWDG_Prescaler_4,//时钟源的4分频
    WWDG_Div_8 = WWDG_Prescaler_8,//时钟源的8分频
}WHT_WWDG_Div_enum;

/*WWD配置结构体*/
typedef struct
{
    WHT_WWDG_Div_enum Prescaler;            //时钟分频
    unsigned int Window_Counter;            //for Max =0x7f;Min =0x40;
    unsigned int Start_Counter;             //for Max =0x7f;Min =0x40;
    FunctionalState Early_Wakeup_Interrupt; //提前唤醒中断功能
}WHT_WWDG_Advance_Mode_Config_t;

/*WWDG回调函数结构体*/
typedef struct
{
    void (*WHT_Config)(const WHT_WWDG_Advance_Mode_Config_t* config);               //配置WWDG,内部带有防多次配置
    void (*WHT_Feed_Dog)(void);                                                     //提前喂狗会导致系统复位
    void (*WHT_System_Restart)(void);                                               //WWDG复位系统
    void (*WHT_Reset_Windows_Counter)(const WHT_WWDG_Advance_Mode_Config_t* config);//会主动喂狗1次
    void (*WHT_Reset_Start_Counter)(const WHT_WWDG_Advance_Mode_Config_t* config);  //会主动喂狗1次
}WHT_WWDG_BSP_Advance_Mode_t;

/*全局常量*/
extern const WHT_WWDG_BSP_Advance_Mode_t WHT_WWDG_BSP_Advance_Mode;


#else //窗口看门狗时间模式

/*WWDG配置结构体*/
typedef struct
{
    unsigned int APB1_Clk_KHz;              //for STM32F10x APB1 Max=36Mhz
    unsigned int Window_Time_ms;            //for 36Mhz Max =58ms;Min =1ms;
    unsigned int Start_Time_ms;             //for 36Mhz Max =58ms;Min =1ms;
    FunctionalState Early_Wakeup_Interrupt; //提前唤醒中断功能
}WHT_WWDG_Config_t;

/*WWDG回调函数结构体*/
typedef struct
{
    void (*WHT_Config)(const WHT_WWDG_Config_t* config);            //配置WWDG,内部带有防多次配置
    void (*WHT_Feed_Dog)(void);                                     //提前喂狗会导致系统复位
    void (*WHT_System_Restart)(void);                               //is System Restart
    void (*WHT_Reset_Windows_Time)(const WHT_WWDG_Config_t* config);//会主动喂狗1次
    void (*WHT_Reset_Start_Time)(const WHT_WWDG_Config_t* config);  //会主动喂狗1次
}WHT_WWDG_BSP_t;

/*全局常量*/
extern const WHT_WWDG_BSP_t WHT_WWDG_BSP;

#endif /*WHT_WWDG_Time_Mode*/

#endif /*__WWDG_BSP_H__*/
