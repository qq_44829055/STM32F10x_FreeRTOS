#include "timer_bsp.h"

static volatile uint32_t* LED;

static void led_test(void)
{
    *LED = *LED == Hig? Low:Hig;
}

static const WHT_Timer_Basic_Config_t Basic_config[1] = 
{
    {
    .PreemptionPriority = 15,
    .SubPriority =0,
    .Interrupt_Callback = led_test,
    }
};

static const WHT_Timer_PWM_Out_Config_t PWM_config[1] =
{
    {
        .Channel = Timer_CH1,
        .Normal_Polarity = Hig,
        .Normal_Idle_Polarity = Hig,
        .Normal_Output_GPIO_Port = PortA,
        .Normal_Output_GPIO_Pin = Pin8,

        .Complementary_Output_State = ENABLE,
        .CO_Polarity = Hig,
        .CO_Idle_Polarity = Low,
        .CO_Output_GPIO_Port = PortB,
        .CO_Output_GPIO_Pin = Pin13,

        .Brake_Output_State = DISABLE,
        .Brake_Output_GPIO_Port = PortB,
        .Brake_Output_GPIO_Pin = Pin12,
    },
};


static WHT_Timer_Config_t timer_config =
{
    .Name = Timer1,
    .Cycle_ns = 80000000,
    .Mode = Timer_Output_Comparison,
    /////.Mode = Timer_Basic,
    .Config_Count = 1,
    .Config = &PWM_config,
    ////.Config = &Basic_config,
};



void timer_test(void)
{
    static uint8_t aaa = 0;
    static uint8_t bbb = 0;

    
    if (aaa == 0)
    {
        aaa++;

        WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
        WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin5, Mode_Out_PP);
        WHT_GPIO_BSP.WHT_Set_State(PortB, Pin5, Hig);
        WHT_GPIO_BSP.WHT_Config_Bit_Output(PortB, Pin5, &LED);


        WHT_Timer_BSP.WHT_Config(&timer_config);
        WHT_Timer_BSP.WHT_Start(&timer_config);
        WHT_Timer_BSP.WHT_Set_Duty_Cycle(&timer_config, PWM_config->Channel, 50.0f);
        
    }
    if (bbb++ == 10)
    {
        WHT_Timer_BSP.WHT_Stop(&timer_config);
    }
    if (bbb >= 15)
    {
        bbb = 1;
        WHT_Timer_BSP.WHT_Start(&timer_config);
    }
    
   //// led_test();
    
}
