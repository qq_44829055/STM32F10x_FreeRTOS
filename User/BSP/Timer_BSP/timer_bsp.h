//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.2
/********************************************************
    说明：
    1、普通定时器。
    2、PWM输出。
    3、捕获输入暂不支持。
    4、基于STM32F10X标准库V3.6.0
**********************************************************/
#ifndef __TIMER_BSP_H__
#define __TIMER_BSP_H__

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "misc.h"

#include "../GPIO_BSP/gpio_bsp.h"
#include "../DMA_BSP/dma_bsp.h"

/*高级定时器只有定时器1和定时器8*/
/*
通道    TIMER1           TIMER8    TIMER2    TIMER5  TIMER3        TIMER4
CH1     PA8/PE9          PC6       PA0/PA15  PA0     PA6/PC6/PB4   PB6/PD12
CH1N    PB13/PA7/PE8     PA7       ****      ****    ****          ****
CH2     PA9/PE11         PC7       PA1/PB3   PA1     PA7/PC7/PB5   PB7/PD13
CH2N    PB14/PB0/PE10    PB0       ****      ****    ****          ****
CH3     PA10/PE13        PC8       PA2/PB10  PA2     PB0/PC8       PB8/PD14
CH3N    PB15/PB1/PE12    PB1       ****      ****    ****          ****
CH4     PA11/PE14        PC9       PA3/PB11  PA3     PB1/PC9       PB9/PD15
ETR     PA12/PE7         PA0       PA0/PA15  ****    PD2           PE0
BKIN    PB12/PA6/PE15    PA6       ****      ****    ****          ****
*/

/*Timer号枚举*/
typedef enum
{
    Timer1 = TIM1_BASE,//高级定时器
    Timer2 = TIM2_BASE,//通用定时器
    Timer3 = TIM3_BASE,//通用定时器
    Timer4 = TIM4_BASE,//通用定时器
    Timer5 = TIM5_BASE,//通用定时器
    Timer6 = TIM6_BASE,//基本定时器
    Timer7 = TIM7_BASE,//基本定时器
    Timer8 = TIM8_BASE,//高级定时器
    Timer9 = TIM9_BASE,
    Timer10 = TIM10_BASE,
    Timer11 = TIM11_BASE,
    Timer12 = TIM12_BASE,
    Timer13 = TIM13_BASE,
    Timer14 = TIM14_BASE,
    Timer15 = TIM15_BASE,
    Timer16 = TIM16_BASE,
    Timer17 = TIM17_BASE,
}WHT_Timer_enum;

/*Timer工作模式*/
typedef enum
{
    Timer_Basic = 0,            //基本定时器
    Timer_Output_Comparison = 1,//输出比较
    Timer_Input_Capture = 2,    //输入捕获
}WHT_Timer_Mode_enum;

/*Timer通道选择*/
typedef enum
{
    Timer_CH1 = 1,//通道1
    Timer_CH2 = 2,//通道2
    Timer_CH3 = 3,//通道3
    Timer_CH4 = 4,//通道4
}WHT_Timer_Channel_enum;

/*Timer定时结构体*/
typedef struct
{
    unsigned char PreemptionPriority;//抢占优先级
    unsigned char SubPriority;       //子优先级
    void (*Interrupt_Callback)(void);//中断回调函数
}WHT_Timer_Basic_Config_t;

/*Timer输出PWM结构体*/
typedef struct
{
    WHT_Timer_Channel_enum Channel;//通道
    void (*Idle_IT_Callback)(void);//空闲中断回调函数

    WHT_GPIO_State_enum Normal_Polarity;       //默认占空比对应的极性
    WHT_GPIO_State_enum Normal_Idle_Polarity;  //默认空闲极性
    WHT_GPIO_Port_enum Normal_Output_GPIO_Port;//PWM输出引脚
    WHT_GPIO_Pin_enum Normal_Output_GPIO_Pin;

    FunctionalState Complementary_Output_State;//互补输出
    WHT_GPIO_State_enum CO_Polarity;           //默认互补占空比对应的极性，一般一样
    WHT_GPIO_State_enum CO_Idle_Polarity;      //默认互补空闲极性,一般相反
    WHT_GPIO_Port_enum CO_Output_GPIO_Port;    //PWM输出引脚或互补PWM输出引脚
    WHT_GPIO_Pin_enum CO_Output_GPIO_Pin;

    FunctionalState Brake_Output_State;        //刹车输出
    WHT_GPIO_Port_enum Brake_Output_GPIO_Port; //刹车输出引脚
    WHT_GPIO_Pin_enum Brake_Output_GPIO_Pin;
}WHT_Timer_PWM_Out_Config_t;

/*Timer捕获输入PWM结构体*/
typedef struct
{
    unsigned char aaa;
}WHT_Timer_PWM_In_Config_t;

/*Timer捕获输入脉宽结构体*/
typedef struct
{
    unsigned char pulse;
}WHT_Timer_Pulse_In_Config_t;


/*Timer配置结构体*/
typedef struct
{
    WHT_Timer_enum Name;            //定时器号
    unsigned int Cycle_ns;          //周期us,范围14~59000000ns
    unsigned short Resolution_Ratio;//分辨率,用户只读取

    WHT_Timer_Mode_enum Mode;  //定时器模式
    unsigned char Config_Count;//配置个数,最大值4
    void* Config;              //配置结构体，可指向配置结构体数组如 &WHT_Timer_PWM_Out_Config_t[0]
    void* Private_Data;        //私有数据
}WHT_Timer_Config_t;

/*定时器回调函数结构体*/
typedef struct
{
    void (*WHT_Config)(const WHT_Timer_Config_t* config);      //自行配置
    void (*WHT_Set_Cycle_ns)(const WHT_Timer_Config_t* config);//设置周期,不适用于基本定时器
    void (*WHT_Set_Duty_Cycle)(const WHT_Timer_Config_t* config, WHT_Timer_Channel_enum channel, float duty_cycle);//设置占空比0.0f~100.0f,不适用于基本定时器
    void (*WHT_Set_Duty_Cycle_DMA)(const WHT_Timer_Config_t* config, WHT_Timer_Channel_enum channel, unsigned char* duty_cycle_buffer, unsigned short duty_cycle_count);//每个周期DMA修改缓存里的占空比
    void (*WHT_Start)(const WHT_Timer_Config_t* config);       //启动计数器
    void (*WHT_Stop)(const WHT_Timer_Config_t* config);        //暂停计数器
}WHT_Timer_BSP_t;


/*全局常量*/
extern const WHT_Timer_BSP_t WHT_Timer_BSP;

#endif // !__TIMER_BSP_H__
