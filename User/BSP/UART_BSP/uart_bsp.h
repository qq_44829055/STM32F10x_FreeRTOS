//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.2
/********************************************************
    说明：
    1、每个外挂的UART模块自行配置UART参数。
    2、每个外挂的UART模块自行提供收发缓存。
    3、MCU自动提供给外挂模块的UART实时状态。
    4、支持普通发数据、中断收发数据、DMA收发数据（串口5没有DMA）。
    5、串口1对应PA9/PA10.串口2对应PA2/PA3.串口3对应PB10/PB11.串口4对应PC10/PC11.串口5对应PC12/PD2。
    5、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __UART_BSP_H__
#define __UART_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "misc.h"
#include "../GPIO_BSP/gpio_bsp.h"
#include "../DMA_BSP/dma_bsp.h"
/*****************************************************/

/*串口号枚举*/
typedef enum
{
    UART_1 = USART1_BASE,//串口1
    UART_2 = USART2_BASE,//串口2
    UART_3 = USART3_BASE,//串口3
    UART_4 = UART4_BASE, //串口4
    UART_5 = UART5_BASE, //串口5
}WHT_UART_enum;

/*串口状态枚举*/
typedef enum
{
    UART_Tx_Idle     = 0,//发送空闲
    UART_Tx_Busy     = 1,//发送忙碌
    UART_Rx_Idle     = 0,//接收空闲
    UART_Rx_Busy     = 1,//接收忙碌
    UART_Rx_Overflow = 2,//接收溢出
}WHT_UART_State_enum;

/*串口奇偶校验枚举*/
typedef enum
{
    UART_Parity_No   = 0,
    UART_Parity_Even = 4,
    UART_Parity_Odd  = 6,
}WHT_UART_Parity_enum;

/*串口停止位个数枚举*/
typedef enum
{
    UART_StopBits_1   = 0,
    UART_StopBits_0_5 = 1,
    UART_StopBits_2   = 2,
    UART_StopBits_1_5 = 3,
}WHT_UART_StopBit_enum;

/*串口信息结构体*/
typedef struct
{
    WHT_UART_State_enum Tx_State; //发送忙碌,只读
    WHT_UART_State_enum Rx_State; //接收忙碌,只读
}WHT_UART_Info_t;

/*串口配置结构体的默认配置值*/
#define WHT_UART_DEFAULT_CONFIG(name)      \
{                                          \
    name,             /* WHT_UART_enum */  \
    115200,           /* 115200 bits/s */  \
    UART_Parity_No,   /* No parity  */     \
    UART_StopBits_1,  /* 1 stopbit */      \
    15,               /* M Priority */     \
    0                 /* S Priority */     \
}

/*串口配置结构体*/
typedef struct
{
    WHT_UART_enum Name;              //串口号
    unsigned int Bps;                //波特率,如(9600)
    WHT_UART_Parity_enum Parity;     //奇偶校验
    WHT_UART_StopBit_enum StopBits;  //停止位个数
    unsigned char PreemptionPriority;//抢占优先级
    unsigned char SubPriority;       //子优先级
}WHT_UART_Config_t;

/*串口缓存结构体*/
typedef struct
{
    unsigned int Rx_Buf_Size;         //接收缓存大小
    unsigned int Rx_Count;            //接收个数
    unsigned int Tx_Count;            //发送个数
    unsigned char* Rx_Buf;            //接收缓存
    unsigned char* Tx_Buf;            //发送缓存
    void (*Rx_Idle_IT_Callback)(void);//接收空闲中断回调函数
    void (*Tx_Idle_IT_Callback)(void);//发送空闲中断回调函数
    volatile WHT_UART_Info_t* Info;   //串口信息
    void* Private_Data;               //私有数据
}WHT_UART_Cache_t;

/*串口回调函数结构体*/
typedef struct
{
    ErrorStatus (*WHT_Register)(const WHT_UART_Config_t* config, const WHT_UART_Cache_t* cache);//注册
    void (*WHT_Config)(const WHT_UART_Config_t* config, const WHT_UART_Cache_t* cache);//每个设备可自行配置
    void (*WHT_Send)(const WHT_UART_Cache_t* cache);             //启动发送数据非中断版
    void (*WHT_Interrupt_Send)(const WHT_UART_Cache_t* cache);   //启动发送数据中断版
    void (*WHT_DMA_Send)(const WHT_UART_Cache_t* cache);         //启动发送数据DMA版
    void (*WHT_Interrupt_Receive)(const WHT_UART_Cache_t* cache);//启动接收数据中断版
    void (*WHT_DMA_Receive)(const WHT_UART_Cache_t* cache);      //启动接收数据DMA版
}WHT_UART_BSP_t;

/*全局常量*/
extern const WHT_UART_BSP_t WHT_UART_BSP;

#endif /*__UART_BSP_H__*/
