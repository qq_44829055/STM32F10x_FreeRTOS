//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、支持手动触发产生中断或停止产生中断。
    2、支持检测或释放中断线路资源。
    3、外部提供中断回调函数。
    4、支持打开或关闭中断线路资源。
    5、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __EXTI_BSP_H__
#define __EXTI_BSP_H__

/*****************************************************/
#include "stm32f10x_exti.h"
#include "misc.h"
#include "../GPIO_BSP/gpio_bsp.h"
/*****************************************************/


/*EXTI枚举*/
typedef enum
{
    Exti_GPIO     = 0,//GPIO跳变
    Exti_PVD      = 1,//PVD输出
    Exti_RTC      = 2,//RTC闹钟事件
    Exti_USB      = 3,//USB唤醒事件
    Exti_Ethernet = 4,//Ethernet唤醒事件
}WHT_EXTI_enum;

/*EXTI引脚端口中断源枚举*/
typedef enum
{
    Exti_Source_GPIOA = GPIO_PortSourceGPIOA,//GPIO端口A
    Exti_Source_GPIOB = GPIO_PortSourceGPIOB,//GPIO端口B
    Exti_Source_GPIOC = GPIO_PortSourceGPIOC,//GPIO端口C
    Exti_Source_GPIOD = GPIO_PortSourceGPIOD,//GPIO端口D
    Exti_Source_GPIOE = GPIO_PortSourceGPIOE,//GPIO端口E
    Exti_Source_GPIOF = GPIO_PortSourceGPIOF,//GPIO端口F
    Exti_Source_GPIOG = GPIO_PortSourceGPIOG,//GPIO端口G
}WHT_EXTI_Source_GPIO_Port_enum;

/*EXTI引脚中断源枚举*/
typedef enum
{
    Exti_Source_Pin0  = GPIO_PinSource0, //引脚0
    Exti_Source_Pin1  = GPIO_PinSource1, //引脚1
    Exti_Source_Pin2  = GPIO_PinSource2, //引脚2
    Exti_Source_Pin3  = GPIO_PinSource3, //引脚3
    Exti_Source_Pin4  = GPIO_PinSource4, //引脚4
    Exti_Source_Pin5  = GPIO_PinSource5, //引脚5
    Exti_Source_Pin6  = GPIO_PinSource6, //引脚6
    Exti_Source_Pin7  = GPIO_PinSource7, //引脚7
    Exti_Source_Pin8  = GPIO_PinSource8, //引脚8
    Exti_Source_Pin9  = GPIO_PinSource9, //引脚9
    Exti_Source_Pin10 = GPIO_PinSource10,//引脚10
    Exti_Source_Pin11 = GPIO_PinSource11,//引脚11
    Exti_Source_Pin12 = GPIO_PinSource12,//引脚12
    Exti_Source_Pin13 = GPIO_PinSource13,//引脚13
    Exti_Source_Pin14 = GPIO_PinSource14,//引脚14
    Exti_Source_Pin15 = GPIO_PinSource15,//引脚15
}WHT_EXTI_Source_GPIO_Pin_enum;

/*EXTI触发模式枚举*/
typedef enum
{
    Trigger_Rising         = EXTI_Trigger_Rising,        //上升沿触发
    Trigger_Falling        = EXTI_Trigger_Falling,       //下降沿触发
    Trigger_Rising_Falling = EXTI_Trigger_Rising_Falling,//上升和下降沿触发
}WHT_EXTI_Trigger_enum;

/*EXTI中断模式枚举*/
typedef enum
{
    Mode_Interrupt = EXTI_Mode_Interrupt,//中断模式
    Mode_Event     = EXTI_Mode_Event,    //事件模式
}WHT_EXTI_Mode_enum;

/*EXTI中断线枚举*/
typedef enum
{
    Exti_Line_Pin0     = EXTI_Line0, //任意端口的引脚0
    Exti_Line_Pin1     = EXTI_Line1, //任意端口的引脚1
    Exti_Line_Pin2     = EXTI_Line2, //任意端口的引脚2
    Exti_Line_Pin3     = EXTI_Line3, //任意端口的引脚3
    Exti_Line_Pin4     = EXTI_Line4, //任意端口的引脚4
    Exti_Line_Pin5     = EXTI_Line5, //任意端口的引脚5
    Exti_Line_Pin6     = EXTI_Line6, //任意端口的引脚6
    Exti_Line_Pin7     = EXTI_Line7, //任意端口的引脚7
    Exti_Line_Pin8     = EXTI_Line8, //任意端口的引脚8
    Exti_Line_Pin9     = EXTI_Line9, //任意端口的引脚9
    Exti_Line_Pin10    = EXTI_Line10,//任意端口的引脚10
    Exti_Line_Pin11    = EXTI_Line11,//任意端口的引脚11
    Exti_Line_Pin12    = EXTI_Line12,//任意端口的引脚12
    Exti_Line_Pin13    = EXTI_Line13,//任意端口的引脚13
    Exti_Line_Pin14    = EXTI_Line14,//任意端口的引脚14
    Exti_Line_Pin15    = EXTI_Line15,//任意端口的引脚15
    Exti_Line_PVD      = EXTI_Line16,//电源监控
    Exti_Line_RTC      = EXTI_Line17,//RTC闹钟
    Exti_Line_USB      = EXTI_Line18,//USB唤醒
    Exti_Line_Ethernet = EXTI_Line19,//以太网
}WHT_EXTI_Line_enum;

/*EXTI中断函数指针结构体*/
typedef void (*WHT_EXTI_BSP_Interrupt_Callback_t)(void);

/*EXTI配置结构体*/
typedef struct
{
    WHT_EXTI_enum Name;                                  //中断类型
    WHT_EXTI_Line_enum Line;                             //中断线路
    WHT_EXTI_Trigger_enum Trigger;                       //中断触发沿
    WHT_EXTI_Mode_enum Mode;                             //Interrupt OR Event
    WHT_EXTI_BSP_Interrupt_Callback_t Interrupt_Callback;//Mode选择中断时此处有效
    WHT_EXTI_Source_GPIO_Port_enum GPIO_Port_Source;     //Name选择GPIO时此处有效
    WHT_EXTI_Source_GPIO_Pin_enum  GPIO_Pin_Source;      //Name选择GPIO时此处有效
}WHT_EXTI_Config_t;

/*EXTI回调函数结构体*/
typedef struct
{
    FlagStatus (*WHT_Get_Line_Mutex_State)(const WHT_EXTI_Config_t* config);           //获取此路中断线是否正在使用
    void (*WHT_Clr_Line_Mutex_State)(const WHT_EXTI_Config_t* config);                 //释放此路中断线资源
    void (*WHT_Config)(const WHT_EXTI_Config_t* config);                               //初始化配置中断线路
    void (*WHT_Line_Cmd)(const WHT_EXTI_Config_t* config, FunctionalState state);      //开启或关闭此中断线路
    void (*WHT_Soft_Interrupt)(const WHT_EXTI_Config_t* config, FunctionalState state);//手动控制产生中断或停止
}WHT_EXTI_BSP_t;

/*EXTI注册API*/
extern void WHT_EXTI_BSP_Register(WHT_EXTI_Line_enum line, WHT_EXTI_BSP_t* bsp);

#endif // !__EXTI_BSP_H__
