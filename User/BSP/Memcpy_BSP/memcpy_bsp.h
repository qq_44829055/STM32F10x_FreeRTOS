//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、最大支持12路并行DMA内存拷贝
    2、无法知道DMA传输是否错误
**********************************************************/

#ifndef __MEMCPY_BSP_H__
#define __MEMCPY_BSP_H__

#include "../../BSP/DMA_BSP/dma_bsp.h"


extern void WHT_DMA_BSP_Memcpy(void* _Dst, void const* _Src, unsigned int _Size);//无法知道传输的状态

#endif // !__MEMCPY_BSP_H__
