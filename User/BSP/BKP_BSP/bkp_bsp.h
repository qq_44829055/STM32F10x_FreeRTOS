//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、支持读写BKP里的数据。
    2、支持复位BKP里的数据。
    3、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __BKP_BSP_H__
#define __BKP_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_bkp.h"
#include "stm32f10x_pwr.h"
#include "stm32f10x_rcc.h"
/*****************************************************/

#define WHT_BKP_BSP_DATA_SIZE   (42*2)

/*BKP缓存结构体*/
typedef struct
{
    unsigned int Buf_Size;           //最大WHT_BKP_BSP_DATA_SIZE字节
    volatile unsigned int Start_Addr;//起始位置,如0 1 2 3 ...
    volatile unsigned int TRx_Count; //读取或写入个数
    unsigned char* TRx_Buf;          //读写缓存,外部提供
}WHT_BKP_Cache_t;

/*BKP回调函数结构体*/
typedef struct
{
    void (*WHT_RW_Start)(void);                     //启动读写
    void (*WHT_Reset)(void);                        //备份数据复位
    void (*WHT_Read)(WHT_BKP_Cache_t* cache);       //读取备份里的数据
    void (*WHT_Write)(const WHT_BKP_Cache_t* cache);//写入数据到备份里
}WHT_BKP_BSP_t;

/*全局常量*/
extern const WHT_BKP_BSP_t WHT_BKP_BSP;

#endif // !__BKP_BSP_H__
