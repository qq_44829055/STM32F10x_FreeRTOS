#ifndef __I2C_BUS_H__
#define __I2C_BUS_H__

#include "i2c_bsp.h"

extern WHT_I2C_BUS_t* WHT_I2C_BUS1;//总线1
extern WHT_I2C_BUS_t* WHT_I2C_BUS2;//总线2
extern char WHT_I2C_BUS1_Init(void);//返回值非0错误
extern char WHT_I2C_BUS2_Init(void);//返回值非0错误


#endif // !__I2C_BUS_H__
