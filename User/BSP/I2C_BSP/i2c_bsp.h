//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.2.0
/********************************************************
    说明：
    1、先注册个总线，最大2个。每个总线上可挂多个I2C设备。每个总线的速率可动态调整。(挂在一起的设备引脚必须一样)
    2、每个外挂的IIC设备自行提供收发缓存。
    3、每个总线均会自动提供给I2C实时状态和互斥锁状态。
    4、支持普通读写、中断读写、DMA读写。
    5、目前只支持Master模式。
    6、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __I2C_BSP_H__
#define __I2C_BSP_H__

#include "../DMA_BSP/dma_bsp.h"

//当使用FSMC功能时，NADV被配置成复用输出，该信号会被默认置位为‘1’，因此，使用硬件I2C时，没有办法把I2C1_SDA信号拉低。体现在硬件I2C从机上就是，从机无法应答。
//从stm32f103的数据手册上可以看出：I2C1_SDA和FSMC_NADV共用同一个引脚，即PB7
#define WHT_I2C1_Name    "WHT_I2C1"
#define WHT_I2C2_Name    "WHT_I2C2"

/*I2C发送或接收枚举*/
typedef enum
{
    I2C_Write = 0,//写
    I2C_Read  = 1,//读
}WHT_I2C_RW_enum;

/*I2C工作模式枚举*/
typedef enum
{
    I2C_Master_Mode = 0,//主模式
    I2C_Slave_Mode  = 1,//从模式
}WHT_I2C_BUS_Mode_enum;

/*I2C状态枚举*/
typedef enum
{
    I2C_No_Error          = 0,//无错误码
    I2C_Slave_No_ACK      = 1,//从设备无应答
    I2C_BUS_Error         = 2,//总线错误
    I2C_ARLO_Error        = 3,//仲裁丢失
    I2C_Rx_Over_Error     = 4,//接收溢出
    I2C_Tx_Send_Error     = 5,//发送超时
    I2C_Other_Error       = 6,//其它错误
    I2C_Rx_Overtime_Error = 7,//超时未接收到数据
}WHT_I2C_BUS_State_enum;

/*软I2C锁枚举*/
typedef enum
{
    I2C_Unlock = 0,
    I2C_Lock   = 1,
}WHT_I2C_BUS_Lock_enum;


/*I2C总线配置结构体*/
typedef struct
{
    unsigned int Sys_Rate_Hz;           //系统频率
    unsigned int I2C_Rate_Hz;           //I2C频率
    unsigned char IT_PreemptionPriority;//中断抢占优先级
    unsigned char IT_SubPriority;       //中断子优先级
    WHT_I2C_BUS_Mode_enum Mode;         //总线工作模式
}WHT_I2C_BUS_Config_t;

/*I2C总线结构体*/
typedef struct
{
    char* Name;                 //总线名字
    WHT_I2C_BUS_Config_t Config;//总线配置信息
    const WHT_I2C_BUS_Lock_enum Mutex;//驱动锁,自动上锁与解锁
    const WHT_I2C_BUS_State_enum State;//错误标志,自动清零
}WHT_I2C_BUS_t;

/*I2C缓存结构体*/
typedef struct
{
    unsigned char Addr_7Bit;  //从设备7位地址
    WHT_I2C_RW_enum Dir;      //读写方向
    unsigned char Cmd_Count;  //命令个数
    unsigned char Cmd[4];     //从设备命令,配合WHT_I2C_Slave_nByte_Cmd_enum
    unsigned int Buffer_Count;//个数
    unsigned char* Buffer;    //缓存
    void (*Read_Finish_IT_CB)(void); //读取完成中断回调函数
    void (*Write_Finish_IT_CB)(void);//写入完成中断回调函数
}WHT_I2C_Cache_t;

/*I2C回调函数结构体*/
typedef struct
{
    WHT_I2C_BUS_t* (*Register)(char* name, WHT_I2C_BUS_Config_t* config, void (*gpio_init_callback)(void));//注册
    void (*Unregister)(WHT_I2C_BUS_t* bus);      //卸载总线
    void (*Set_Rate)(WHT_I2C_BUS_t* bus);        //设置总线速率
    char (*Scan_Mount_Count)(WHT_I2C_BUS_t* bus);//获取总线上挂载多少个设备
    void (*Receive_EN)(WHT_I2C_BUS_t* bus, FunctionalState NewState);  //启动或暂停i2c功能
    void (*Read_Write)(WHT_I2C_BUS_t* bus, WHT_I2C_Cache_t* cache);    //阻塞读写
    void (*Read_Write_IT)(WHT_I2C_BUS_t* bus, WHT_I2C_Cache_t* cache); //中断读写 设备工作在主模式下此函数有效
    void (*Read_Write_DMA)(WHT_I2C_BUS_t* bus, WHT_I2C_Cache_t* cache);//DMA读写  设备工作在主模式下此函数有效
}WHT_I2C_BUS_OPS_t;

/*全局常量*/
extern const WHT_I2C_BUS_OPS_t WHT_I2C_BUS_OPS;

#endif // !__I2C_BSP_H__
