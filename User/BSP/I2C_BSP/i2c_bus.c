#include "i2c_bus.h"
#include "../GPIO_BSP/gpio_bsp.h"


WHT_I2C_BUS_t* WHT_I2C_BUS1;//总线1
WHT_I2C_BUS_t* WHT_I2C_BUS2;//总线2

static const WHT_I2C_BUS_Config_t WHT_I2C_BUS_Config =
{
    .Sys_Rate_Hz = 72000000,
    .I2C_Rate_Hz = 400000,
    .IT_PreemptionPriority = 15,
    .IT_SubPriority = 0,
    .Mode = I2C_Master_Mode,
};

static void WHT_I2C_BUS1_GPIO_Init_Callback(void)
{
    //I2C1的引脚初始化
    WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
    WHT_GPIO_BSP.WHT_Set_State(PortB, Pin6 | Pin7, Hig);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin6 | Pin7, Mode_AF_OD);
}
static void WHT_I2C_BUS2_GPIO_Init_Callback(void)
{
    //I2C2的引脚初始化
    WHT_GPIO_BSP.WHT_Set_Clock(PortB, ENABLE);
    WHT_GPIO_BSP.WHT_Set_State(PortB, Pin10 | Pin11, Hig);
    WHT_GPIO_BSP.WHT_Set_Mode(PortB, Pin10 | Pin11, Mode_AF_OD);
}


char WHT_I2C_BUS1_Init(void)
{ 
    WHT_I2C_BUS1 = WHT_I2C_BUS_OPS.Register(WHT_I2C1_Name, (WHT_I2C_BUS_Config_t*)&WHT_I2C_BUS_Config, WHT_I2C_BUS1_GPIO_Init_Callback);
    return WHT_I2C_BUS1 == (void*)0 ? -1 : 0;
}

char WHT_I2C_BUS2_Init(void)
{
    WHT_I2C_BUS2 = WHT_I2C_BUS_OPS.Register(WHT_I2C2_Name, (WHT_I2C_BUS_Config_t*)&WHT_I2C_BUS_Config, WHT_I2C_BUS2_GPIO_Init_Callback);
    return WHT_I2C_BUS2 == (void*)0 ? -1 : 0;
}
