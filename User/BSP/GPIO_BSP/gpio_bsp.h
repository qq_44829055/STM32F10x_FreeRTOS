//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、GPIO端口时钟开启或关闭。
    2、GPIO引脚模式配置。
    3、GPIO引脚状态获取或设置。
    4、GPIO端口16个引脚状态获取或设置。
    5、位操作GPIO端口引脚(操作几个引脚的话很推荐此方法)。
    6、基于STM32F10X标准库V3.6.0
**********************************************************/

#ifndef __GPIO_BSP_H__
#define __GPIO_BSP_H__

/*****************************************************/
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
/*****************************************************/

#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif

/*GPIO端口枚举*/
typedef enum
{
    PortA = (uint32_t)GPIOA,//端口A
    PortB = (uint32_t)GPIOB,//端口B
    PortC = (uint32_t)GPIOC,//端口C
    PortD = (uint32_t)GPIOD,//端口D
    PortE = (uint32_t)GPIOE,//端口E
    PortF = (uint32_t)GPIOF,//端口F
    PortG = (uint32_t)GPIOG,//端口G
}WHT_GPIO_Port_enum;

/*GPIO引脚号枚举*/
typedef enum
{
    Pin0  = GPIO_Pin_0, //引脚0号
    Pin1  = GPIO_Pin_1, //引脚1号
    Pin2  = GPIO_Pin_2, //引脚2号
    Pin3  = GPIO_Pin_3, //引脚3号
    Pin4  = GPIO_Pin_4, //引脚4号
    Pin5  = GPIO_Pin_5, //引脚5号
    Pin6  = GPIO_Pin_6, //引脚6号
    Pin7  = GPIO_Pin_7, //引脚7号
    Pin8  = GPIO_Pin_8, //引脚8号
    Pin9  = GPIO_Pin_9, //引脚9号
    Pin10 = GPIO_Pin_10,//引脚10号
    Pin11 = GPIO_Pin_11,//引脚11号
    Pin12 = GPIO_Pin_12,//引脚12号
    Pin13 = GPIO_Pin_13,//引脚13号
    Pin14 = GPIO_Pin_14,//引脚14号
    Pin15 = GPIO_Pin_15,//引脚15号
    PinAll= GPIO_Pin_All,//所有引脚
}WHT_GPIO_Pin_enum;

/*GPIO寄存器偏移地址枚举*/
typedef enum
{
    GPIO_ODR_Offset_Address = 0x0c,//输出寄存器偏移量
    GPIO_IDR_Offset_Address = 0x08,//输入寄存器偏移量
    GPIO_BSRR_Offset_Address= 0x10,//复位寄存器偏移量
    GPIO_BRR_Offset_Address = 0x14,//置位寄存器偏移量
}WHT_GPIO_Bit_Offset_Address_enum;

/*GPIO状态枚举*/
typedef enum
{
    Low = 0,      //低电平
    Hig = 1,      //高电平
    Bit_Reset = 0,//低电平
    Bit_Set   = 1,//高电平
}WHT_GPIO_State_enum;

/*GPIO工作模式枚举*/
typedef enum
{
    Mode_Ain         = (uint8_t)GPIO_Mode_AIN,        //模拟输入
    Mode_IN_FLOATING = (uint8_t)GPIO_Mode_IN_FLOATING,//浮空输入
    Mode_IPD         = (uint8_t)GPIO_Mode_IPD,        //下拉输入
    Mode_IPU         = (uint8_t)GPIO_Mode_IPU,        //上拉输入
    Mode_Out_OD      = (uint8_t)GPIO_Mode_Out_OD,     //开漏输出
    Mode_Out_PP      = (uint8_t)GPIO_Mode_Out_PP,     //推挽输出
    Mode_AF_OD       = (uint8_t)GPIO_Mode_AF_OD,      //开漏复用输出
    Mode_AF_PP       = (uint8_t)GPIO_Mode_AF_PP,      //推挽复用输出
}WHT_GPIO_Mode_enum;

/*GPIO回调函数结构体*/
typedef struct
{
    void (*WHT_Set_Clock)(WHT_GPIO_Port_enum portx, FunctionalState state);                                     //使能端口时钟
    void (*WHT_Set_Mode)(WHT_GPIO_Port_enum portx, uint16_t pinx, WHT_GPIO_Mode_enum mode);                     //设置工作模式 pinx for WHT_GPIO_Pin_enum
    void (*WHT_Set_State)(WHT_GPIO_Port_enum portx, uint16_t pinx, WHT_GPIO_State_enum state);                  //设置引脚状态 pinx for WHT_GPIO_Pin_enum
    WHT_GPIO_State_enum(*WHT_Get_State)(WHT_GPIO_Port_enum portx, WHT_GPIO_Pin_enum pinx);                      //获取引脚状态
    uint16_t (*WHT_Get_Prot_Value)(WHT_GPIO_Port_enum portx);                                                   //获取一组引脚状态
    void (*WHT_Set_Prot_Value)(WHT_GPIO_Port_enum portx, uint16_t port_value);                                  //设置一组引脚状态
    void (*WHT_Config_Bit_Output)(WHT_GPIO_Port_enum portx, WHT_GPIO_Pin_enum pinx, volatile uint32_t** output);//注册位操作
    void (*WHT_Config_Bit_Input)(WHT_GPIO_Port_enum portx, WHT_GPIO_Pin_enum pinx, volatile uint32_t** input);  //注册位操作
}WHT_GPIO_BSP_t;

/*全局常量*/
extern const WHT_GPIO_BSP_t WHT_GPIO_BSP;

#endif /*__GPIO_BSP_H__*/
