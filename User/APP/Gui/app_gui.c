//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.0.2
#include "app_gui.h"

/****************变量************************/
static const WHT_GUI_Position_Info_t WHT_GUI_Position_Info =
{
    {0,110},
    {1,200},
    {2,200},
    {3,200},
    {4,150},
    {5,150},
    {6,0},
};
/****************变量************************/


static void WHT_GUI_APP_NAME(void)
{
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_BLUE;
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.NAME.Row, WHT_GUI_Position_Info.NAME.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(My_Name);
}
static void WHT_GUI_APP_LCD_ID(void)
{
    uint16_t Temp_Buf[9] = {0};

    WHT_LCD_Driver.WHT_LCD_Get_Reg_Value(LCD_Read_ID,Temp_Buf,4);
    sprintf((char*)&Temp_Buf[4],"0x%.2x%.2x%.2x\r\n",Temp_Buf[1],Temp_Buf[2],Temp_Buf[3]);
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_BLACK;
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.LCD_ID.Row, WHT_GUI_Position_Info.LCD_ID.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts((char*)&Temp_Buf[4]);
}
static void WHT_GUI_APP_Version(void)
{
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_BLACK;
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Hardware_Version.Row, WHT_GUI_Position_Info.Hardware_Version.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(HARD_VERSION);
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Software_Version.Row, WHT_GUI_Position_Info.Software_Version.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(SOFT_VERSION);
}
static void WHT_GUI_APP_Writer_Info(const char* name, const char* qq)
{
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_GREEN;
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Writer.Row, WHT_GUI_Position_Info.Writer.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(name);
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.QQ.Row, WHT_GUI_Position_Info.QQ.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(qq);
}

static void WHT_GUI_APP_Reset(void)
{
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_BLACK;
    WHT_LCD_Driver.WHT_LCD_Clear_Row(WHT_GUI_Position_Info.Error_Info.Row);
    WHT_LCD_Driver.WHT_LCD_Clear_Row(WHT_GUI_Position_Info.Error_Info.Row + 1);
    WHT_LCD_Driver.WHT_LCD_Clear_Row(WHT_GUI_Position_Info.Error_Info.Row + 2);
}
static void WHT_GUI_APP_Init(void)
{
    WHT_GUI_APP_Reset();
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.LCD_ID.Row, 0);
    WHT_LCD_Driver.WHT_LCD_Puts("LCD ILI9341 ID:");
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Hardware_Version.Row, 0);
    WHT_LCD_Driver.WHT_LCD_Puts("Hardware_Version:");
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Software_Version.Row, 0);
    WHT_LCD_Driver.WHT_LCD_Puts("Software_Version:");
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Writer.Row, 0);
    WHT_LCD_Driver.WHT_LCD_Puts("Writer:");
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.QQ.Row, 0);
    WHT_LCD_Driver.WHT_LCD_Puts("QQ:");
}

static void WHT_GUI_APP_Other_Info(const char* other_info)
{
    WHT_LCD_Driver.WHT_LCD_Info->Font_Color = RGB565_RED;
    WHT_LCD_Driver.WHT_LCD_Set_Font_Row(WHT_GUI_Position_Info.Error_Info.Row, WHT_GUI_Position_Info.Error_Info.Offset_Pixels);
    WHT_LCD_Driver.WHT_LCD_Puts(other_info);
}

void WHT_GUI_APP_Init_Register(WHT_GUI_App_t* app)
{
    WHT_LCD_Driver.WHT_LCD_Init();
    WHT_LCD_Driver.WHT_LCD_Info->BK_Color = RGB565_WHITE;
    WHT_GUI_APP_Init();
    WHT_GUI_APP_NAME();
    WHT_GUI_APP_Version();
    WHT_GUI_APP_LCD_ID();

    app->WHT_GUI_Reset       = WHT_GUI_APP_Reset;
    app->WHT_GUI_Writer_Info = WHT_GUI_APP_Writer_Info;
    app->WHT_GUI_Other_Info  = WHT_GUI_APP_Other_Info;
}
