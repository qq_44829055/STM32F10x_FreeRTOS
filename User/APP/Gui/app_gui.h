//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.0.1
#ifndef __GUI_H__
#define __GUI_H__

#include "../../Driver/Lcd/lcd_driver.h"

/*   LCD大概显示示例
                 WHT
LCD ILI9341 ID:      0x009341
Hardware_Version:    V1.1.1
Software_Version:    V2.2.2
Writer:              WHT
QQ:                  1126471088
*/

#define My_Name         "WHT"
#define HARD_VERSION    "V1.1.1"
#define SOFT_VERSION    "V2.2.2"

typedef struct
{
    uint8_t Row;
    uint8_t Offset_Pixels;
}WHT_GUI_Position_t;

typedef struct
{
    WHT_GUI_Position_t NAME;            //Name
    WHT_GUI_Position_t LCD_ID;          //屏幕ID
    WHT_GUI_Position_t Hardware_Version;//硬件版本
    WHT_GUI_Position_t Software_Version;//软件版本
    WHT_GUI_Position_t Writer;          //作者
    WHT_GUI_Position_t QQ;              //QQ
    WHT_GUI_Position_t Error_Info;      //错误信息
}WHT_GUI_Position_Info_t;

typedef struct
{
    void (*WHT_GUI_Reset)(void);
    void (*WHT_GUI_Writer_Info)(const char* name, const char* qq);
    void (*WHT_GUI_Other_Info)(const char* other_info);
    /*
    add other code
    */
}WHT_GUI_App_t;

extern void WHT_GUI_APP_Init_Register(WHT_GUI_App_t* app);

#endif /*__GUI_H__*/
