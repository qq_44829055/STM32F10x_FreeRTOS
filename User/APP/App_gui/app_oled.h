//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.0.1
#ifndef __APP_OLED_H__
#define __APP_OLED_H__

#include "../../../U8G2/u8g2.h"
#include "../../Delay/delay.h"
#include "../../Driver/OLED/oled_driver.h"


extern TaskHandle_t WHT_OLED_Task_Handle;//任务句柄
extern void WHT_OLED_Install(uint8_t task_priority);


#endif /*__APP_OLED_H__*/
