#include "app_modbus.h"


static void WHT_ModBus_Task(void* pvParameters)
{
    WHT_FreeModBus_Lite_Init(MB_RTU,  MB_SAMPLE_TEST_SLAVE_ADDR);

    for (; ;)
    {
        WHT_FreeModBus_Lite_Poll();
    }
}

void WHT_ModBus_Install(uint8_t task_priority)
{
	/* 更新保持寄存器值 */
	WHT_SRegHold_Buffer[0] = 1;
	WHT_SRegHold_Buffer[1] = 2;
	WHT_SRegHold_Buffer[2] = 3;
	WHT_SRegHold_Buffer[3] = 4;
	
	/* 更新输入寄存器值 */
	WHT_SRegIn_Buffer[0];
	WHT_SRegIn_Buffer[1];
	WHT_SRegIn_Buffer[2];
	WHT_SRegIn_Buffer[3];
	
	/* 更新线圈 */
	WHT_SCoil_Buffer[0] = 255;
	WHT_SCoil_Buffer[1] = 0x0f;
	WHT_SCoil_Buffer[2] = 0xff;
	WHT_SCoil_Buffer[3] = 0xff;
	
	/* 离散输入变量 */
	WHT_SDiscIn_Buffer[0];
	WHT_SDiscIn_Buffer[1];

    xTaskCreate(WHT_ModBus_Task, "WHT_ModBus_Task", 256, NULL, task_priority, NULL);
}
