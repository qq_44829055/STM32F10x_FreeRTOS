#ifndef __APP_MODBUS_H__
#define __APP_MODBUS_H__

#include "../../FreeModBus_Lite/user_freemodbus_lite_demo.h"

#include "FreeRTOS.h"
#include "task.h"

extern void WHT_ModBus_Install(uint8_t task_priority);

#endif
