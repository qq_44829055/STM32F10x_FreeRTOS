//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、灯的个性化控制，响应时间小于10毫秒。
    2、支持独立灯控制。
    3、矩阵灯暂不支持，如果要支持，灯的更新频率要在几毫秒有点浪费CPU。
    4、支持常规亮和灭，支持周期闪烁，支持SOS报警闪烁。
    5、默认独立灯支持3个，矩阵灯支持5行5列。
**********************************************************/
#ifndef __APP_LED_H__
#define __APP_LED_H__

#include "FreeRTOS.h"
#include "task.h"
#include "../../BSP/GPIO_BSP/gpio_bsp.h"


#define LED_Task_Update_Rate   (10 / portTICK_RATE_MS)//LED任务状态更新率

#define Independent_Lamp_Enable //独立灯
//#define Matrix_Lamp_Enable      //矩阵灯

#define Independent_Lamp_Count  3 //独立灯个数
#define Matrix_Lamp_Row         0 //矩阵灯行数
#define Matrix_Lamp_Col         0 //矩阵灯列数
#define Matrix_Lamp_Count       (Matrix_Lamp_Row*Matrix_Lamp_Col)         //矩阵灯总个数
#define LED_Sum_Count           (Independent_Lamp_Count+Matrix_Lamp_Count)//独立灯+矩阵灯总个数


//独立LED
#define Independent_LED0_GPIO_Port   PortB
#define Independent_LED0_GPIO_Pin    Pin5

#define Independent_LED1_GPIO_Port   PortB
#define Independent_LED1_GPIO_Pin    Pin0

#define Independent_LED2_GPIO_Port   PortB
#define Independent_LED2_GPIO_Pin    Pin1

//矩阵LED
#define Matrix_LED_ROW0_GPIO_Port    PortB
#define Matrix_LED_ROW0_GPIO_Pin     Pin1

#define Matrix_LED_ROW1_GPIO_Port    PortB
#define Matrix_LED_ROW1_GPIO_Pin     Pin1

#define Matrix_LED_ROW2_GPIO_Port    PortB
#define Matrix_LED_ROW2_GPIO_Pin     Pin1

#define Matrix_LED_ROW3_GPIO_Port    PortB
#define Matrix_LED_ROW3_GPIO_Pin     Pin1

#define Matrix_LED_ROW4_GPIO_Port    PortB
#define Matrix_LED_ROW4_GPIO_Pin     Pin1

#define Matrix_LED_COL0_GPIO_Port    PortB
#define Matrix_LED_COL0_GPIO_Pin     Pin1

#define Matrix_LED_COL1_GPIO_Port    PortB
#define Matrix_LED_COL1_GPIO_Pin     Pin1

#define Matrix_LED_COL2_GPIO_Port    PortB
#define Matrix_LED_COL2_GPIO_Pin     Pin1

#define Matrix_LED_COL3_GPIO_Port    PortB
#define Matrix_LED_COL3_GPIO_Pin     Pin1

#define Matrix_LED_COL4_GPIO_Port    PortB
#define Matrix_LED_COL4_GPIO_Pin     Pin1


typedef enum
{
    LED0 = 0,
    LED1,
    LED2,
    LED3,
    LED4,
    LED5,
    LED6,
    LED7,
    LED8,
    LED9,
    LED10,
    LED11,
    LED12,
    LED13,
    LED14,
    LED15,
    LED16,
    LED17,
    LED18,
    LED19,
    LED_ALL,
}WHT_LED_Name_enum;

typedef enum
{
    LED_OFF = 0,//熄灭
    LED_ON = 1, //点亮
}WHT_LED_State_enum;

typedef enum
{
    LED_5Hz   = 200,      //5Hz
    LED_2Hz   = 500,      //2Hz
    LED_1Hz   = 1000,     //1Hz
    LED_0_5Hz = 2000,     //0.5Hz
    LED_0_2Hz = 5000,     //0.2Hz
    LED_0_1Hz = 10000,    //0.1Hz
}WHT_LED_Flash_Freq_enum;

typedef struct
{
    WHT_LED_Name_enum Name;
    WHT_LED_State_enum State;
}WHT_LED_Routine_Config_t;

typedef struct
{
    WHT_LED_Name_enum Name;
    WHT_LED_State_enum End_State;//结束闪烁后灯的状态
    unsigned short Flash_Count;  //闪烁次数,0表示持续闪烁
    unsigned short LED_ON_Time;  //点亮时间,小于等于Flash_Cycle
    unsigned short Flash_Cycle;  //闪烁周期,可以是WHT_LED_Flash_Freq_enum
}WHT_LED_Flash_Config_t;


typedef struct
{
    void (*LED_Task_Install)(unsigned char task_priority);                     //安装LED任务
    void (*LED_Task_Uninstall)(void);                                          //卸载LED任务
    void (*LED_Set_Routine_New)(WHT_LED_Name_enum name, WHT_LED_State_enum state);//常规熄灭点亮
    void (*LED_Set_Flash_New)(WHT_LED_Name_enum name, unsigned short flash_count, unsigned short led_on_time, unsigned short flash_cycle, WHT_LED_State_enum end_state);//闪烁
    void (*LED_Routine_Config)(const WHT_LED_Routine_Config_t* routine_config);//常规熄灭点亮
    void (*LED_Flash_Config)(const WHT_LED_Flash_Config_t* flash_config);      //闪烁
    void (*LED_SOS_Start)(const WHT_LED_Name_enum led_number);                 //开启SOS
    void (*LED_SOS_End)(const WHT_LED_Name_enum led_number);                   //结束SOS
}WHT_LED_t;

extern WHT_LED_t WHT_LED_Handle;//LED的控制句柄

#endif
