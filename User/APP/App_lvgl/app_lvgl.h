#ifndef __APP_LVGL_H__
#define __APP_LVGL_H__

#include <string.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"


#include "lvgl.h"                // 它为整个LVGL提供了更完整的头文件引用
#include "lv_port_disp.h"        // LVGL的显示支持
#include "lv_port_indev.h"       // LVGL的触屏支持
#include "../../LVGL/demos/keypad_encoder/lv_demo_keypad_encoder.h"

extern void WHT_LVGL_Install(uint8_t task_priority);

#endif
