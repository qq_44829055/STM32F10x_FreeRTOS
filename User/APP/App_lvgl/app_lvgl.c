#include "app_lvgl.h"
#include "../../../LVGL/demos/lv_demos.h"

void vApplicationTickHook(void)
{
    /*RTOS给LVGL提供时基*/
    lv_tick_inc(portTICK_RATE_MS);
}

static void WHT_LVGL_Task(void* pvParameters)
{
    lv_init();                             // LVGL 初始化
    lv_port_disp_init();                   // 注册LVGL的显示任务
    lv_port_indev_init();                  // 注册LVGL的触屏检测任务
#if 1
    vTaskDelay(3000);
    // 按钮
    lv_obj_t *myBtn = lv_btn_create(lv_scr_act());                               // 创建按钮; 父对象：当前活动屏幕
    lv_obj_set_pos(myBtn, 10, 10);                                               // 设置坐标
    lv_obj_set_size(myBtn, 120, 50);                                             // 设置大小
   
    // 按钮上的文本
    lv_obj_t *label_btn = lv_label_create(myBtn);                                // 创建文本标签，父对象：上面的btn按钮
    lv_obj_align(label_btn, LV_ALIGN_CENTER, 0, 0);                              // 对齐于：父对象
    lv_label_set_text(label_btn, "My Test");                                     // 设置标签的文本
 
    // 独立的标签
    lv_obj_t *myLabel = lv_label_create(lv_scr_act());                           // 创建文本标签; 父对象：当前活动屏幕
    lv_label_set_text(myLabel, "Hello world!");                                  // 设置标签的文本
    lv_obj_align(myLabel, LV_ALIGN_CENTER, 0, 0);                                // 对齐于：父对象
    lv_obj_align_to(myBtn, myLabel, LV_ALIGN_OUT_TOP_MID, 0, -20);               // 对齐于：某对象
#else
    lv_demo_widgets(); /* 测试的 demo */
#endif
    

	TickType_t xLastWakeTime;
	const TickType_t xPeriod = pdMS_TO_TICKS( 10 );
	// 使用当前时间初始化变量xLastWakeTime ,注意这和vTaskDelay()函数不同 
	xLastWakeTime = xTaskGetTickCount();  
	for(;;)
	{
        
		/* 调用系统延时函数,周期性阻塞5ms */    
		vTaskDelayUntil( &xLastWakeTime, xPeriod);
		lv_task_handler();
	}
}



void WHT_LVGL_Install(uint8_t task_priority)
{
    xTaskCreate(WHT_LVGL_Task, "WHT_LVGL_Task", 768, NULL, task_priority, NULL);
}
