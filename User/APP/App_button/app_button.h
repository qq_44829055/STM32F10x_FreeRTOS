#ifndef __APP_BUTTON_H__
#define __APP_BUTTON_H__

#include <string.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "../../Driver/Button/button_driver.h"


#define Button_Task_Scan_Rate    20//按键任务扫描速率毫秒

typedef enum
{
    Short_Press_Min_Time = 40, //短按最小时长毫秒
    Long_Press_Min_Time = 1000,//长按最小时长毫秒
    Keep_Press_Min_Time = 3000,//一直按最小时长毫秒
    Button_Error        = 10000,//按键错误
}WHT_Button_Time_enum;

typedef struct
{
    void (*Button_Task_Install)(uint8_t task_priority);
    void (*Button_Task_Uninstall)(void);
}WHT_Button_t;

extern void WHT_Button_Init(uint8_t task_priority, WHT_Button_t* button);

#endif // !__APP_BUTTON_H__
