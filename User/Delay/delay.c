#include "delay.h"
//基于系统时钟72MHz

void WHT_Delay_Count_2x(volatile unsigned int count)
{
	while (count--);
}
void WHT_Delay_us(volatile unsigned int us)
{
	us = 9 * us;
	while (us--);
}
void WHT_Delay_ms(volatile unsigned int ms)
{
	ms = 8000 * ms;
	while (ms--);
}
