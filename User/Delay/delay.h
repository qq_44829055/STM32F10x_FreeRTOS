#ifndef __DELAY_H__
#define __DELAY_H__

extern void WHT_Delay_Count_2x(volatile unsigned int count);
extern void WHT_Delay_us(volatile unsigned int us);
extern void WHT_Delay_ms(volatile unsigned int ms);

#endif /*__DELAY_H__*/
