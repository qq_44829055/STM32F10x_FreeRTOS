//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
#ifndef __ESP8266_DRIVER_H__
#define __ESP8266_DRIVER_H__


#include "../../BSP/UART_BSP/uart_bsp.h"
#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../Delay/delay.h"
#include "string.h"

/* ESP8266 RST脚 */
#define ESP8266_RST_GPIO_PORT    PortB
#define ESP8266_RST_GPIO_PIN	 Pin9
/* ESP8266 EN控制脚 */
#define ESP8266_EN_GPIO_PORT     PortB
#define ESP8266_EN_GPIO_PIN		 Pin8


/*开启任务延迟，特别大批量数据发送*/
#define ESP8266_Task_Delay

#define ESP8266_Max_Buf_Count      512//一次性扫描周边8个设备，内存大点以防止接收溢出


typedef enum
{
	AT_CMD_Fail                   = 0, //AT命令错误
}WHT_WIFI_Error_Code_enum;

typedef enum
{
	WIFI_AT_Execute_Test = 0,  /* "AT\r\n"          测试指令 */
	WIFI_AT_Execute_Reset,     /* "AT+RST\r\n"      模块复位 */
	WIFI_AT_Execute_CWQAP,     /* "AT+CWQAP\r\n"    退出与AP的连接 */
	WIFI_AT_Execute_CIPCLOSE,  /* "AT+CIPCLOSE\r\n" 单路连接时使用，关闭TCP或UDP */
	WIFI_AT_Execute_CIPSEND,   /* "AT+CIPSEND\r\n"  进入透传，会先返回字符"\r\n>"，芯片收到一个包"+++"后退出，每包数据以20ms间隔区分 */
}WHT_WIFI_AT_Execute_Cmd_enum;//AT执行指令

typedef enum
{
	WIFI_AT_Query_GMR = 0,     /* "AT+GMR\r\n"       查询软件版本信息 */
	WIFI_AT_Query_CWMODE,      /* "AT+CWMODE?\r\n"   查询当前模块的模式 */
	WIFI_AT_Query_CWJAP,       /* "AT+CWJAP?\r\n"    返回当前选择的AP的SSID*/         
	WIFI_AT_Query_CWLAP,       /* "AT+CWLAP\r\n"     查询当前可用AP */
	WIFI_AT_Query_CWSAP,       /* "AT+CWSAP?\r\n"    查询当前AP参数 */
	WIFI_AT_Query_CWLIF,       /* "AT+CWLIF\r\n"     查询已接入设备的IP */
	WIFI_AT_Query_CIPSTATUS,   /* "AT+CIPSTATUS\r\n" 查询当前模块的连接状态和连接参数 */
	WIFI_AT_Query_CIFSR,       /* "AT+CIFSR\r\n"     查询本地IP地址 */
	WIFI_AT_Query_CIPMODE,     /* "AT+CIPMODE?\r\n"  查询模块传输模式 */
	WIFI_AT_Query_CIPSTO,      /* "AT+CIPSTO?\r\n"   查询服务器超时时间 */
}WHT_WIFI_AT_Query_Cmd_enum;

typedef enum
{
	WIFI_AT_Set_CWMODE = 0, /* "AT+CWMODE=xx\r\n"               设置当前模块的模式，重启生效 */
	WIFI_AT_Set_CWJAP,      /* "AT+CWJAP=xx,xx\r\n"             加入AP */
	WIFI_AT_Set_CWSAP,      /* "AT+CWSAP=ssid,pwd,chl,ecn\r\n"  设置AP模式下的参数 */
	WIFI_AT_Set_CIPSTART,   /* "AT+CIPSTART=type,addr,port\r\n" 建立TCP连接或注册UDP端口号，单连接和多连接 */
	WIFI_AT_Set_CIPSEND,    /* "AT+CIPSEND=xx\r\n"              发送指定长度的数据，单连接和多连接 会先返回字符"\r\n>",之后发送数据即可*/
	WIFI_AT_Set_CIPCLOSE,   /* "AT+CIPCLOSE=id\r\n"             id=5则关闭关闭TCP或UDP */
	WIFI_AT_Set_CIPMUX,     /* "AT+CIPMUX=mode\r\n"             启动多连接，单路连接或多路连接，只有当连接都断开后才能更改，如果开启过server需要重启模块 */
	WIFI_AT_Set_CIPSERVER,  /* "AT+CIPSERVER=mode,port\r\n"     关闭server需要重启,开启server需要"AT+CIPMUX=1"时才能开启服务器 */
	WIFI_AT_Set_CIPMODE,    /* "AT+CIPMODE=mode\r\n"            设置模块传输模式 */
	WIFI_AT_Set_CIPSTO,     /* "AT+CIPSTO=time\r\n"             设置服务器超时时间,0~28800 */
}WHT_WIFI_AT_Set_Cmd_enum;

typedef	enum
{
	WHT_WIFI_Station = 0,  //Station 模式
	WHT_WIFI_AP,           //AP 模式
	WHT_WIFI_Station_AP,   //AP 兼 Station 模式
}WHT_WIFI_Net_Mode_enum;

typedef enum
{
	WHT_WIFI_OPEN = 0,    //不加密
	WHT_WIFI_WEP,         //WEP加密
	WHT_WIFI_WPA_PSK,     //WPA_PSK加密
	WHT_WIFI_WPA2_PSK,    //WPA2_PSK加密
	WHT_WIFI_WPA_WPA2_PSK,//WPA_WPA2_PSK加密
}WHT_WIFI_ECN_enum;

typedef enum
{
	WHT_WIFI_Ch1 = 0,
	WHT_WIFI_Ch2,
	WHT_WIFI_Ch3,
	WHT_WIFI_Ch4,
	WHT_WIFI_Ch5,
	WHT_WIFI_Ch6,
	WHT_WIFI_Ch7,
	WHT_WIFI_Ch8,
	WHT_WIFI_Ch9,
	WHT_WIFI_Ch10,
	WHT_WIFI_Ch11,
	WHT_WIFI_Ch12,
	WHT_WIFI_Ch13,
	WHT_WIFI_Ch14,//日本有
}WHT_WIFI_CHx_enum;

typedef enum
{
	WHT_WIFI_TCP = 0,
	WHT_WIFI_UDP,
}WHT_WIFI_Net_Type_enum;

typedef enum
{
	WHT_WIFI_Multiple_ID0 = 0,
	WHT_WIFI_Multiple_ID1,
	WHT_WIFI_Multiple_ID2,
	WHT_WIFI_Multiple_ID3,
	WHT_WIFI_Multiple_ID4,
	WHT_WIFI_Single_ID0,
}WHT_WIFI_Net_ID_enum;


typedef struct
{
	void (*WHT_WIFI_Reset)(void);                                                                              //获取WIFI连接状态
	ErrorStatus (*WHT_WIFI_Config)(void);                                                                      //WIFI初始化配置
	ErrorStatus (*WHT_WIFI_Execute_Cmd)(WHT_WIFI_AT_Execute_Cmd_enum cmd_code);                                //AT执行指令操作
	ErrorStatus (*WHT_WIFI_Query_Cmd)(WHT_WIFI_AT_Query_Cmd_enum cmd_code, char** rx_buf, uint32_t* rx_length);//AT查询指令操作
	ErrorStatus (*WHT_WIFI_Set_Cmd)(WHT_WIFI_AT_Set_Cmd_enum cmd_code, char* tx_buf, uint32_t tx_length);      //AT设置指令操作
	ErrorStatus (*WHT_Transparent_Transmission_Send)(const char* tx_buf, uint32_t tx_length);                  //透传发送
    ErrorStatus (*WHT_Transparent_Transmission_Receive)(char** rx_buf, uint32_t* rx_length);                   //透传接收
}WHT_ESP8266_Driver_t;

extern void WHT_ESP8266_Driver_Register(WHT_ESP8266_Driver_t* driver);

#ifdef ESP8266_Task_Delay
#include "FreeRTOS.h"
#include "task.h"
#endif // ESP8266_Task_Delay

#endif /*__ESP8266_DRIVER_H__*/
