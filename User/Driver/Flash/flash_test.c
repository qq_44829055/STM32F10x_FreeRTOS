#include "flash_driver.h"
#include "../Debug_Driver/debug_driver.h"




void FLASH_TEST_FUNC(void)
{
    WHT_Flash_Driver.WHT_Init();

    uint32_t id = WHT_Flash_Driver.WHT_Read_JEDEC_ID();
    printf ("FLASH ID = 0x%x\r\n", id);


    WHT_Flash_Driver.WHT_Erase(Sector_Erase_4KB, 0, sector0);

    /*清零*/
    memset(WHT_Flash_Driver.WHT_TRx_Buffer, 0xff, 55);
    WHT_Flash_Driver.WHT_Write_Data(0, 55);
    WHT_Flash_Driver.WHT_Read_Data(0, 55);
    WHT_Debug_Driver.WHT_Printf(WHT_Flash_Driver.WHT_TRx_Buffer, 55);


    /*写字符串*/
    memcpy(WHT_Flash_Driver.WHT_TRx_Buffer, "user 1.Erase 2.Wite. is my w25q128 test\r\n", 42);
    WHT_Flash_Driver.WHT_Write_Data(0, strlen(WHT_Flash_Driver.WHT_TRx_Buffer));
    WHT_Flash_Driver.WHT_Read_Data(0, 50);
    WHT_Debug_Driver.WHT_Printf(WHT_Flash_Driver.WHT_TRx_Buffer, 50);
    
}
