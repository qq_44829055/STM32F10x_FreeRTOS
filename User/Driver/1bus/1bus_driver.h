//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
	1、基于DS18B20单线通信。
	2、GPIO引脚必须要有上拉电阻。
	3、基于GPIO_BSP驱动。
    4、除搜索ROM和搜索报警未完善外，其它均已完成。
	5、跳过ROM匹配，通信时长会减小很多。
**********************************************************/

#ifndef __1BUS_DRIVER_H__
#define __1BUS_DRIVER_H__


#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../Delay/delay.h"


/*野火指南者对应PE6引脚*/

#if  1
#define _1BUS_LOGI   printf
#else
#define _1BUS_LOGI(...)
#endif

/*开启任务延迟，特别大批量数据发送*/
#define _1BUS_RTOS_Task_Delay


/*精度选择枚举*/
typedef enum
{
	Accuracy_9Bit  = 0x1f,//9位精度
	Accuracy_10Bit = 0x3f,//10位精度
	Accuracy_11Bit = 0x5f,//11位精度
	Accuracy_12Bit = 0x7f,//12位精度
}WHT_DS18B20_Accuracy_Sel_enum;

/*精度对应的延迟枚举*/
typedef enum
{
	Accuracy_9Bit_Delay_ms  = 94, //9位精度,转换时间93.75ms
	Accuracy_10Bit_Delay_ms = 188,//10位精度,转换时间187.5ms
	Accuracy_11Bit_Delay_ms = 375,//11位精度,转换时间375ms
	Accuracy_12Bit_Delay_ms = 750,//12位精度,转换时间750ms
}WHT_DS18B20_Accuracy_Delay_enum;

/*供电模式枚举*/
typedef enum
{
	BUS_Power_Mode = 0,//寄生供电模式
	DC_Power_Mode  = 1,//DC供电模式
}WHT_DS18B20_Power_Mode_enum;

/*DS18B20缓存结构体*/
typedef struct
{
	unsigned char :8;
	unsigned char :8;
	char Alert_Temp_Hig;   //报警高温
	char Alert_Temp_Low;   //报警低温
	unsigned char Accuracy;//温度精度，WHT_DS18B20_Accuracy_Sel_enum
	unsigned char :8;
	unsigned char :8;
	unsigned char :8;
	unsigned char :8;
	const unsigned char ROM_Code[8];//序列号，我的是0x070822816d3d9928
	const WHT_DS18B20_Power_Mode_enum Power_Mode;//供电模式
	const float TEMP;      //当前温度
}WHT_DS18B20_Cache_t;

/*DS18B20配置结构体*/
typedef struct
{
    WHT_GPIO_Port_enum BUS_GPIO_Port;//1总线端口
    WHT_GPIO_Pin_enum BUS_GPIO_Pin;  //1总线引脚
	volatile unsigned int* BUS_Set_State;//内部使用，外部无需管理
	volatile unsigned int* BUS_Get_State;//内部使用，外部无需管理
}WHT_DS18B20_Config_t;


/*DS18B20回调函数结构体*/
typedef struct
{
	void (*WHT_Config)(WHT_DS18B20_Config_t* config);                                                                          //初始化配置
	void (*WHT_Get_Rom_Code)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache);                                        //只适用于总线上只有1个设备
	void (*WHT_Get_Power_Mode)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);             //获取供电模式
	void (*WHT_Get_RAM_Value)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);              //获取所有数据

	void (*WHT_Set_Alarm_Temp_Accuracy)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);    //配置高温报警温度和低温报警温度和温度精度
	void (*WHT_RAM_Value_Copy_EEPROM)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);      //拷贝配置好的寄存器的值到EEPROM里
	void (*WHT_EEPROM_Value_Copy_RAM)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);      //召回EEPROM里的3个字节数据到寄存器对应里面（高温、低温、精度）
	//ErrorStatus (*WHT_Search_All_ROM)(WHT_DS18B20_Config_t* config, unsigned char* rom_code);                                  //搜索总线上所有ROM
	ErrorStatus (*WHT_Convert_Temp)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);        //转换温度
	ErrorStatus (*WHT_Get_Temp)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);            //读取温度,温度保存在缓存里
	//FlagStatus (*WHT_Get_Temp_Alert_Status)(WHT_DS18B20_Config_t* config, WHT_DS18B20_Cache_t* cache, FlagStatus matching_rom);//搜索温度报警状态
}WHT_DS18B20_Driver_t;

extern WHT_DS18B20_Driver_t WHT_DS18B20_Driver;

#ifdef _1BUS_RTOS_Task_Delay
#include "FreeRTOS.h"
#include "task.h"
#endif /*_1BUS_RTOS_Task_Delay*/

#endif // !__1BUS_DRIVER_H__
