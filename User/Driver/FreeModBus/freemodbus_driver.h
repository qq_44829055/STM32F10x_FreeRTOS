//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
#ifndef __FREEMODBUS_DRIVER_H__
#define __FREEMODBUS_DRIVER_H__

#include "../../BSP/UART_BSP/uart_bsp.h"
#include "../../Tool/Ring_Buffer/Ring_Buffer.h"
#include "../../Tool/Ring_Queue/Ring_Queue.h"


#define FREEMODBUS_Max_Buff_Count    513
#define FREEMODBUS_Max_Queue_Count   4



extern void WHT_FreeModBus_Printf(const uint8_t* tx_buf, uint16_t send_length);
extern uint16_t WHT_FreeModBus_Scanf(uint8_t* rx_buf, uint16_t max_receive_length);


#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#endif /*__FREEMODBUS_DRIVER_H__*/
