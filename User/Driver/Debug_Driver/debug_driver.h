//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.2.1
/********************************************************
    说明：
    1、Debug使用串口1，波特率使用256000bps。
    2、发送使用环形缓冲区模式，缓存大小=DEBUG_Max_Buff_Count。
    3、接收使用环形队列模式，缓存大小=DEBUG_Max_Buff_Count*3。
    4、基于FreeRTOS系统，注册初始化后会自动创建一个任务。
    5、外部可使用printf等标准输入输出函数。
    6、获取串口数据加入超时等待时间。
**********************************************************/
#ifndef __DEBUG_DRIVER_H__
#define __DEBUG_DRIVER_H__

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "../../BSP/UART_BSP/uart_bsp.h"
#include "../../Tool/Ring_Buffer/Ring_Buffer.h"
#include "../../Tool/Ring_Queue/Ring_Queue.h"


#define DEBUG_Serial_Baud_Rate  256000
#define DEBUG_Max_Buff_Count    128
#define DEBUG_Max_Queue_Count   5

/*Debug回调函数结构体*/
typedef struct
{
    void (*WHT_Init)(void);//注册初始化
    void (*WHT_Printf)(const char* tx_buffer, uint16_t seng_length);//缓存数据的发送
    uint16_t (*WHT_Scanf)(uint8_t* rx_buffer, uint16_t max_receive_length, uint32_t timeout);//预计读取max_receive_length个数据，返回实际读取一帧数据的长度，返回0表示超时
}WHT_Debug_Driver_t;

extern WHT_Debug_Driver_t WHT_Debug_Driver;

#endif /*__DEBUG_DRIVER_H__*/
