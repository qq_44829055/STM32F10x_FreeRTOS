//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
#ifndef __HC05_DRIVER_H__
#define __HC05_DRIVER_H__

#include "../../BSP/UART_BSP/uart_bsp.h"
#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../Delay/delay.h"
#include "string.h"

/* HC05 INT状态脚 */
#define HC05_INT_GPIO_PORT    	PortB
#define HC05_INT_GPIO_PIN		Pin13
/* HC05 KEY控制脚 */
#define HC05_KEY_GPIO_PORT    	PortB
#define HC05_KEY_GPIO_PIN		Pin14


/*开启任务延迟，特别大批量数据发送*/
#define HC05_Task_Delay

#define HC05_Max_Buf_Count         512//有些操作使用了这个缓存
#define BT_Device_Info_Max_Count   8  //最多存储周期扫描到的8个蓝牙设备
#define BT_Device_Info_Name        64
#define BT_Device_Info_MAC         14
#define BT_Device_Info_IAC         6
#define BT_Device_Info_Rssi        6

typedef enum
{
	AT_CMD_Fail                   = 0, //AT命令错误
	AT_Result_Default             = 1, //指令结果为默认值
	PSKEY_Write_Fail              = 2, //PSKEY写错误
	Device_Name_Overlength_32Byte = 3, //设备名称太长（超过 32个字节）
	Devie_Name_0Byte              = 4, //设备名称长度为零
	BT_Addr_NAP_Overlength        = 5, //蓝牙地址：NAP太长
	BT_Addr_UAP_Overlength        = 6, //蓝牙地址：UAP太长
	BT_Addr_LAP_Overlength        = 7, //蓝牙地址：LAP太长
	Pinx_Mask_0Byte               = 8, //PIO序号掩码长度为零
	Pinx_No_Number                = 9, //无数 PIO序号
	Device_Type_0Byte             = 10,//设备类长度为零
	Device_Type_Number_Overlength = 11,//设备类数字太长
	Access_Code_0Byte             = 12,//查询访问码长度为零
	Access_Code_Overlength        = 13,//查询访问码数字太长
	No_Access_Code                = 14,//无效查询访问码
	Pairing_Code_0Byte            = 15,//配对码长度为零
	Pairing_Code_Overlength_16Byte= 16,//配对码太长（超过 16个字节）
	Module_Mode_Fail              = 17,//模块角色无效
	Baudrate_Fail                 = 18,//波特率无效
	Stop_Bit_Fail                 = 19,//停止位无效
	Check_Bit_Fail                = 20,//校验位无效
	No_Paired_Devices             = 21,//配对列表中不存在认证设备
	SPP_No_Init                   = 22,//SPP库没有初始化
	SPP_Sreset_Init               = 23,//SPP库重复初始化
	Query_Mode_Fail               = 24,//无效查询模式
	Query_Overtime                = 25,//查询超时太大
	BT_Addr_0Byte                 = 26,//蓝牙地址为零
	Safe_Mode_Fail                = 27,//无效安全模式
	Cipher_Mode_Fail              = 28,//无效加密模式
}WHT_BT_Error_Code_enum;

typedef enum
{
	BT_AT_Cmd_Test = 0,                     //"AT\r\n"                      //测试指令
	BT_AT_Cmd_Reset,                        //"AT+RESET\r\n"                //模块复位
	BT_AT_Cmd_Set_Default_State,            //"AT+ORGL\r\n"                 //恢复默认状态
	BT_AT_Cmd_Del_All_BT_Device,            //"AT+RMAAD\r\n"                //从蓝牙配对列表中删除所有认证设备
	BT_AT_Cmd_Init_BT_Module_SPP_Library,   //"AT+INIT\r\n"                 //初始化SPP规范库
	BT_AT_Cmd_Query_BT_Device,              //"AT+INQ\r\n"                  //查询蓝牙设备
	BT_AT_Cmd_Stop_Query_BT_Device,         //"AT+INQC\r\n"                 //取消查询蓝牙设备
	BT_AT_Cmd_Device_Break_Link,            //"AT+DISC\r\n"                 //断开连接
}WHT_BT_AT_Execute_Cmd_enum;//AT执行指令

typedef enum
{
	BT_AT_Cmd_Version = 0,                  //"AT+VERSION?\r\n"             //查询软件版本号
	BT_AT_Cmd_Get_BT_Addr,                  //"AT+ADDR?\r\n"                //查询模块蓝牙地址
	BT_AT_Cmd_Get_BT_Name,                  //"AT+NAME?\r\n"                //查询设备名称
	//BT_AT_Cmd_Get_Remote_BT_Name,         //"AT+RNAME?\r\n"               //获取远程蓝牙设备名,这个获取特殊不提供给上层
	BT_AT_Cmd_Get_Role_Mode,                //"AT+ROLE?\r\n"                //查询模块角色
	BT_AT_Cmd_Get_BT_Class,                 //"AT+CLASS?\r\n"               //查询设备类
	BT_AT_Cmd_Get_Access_Code,              //"AT+IAC?\r\n"                 //查询访问码
	BT_AT_Cmd_Get_Access_Mode,              //"AT+INQM?\r\n"                //查询访问模式
	BT_AT_Cmd_Get_PSWD,                     //"AT+PSWD?\r\n"                //查询配对码
	BT_AT_Cmd_Get_Uart_Config,              //"AT+UART?\r\n"                //查询串口参数
	BT_AT_Cmd_Get_BT_Connection_Mode,       //"AT+CMODE?\r\n"               //查询连接模式
	BT_AT_Cmd_Get_BT_Bind_Addr,             //"AT+BIND?\r\n"                //查询绑定蓝牙地址
	BT_AT_Cmd_Get_LED_Work_Connection_State,//"AT+POLAR?\r\n"               //查询LED指示灯和连接状态输出极性
	BT_AT_Cmd_Get_BT_GPIO_Port_Value,       //"AT+MPIO?\r\n"                //查询询PIO端口输入
	BT_AT_Cmd_Get_IPSCAN,                   //"AT+IPSCAN?\r\n"              //查询PIO资源
	BT_AT_Cmd_Get_SNIFF_Value,              //"AT+SNIFF?\r\n"               //查询SHIFF节能参数
	BT_AT_Cmd_Get_Security_Encryption_Mode, //"AT+SENM?\r\n"                //查询安全和加密模式
	BT_AT_Cmd_Get_BT_Device_Count,          //"AT+ADCN?\r\n"                //查询蓝牙配对列表中认证设备数
	BT_AT_Cmd_Get_Last_Time_BT_Device_Addr, //"AT+MRAD?\r\n"                //查询最近使用过的蓝牙认证设备地址
	BT_AT_Cmd_Get_BT_Module_State,          //"AT+STATE?\r\n"               //查询蓝牙模块工作状态
}WHT_BT_AT_Query_Cmd_enum;

typedef enum
{
	BT_AT_Cmd_Set_BT_Name = 0,              //"AT+NAME=xx\r\n"              //设置设备名称
	BT_AT_Cmd_Set_Role_Mode,                //"AT+ROLE=0/1/2\r\n"           //设置模块角色
	BT_AT_Cmd_Set_BT_Class,                 //"AT+CLASS=32bit\r\n"          //设置设备类
	BT_AT_Cmd_Set_Access_Code,              //"AT+IAC=xx\r\n"               //设置访问码
	BT_AT_Cmd_Set_Access_Mode,              //"AT+INQM=xx,xx,xx\r\n"        //设置查询模式
	BT_AT_Cmd_Set_PSWD,                     //"AT+PSWD=xx\r\n"              //设置配对码
	BT_AT_Cmd_Set_Uart_Config,              //"AT+UART=xx,xx,xx\r\n"        //设置串口参数
	BT_AT_Cmd_Set_BT_Connection_Mode,       //"AT+CMODE=xx\r\n"             //设置连接模式
	BT_AT_Cmd_Set_BT_Bind_Addr,             //"AT+BIND=xx\r\n"              //设置绑定蓝牙地址
	BT_AT_Cmd_Set_LED_Work_Connection_State,//"AT+POLAR=xx,xx\r\n"          //设置LED指示灯和连接状态输出极性
	BT_AT_Cmd_Set_BT_GPIO_Pinx_State,       //"AT+PIO=xx,xx\r\n"            //设置PIO单端口输出
	BT_AT_Cmd_Set_BT_GPIO_MPinx_State,      //"AT+MPIO=xx\r\n"              //设置PIO多端口输出
	BT_AT_Cmd_Set_IPSCAN,                   //"AT+IPSCAN=xx,xx,xx,xx\r\n"   //设置PIO资源
	BT_AT_Cmd_Set_SNIFF_Value,              //"AT+SNIFF=xx,xx,xx,xx\r\n"    //设置SHIFF节能参数
	BT_AT_Cmd_Set_Security_Encryption_Mode, //"AT+SENM=xx,xx\r\n"           //设置安全和加密模式
	BT_AT_Cmd_Del_BT_Device,                //"AT+RMSAD=xx\r\n"             //从蓝牙配对列表中删除指定认证设备
	BT_AT_Cmd_Check_BT_Device,              //"AT+FSAD=xx\r\n"              //从蓝牙配对列表中查找指定的认证设备
	BT_AT_Cmd_Device_Pairing,               //"AT+PAIR=xx,xx\r\n"           //设备配对
	BT_AT_Cmd_Device_Link,                  //"AT+LINK=xx\r\n"              //设备连接
	BT_AT_Cmd_Power_Saving_Mode,            //"AT+ENSNIFF=xx\r\n"           //进入节能模式
	BT_AT_Cmd_Exit_Power_Saving_Mode,       //"AT+EXSNIFF=xx\r\n"           //退出节能模式
}WHT_BT_AT_Set_Cmd_enum;

/*蓝牙地址十六进制，NAP:UAP:LAP，例如：蓝牙设备地址：00:02:72:od:22:24对于HC05则为02:72:od2224*/
typedef struct WHT_BT_Device_Info_Node
{
	struct WHT_BT_Device_Info_Node* Prev;
	struct WHT_BT_Device_Info_Node* Next;
	char Name[BT_Device_Info_Name];//蓝牙设备名字
	char MAC[BT_Device_Info_MAC];  //蓝牙设备地址(NAP:UAP:LAP)
	char IAC[BT_Device_Info_IAC];  //蓝牙设备类型
	char Rssi[BT_Device_Info_Rssi];//蓝牙设备信号强度
}WHT_BT_Device_Info_Node_t;

typedef struct
{
	uint32_t Count;
	WHT_BT_Device_Info_Node_t* Head_Node;
	char Local_Name[BT_Device_Info_Name];//蓝牙设备名字
	char Local_MAC[BT_Device_Info_MAC];  //蓝牙设备地址(NAP:UAP:LAP)
	char Local_PSWD[32];                 //蓝牙配对码
	char Local_Version[32];              //蓝牙软件版本
}WHT_BT_Device_Info_List_t;


typedef struct
{
	const WHT_BT_Device_Info_List_t* WHT_BT_Device_Info_List;                                        //本地及远程蓝牙信息
	FlagStatus (*WHT_Get_BT_Connected_State)(void);                                                  //获取蓝牙连接状态
	ErrorStatus (*WHT_BT_Config)(void);                                                              //蓝牙初始化配置
	ErrorStatus (*WHT_BT_Execute_Cmd)(WHT_BT_AT_Execute_Cmd_enum cmd_code);                          //AT执行指令操作
	ErrorStatus (*WHT_BT_Query_Cmd)(WHT_BT_AT_Query_Cmd_enum cmd_code, char** rx_buf, uint32_t* rx_length);//AT查询指令操作
	ErrorStatus (*WHT_BT_Set_Cmd)(WHT_BT_AT_Set_Cmd_enum cmd_code, const char* tx_buf, uint32_t tx_length);//AT设置指令操作
	ErrorStatus (*WHT_Scan_Remote_Device)(void);                                                     //扫描周围蓝牙信息,会变成主模式然后默认模式
	ErrorStatus (*WHT_Transparent_Transmission_Send)(const char* buf, uint32_t length);              //透传发送
    ErrorStatus (*WHT_Transparent_Transmission_Receive)(char** buf, uint32_t* length);               //透传接收
}WHT_HC05_Driver_t;

extern void WHT_HC05_Driver_Register(WHT_HC05_Driver_t* driver);

#ifdef HC05_Task_Delay
#include "FreeRTOS.h"
#include "task.h"
#endif // HC05_Task_Delay

#endif /*__HC05_DRIVER_H__*/
