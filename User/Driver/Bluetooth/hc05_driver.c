//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.0
#include "hc05_driver.h"

static WHT_BT_Device_Info_List_t BT_Info_List;                          //存放本身蓝牙信息及周围蓝牙信息的索引
static WHT_BT_Device_Info_Node_t BT_Info_Node[BT_Device_Info_Max_Count];//存放周围蓝牙的信息

static uint8_t HC05_Tx_Buf[HC05_Max_Buf_Count] = { 0 };
static uint8_t HC05_Rx_Buf[HC05_Max_Buf_Count] = { 0 };

/**********************驱动初始化配置*********************/
static const WHT_UART_Config_t wht_config =
{
    .Name = UART_2,
    .Bps = 38400,
    .PreemptionPriority = 15,
    .SubPriority = 0,
};
static WHT_UART_Cache_t wht_cache =
{
    .Rx_Buf_Size = HC05_Max_Buf_Count,
    .Rx_Count = 0,
    .Rx_Buf = (uint8_t*)HC05_Rx_Buf,
};
static volatile uint32_t* HC05_INT_State = NULL;
static volatile uint32_t* HC05_KEY_State = NULL;
static void WHT_HC05_Driver_GPIO_Init(void)
{
    WHT_GPIO_BSP.WHT_Set_Clock(HC05_INT_GPIO_PORT, ENABLE);
    WHT_GPIO_BSP.WHT_Set_State(HC05_INT_GPIO_PORT, HC05_INT_GPIO_PIN, Hig);
    WHT_GPIO_BSP.WHT_Set_Mode(HC05_INT_GPIO_PORT, HC05_INT_GPIO_PIN, Mode_IN_FLOATING);
    WHT_GPIO_BSP.WHT_Config_Bit_Input(HC05_INT_GPIO_PORT, HC05_INT_GPIO_PIN, &HC05_INT_State);

    WHT_GPIO_BSP.WHT_Set_Clock(HC05_KEY_GPIO_PORT, ENABLE);
    WHT_GPIO_BSP.WHT_Set_State(HC05_KEY_GPIO_PORT, HC05_KEY_GPIO_PIN, Low);
    WHT_GPIO_BSP.WHT_Set_Mode(HC05_KEY_GPIO_PORT, HC05_KEY_GPIO_PIN, Mode_Out_PP);
    WHT_GPIO_BSP.WHT_Config_Bit_Output(HC05_KEY_GPIO_PORT, HC05_KEY_GPIO_PIN, &HC05_KEY_State);
}

static void WHT_HC05_Driver_Init(void)
{
    WHT_HC05_Driver_GPIO_Init();
    WHT_UART_BSP.WHT_Register(&wht_config, &wht_cache);
    WHT_UART_BSP.WHT_Config(&wht_config, &wht_cache);
    WHT_UART_BSP.WHT_DMA_Receive(&wht_cache);
}
/**********************驱动初始化配置*********************/


static ErrorStatus WHT_HC05_Driver_Puts(const char* tx_buf, uint32_t send_length)
{
    if ((send_length > sizeof(HC05_Tx_Buf)) || (send_length == 0))
        return ERROR;
    while (wht_cache.Info->Tx_State == UART_Tx_Busy);//等待发送完成
    wht_cache.Tx_Count = send_length;
    memcpy((void*)HC05_Tx_Buf, (const void*)tx_buf, send_length);
    
    #ifdef HC05_Task_Delay
        TickType_t Tick_Count = send_length * 10 * 1000  / wht_config.Bps / portTICK_RATE_MS;//单位ms/Tick

        if (Tick_Count)
        {
            WHT_UART_BSP.WHT_DMA_Send(&wht_cache);
            vTaskDelay(Tick_Count);
            Tick_Count = send_length * 10 * 1000  / wht_config.Bps % portTICK_RATE_MS;//单位ms Tick的余数
        }
        else
        {
            WHT_UART_BSP.WHT_Send(&wht_cache);
        }
    #else
        WHT_UART_BSP.WHT_Send(&wht_config);
    #endif // HC05_Task_Delay
    while (wht_cache.Info->Tx_State == UART_Tx_Busy);//等待发送完成
    return SUCCESS;
}
static ErrorStatus WHT_HC05_Driver_Gets(char** rx_buf, uint32_t* receive_length)
{
    if ((wht_cache.Info->Rx_State == UART_Rx_Idle) && (wht_cache.Rx_Count != 0))//表示接收完成
    {
        *rx_buf = (char*)HC05_Rx_Buf;
        *receive_length = wht_cache.Rx_Count;
        wht_cache.Rx_Count = 0;                           //清除缓存
				
        WHT_UART_BSP.WHT_DMA_Receive(&wht_cache);      //接收使用DMA技术
        return SUCCESS;
    }
    else
        return ERROR;
}


/*
AT指令均已\r\n为结束符，HC05检测到后才会解析上几次发送的数据，故AT指令可以分批次发送
*/
static ErrorStatus WHT_HC05_Driver_Check_OK(char** rx_buf, uint32_t* rx_length)
{
    uint32_t wait_time;

    for (wait_time = 0; wait_time < 300; wait_time++)        //最长等待3秒
    {
        #ifdef HC05_Task_Delay
            vTaskDelay(10 / portTICK_RATE_MS);
        #else
                WHT_Delay_ms(10);
        #endif // HC05_Task_Delay
        if (WHT_HC05_Driver_Gets(rx_buf, rx_length) != ERROR)
        {
            *rx_buf[*rx_length] = '\0';
            if (strstr((const char*)(*rx_buf), "OK") != NULL) //检测到OK字样
                return SUCCESS;
            else
                break;
        }
    }
    return ERROR;
}

static ErrorStatus WHT_HC05_Driver_Execute_Cmd(WHT_BT_AT_Execute_Cmd_enum cmd_code)//AT执行指令
{
    char** rx_buf;
    uint32_t rx_length;
    ErrorStatus state = ERROR;

    *HC05_KEY_State = Hig;                          //是否需要延迟需要实际测试或询问FAE
    switch (cmd_code)
    {
    case BT_AT_Cmd_Test:                            //测试指令
        state = WHT_HC05_Driver_Puts("AT\r\n", 4);
        break;
    case BT_AT_Cmd_Reset:                           //模块复位
        state = WHT_HC05_Driver_Puts("AT+RESET\r\n", 10);
        break;
    case BT_AT_Cmd_Set_Default_State:               //恢复默认状态
        state = WHT_HC05_Driver_Puts("AT+ORGL\r\n", 9);
        break;
    case BT_AT_Cmd_Del_All_BT_Device:               //从蓝牙配对列表中删除所有认证设备
        state = WHT_HC05_Driver_Puts("AT+RMAAD\r\n", 10);
        break;
    case BT_AT_Cmd_Init_BT_Module_SPP_Library:      //初始化SPP规范库
        state = WHT_HC05_Driver_Puts("AT+INIT\r\n", 9);
        break;
    case BT_AT_Cmd_Query_BT_Device:                 //查询蓝牙设备
        state = WHT_HC05_Driver_Puts("AT+INQ\r\n", 8);
        break;
    case BT_AT_Cmd_Stop_Query_BT_Device:            //取消查询蓝牙设备
        state = WHT_HC05_Driver_Puts("AT+INQC\r\n", 9);
        break;
    case BT_AT_Cmd_Device_Break_Link:               //断开连接
        state = WHT_HC05_Driver_Puts("AT+DISC\r\n", 9);
        break;
    default:
        return state;
    }
    if (state != ERROR)
        state = WHT_HC05_Driver_Check_OK(rx_buf, &rx_length);
    //*HC05_KEY_State = Low;
    return state;
}
static ErrorStatus WHT_HC05_Driver_Query_Cmd(WHT_BT_AT_Query_Cmd_enum cmd_code, char** rx_buf, uint32_t* rx_length)//AT查询指令
{
    ErrorStatus state = ERROR;

    *HC05_KEY_State = Hig;                          //是否需要延迟需要实际测试或询问FAE
    switch (cmd_code)
    {
    case BT_AT_Cmd_Version:                         //查询软件版本号
        state = WHT_HC05_Driver_Puts("AT+VERSION?\r\n", 13);
        break;
    case BT_AT_Cmd_Get_BT_Addr:                     //查询模块蓝牙地址
        state = WHT_HC05_Driver_Puts("AT+ADDR?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_BT_Name:                     //查询设备名称
        state = WHT_HC05_Driver_Puts("AT+NAME?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_Role_Mode:                   //查询模块角色
        state = WHT_HC05_Driver_Puts("AT+ROLE?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_BT_Class:                    //查询设备类
        state = WHT_HC05_Driver_Puts("AT+CLASS?\r\n", 11);
        break;
    case BT_AT_Cmd_Get_Access_Code:                 //查询访问码
        state = WHT_HC05_Driver_Puts("AT+IAC?\r\n", 9);
        break;
    case BT_AT_Cmd_Get_Access_Mode:                 //查询访问模式
        state = WHT_HC05_Driver_Puts("AT+INQM?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_PSWD:                        //查询配对码
        state = WHT_HC05_Driver_Puts("AT+PSWD?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_Uart_Config:                 //查询串口参数
        state = WHT_HC05_Driver_Puts("AT+UART?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_BT_Connection_Mode:          //查询连接模式
        state = WHT_HC05_Driver_Puts("AT+CMODE?\r\n", 11);
        break;
    case BT_AT_Cmd_Get_BT_Bind_Addr:                //查询绑定蓝牙地址
        state = WHT_HC05_Driver_Puts("AT+BIND?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_LED_Work_Connection_State:   //查询LED指示灯和连接状态输出极性
        state = WHT_HC05_Driver_Puts("AT+POLAR?\r\n", 11);
        break;
    case BT_AT_Cmd_Get_BT_GPIO_Port_Value:          //查询询PIO端口输入
        state = WHT_HC05_Driver_Puts("AT+MPIO?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_IPSCAN:                      //查询PIO资源
        state = WHT_HC05_Driver_Puts("AT+IPSCAN?\r\n", 12);
        break;
    case BT_AT_Cmd_Get_SNIFF_Value:                 //查询SHIFF节能参数
        state = WHT_HC05_Driver_Puts("AT+SNIFF?\r\n", 11);
        break;
    case BT_AT_Cmd_Get_Security_Encryption_Mode:    //查询安全和加密模式
        state = WHT_HC05_Driver_Puts("AT+SENM?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_BT_Device_Count:             //查询蓝牙配对列表中认证设备数
        state = WHT_HC05_Driver_Puts("AT+ADCN?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_Last_Time_BT_Device_Addr:    //查询最近使用过的蓝牙认证设备地址
        state = WHT_HC05_Driver_Puts("AT+MRAD?\r\n", 10);
        break;
    case BT_AT_Cmd_Get_BT_Module_State:             //查询蓝牙模块工作状态
        state = WHT_HC05_Driver_Puts("AT+STATE?\r\n", 11);
        break;
    default:
        return state;
    }
    if (state != ERROR)
        state = WHT_HC05_Driver_Check_OK(rx_buf, rx_length);
    //*HC05_KEY_State = Low;
    return state;
}
static ErrorStatus WHT_HC05_Driver_Set_Cmd(WHT_BT_AT_Set_Cmd_enum cmd_code, const char* tx_buf, uint32_t tx_length)//AT设置指令操作
{
    char** buf;
    ErrorStatus state = ERROR;

    if ((tx_length == 0) || (tx_length > HC05_Max_Buf_Count))
        return state;
    *HC05_KEY_State = Hig;                          //是否需要延迟需要实际测试或询问FAE
    switch (cmd_code)
    {
    case BT_AT_Cmd_Set_BT_Name:                     //设置设备名称
        state = WHT_HC05_Driver_Puts("AT+NAME=", 8);
        break;
    case BT_AT_Cmd_Set_Role_Mode:                   //设置模块角色
        state = WHT_HC05_Driver_Puts("AT+ROLE=", 8);
        break;
    case BT_AT_Cmd_Set_BT_Class:                    //设置设备类
        state = WHT_HC05_Driver_Puts("AT+CLASS=", 9);
        break;
    case BT_AT_Cmd_Set_Access_Code:                 //设置访问码
        state = WHT_HC05_Driver_Puts("AT+IAC=", 7);
        break;
    case BT_AT_Cmd_Set_Access_Mode:                 //设置访问模式
        state = WHT_HC05_Driver_Puts("AT+INQM=", 8);
        break;
    case BT_AT_Cmd_Set_PSWD:                        //设置配对码
        state = WHT_HC05_Driver_Puts("AT+PSWD=", 8);
        break;
    case BT_AT_Cmd_Set_Uart_Config:                 //设置串口参数
        state = WHT_HC05_Driver_Puts("AT+UART=", 8);
        break;
    case BT_AT_Cmd_Set_BT_Connection_Mode:          //设置连接模式
        state = WHT_HC05_Driver_Puts("AT+CMODE=", 9);
        break;
    case BT_AT_Cmd_Set_BT_Bind_Addr:                //设置绑定蓝牙地址
        state = WHT_HC05_Driver_Puts("AT+BIND=", 8);
        break;
    case BT_AT_Cmd_Set_LED_Work_Connection_State:   //设置LED指示灯和连接状态输出极性
        state = WHT_HC05_Driver_Puts("AT+POLAR=", 9);
        break;
    case BT_AT_Cmd_Set_BT_GPIO_Pinx_State:          //设置PIO单端口输出
        state = WHT_HC05_Driver_Puts("AT+PIO=", 7);
        break;
    case BT_AT_Cmd_Set_BT_GPIO_MPinx_State:         //设置PIO多端口输出
        state = WHT_HC05_Driver_Puts("AT+MPIO=", 8);
        break;
    case BT_AT_Cmd_Set_IPSCAN:                      //设置PIO资源
        state = WHT_HC05_Driver_Puts("AT+IPSCAN=", 10);
        break;
    case BT_AT_Cmd_Set_SNIFF_Value:                 //设置SHIFF节能参数
        state = WHT_HC05_Driver_Puts("AT+SNIFF=", 9);
        break;
    case BT_AT_Cmd_Set_Security_Encryption_Mode:    //设置安全和加密模式
        state = WHT_HC05_Driver_Puts("AT+SENM=", 8);
        break;
    case BT_AT_Cmd_Del_BT_Device:                   //从蓝牙配对列表中删除指定认证设备
        state = WHT_HC05_Driver_Puts("AT+RMSAD=", 9);
        break;
    case BT_AT_Cmd_Check_BT_Device:                 //从蓝牙配对列表中查找指定的认证设备
        state = WHT_HC05_Driver_Puts("AT+FSAD=", 8);
        break;
    case BT_AT_Cmd_Device_Pairing:                  //设备配对
        state = WHT_HC05_Driver_Puts("AT+PAIR=", 8);
        break;
    case BT_AT_Cmd_Device_Link:                     //设备连接
        state = WHT_HC05_Driver_Puts("AT+LINK=", 8);
        break;
    case BT_AT_Cmd_Power_Saving_Mode:               //进入节能模式
        state = WHT_HC05_Driver_Puts("AT+ENSNIFF=", 11);
        break;
    case BT_AT_Cmd_Exit_Power_Saving_Mode:          //退出节能模式
        state = WHT_HC05_Driver_Puts("AT+EXSNIFF=", 11);
        break;
    default:
        return state;
    }
    if (state != ERROR)
    {
        state = WHT_HC05_Driver_Puts(tx_buf, tx_length);
        if (state != ERROR)
        {
            state = WHT_HC05_Driver_Puts("\r\n", 2);
            if (state != ERROR)
                state = WHT_HC05_Driver_Check_OK(buf, &tx_length);
        }
    }
    //*HC05_KEY_State = Low;
    return state;
}

static ErrorStatus WHT_HC05_Driver_Transparent_Transmission_Send(const char* tx_buf, uint32_t tx_length)
{
    *HC05_KEY_State = Low;//是否需要延迟需要实际测试或询问FAE
    return WHT_HC05_Driver_Puts(tx_buf, tx_length);
}
static ErrorStatus WHT_HC05_Driver_Transparent_Transmission_Receive(char** rx_buf, uint32_t* rx_length)
{
    //*HC05_KEY_State = Low;
    return WHT_HC05_Driver_Gets(rx_buf, rx_length);
}

static ErrorStatus WHT_HC05_Driver_Test_AT_Cmd(void)
{           
    if (WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Test) != ERROR)//首次验证AT指令可能有之前的缓存
        return WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Test);  //再次判断AT指令
    else
        return ERROR;
}
static ErrorStatus WHT_HC05_Driver_Init_Config(void)
{
    uint32_t count;
    ErrorStatus state;

    state = WHT_HC05_Driver_Test_AT_Cmd();
    for (count = 0; ; count++)
    {
        if (state == ERROR)
            return state;
        switch (count)
        {
        case 0://对模块复位重启
            state = WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Reset);
            break;            
        case 1://对模块配置恢复
            state = WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Set_Default_State);
            break;
        case 2://默认从模式
            state = WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Role_Mode, "0", 1);
            break;
        case 3://初始化SPP规范
            WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Init_BT_Module_SPP_Library);
            state = SUCCESS;
            break;
        case 4://查询各种蓝牙设备
            state = WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_BT_Class, "0", 1);
            break;
        case 5://查询模式设置，带RSSI信号强度
            state = WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Access_Mode, "1,1,5", 6);//默认只扫描1台,因为HC05发送给单片机的数据不是连续数据流
            break;
        case 6://配置蓝牙名字
            state = WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_BT_Name, "WHT HC05", 8);
            break;
        default:
            return SUCCESS;
        }
    }
}
static FlagStatus WHT_HC05_Driver_Get_BT_Connected_State(void)
{
    if (*HC05_INT_State != Low)
        return SET;
    else
        return RESET;
}

static void WHT_HC05_Driver_List_Init(void)
{
    uint32_t i;

    BT_Info_List.Count = 0;
    BT_Info_List.Head_Node = NULL;
    memcpy(BT_Info_List.Local_Name, "WHT HC05", 9);
    memcpy(BT_Info_List.Local_PSWD, "123456", 7);
    memset(BT_Info_List.Local_MAC, '\0', BT_Device_Info_MAC);
    memset(BT_Info_List.Local_Version, '\0', 32);

    for (i = 0; i < BT_Device_Info_Max_Count; i++)
    {
        BT_Info_Node[i].Prev = NULL;
        BT_Info_Node[i].Next = NULL;
        memset(BT_Info_Node[i].Name, '\0', BT_Device_Info_Name);
        memset(BT_Info_Node[i].MAC, '\0', BT_Device_Info_MAC);
        memset(BT_Info_Node[i].IAC, '\0', BT_Device_Info_IAC);
        memset(BT_Info_Node[i].Rssi, '\0', BT_Device_Info_Rssi);
    }
}
static ErrorStatus WHT_HC05_Driver_Get_Rname(void)//查询远程蓝牙的名字
{
    char* buf;
    uint32_t length;
    uint32_t i;

    for (i = 0; i < BT_Info_List.Count; i++)
    {
        if (WHT_HC05_Driver_Puts("AT+RNAME?", 9) == ERROR)//查询远程蓝牙设备名
            return ERROR;
        else
        {
            if (WHT_HC05_Driver_Puts(BT_Info_Node[i].MAC, strlen(BT_Info_Node[i].MAC)) == ERROR)
                return ERROR;
            if (WHT_HC05_Driver_Puts("\r\n", 2) == ERROR)
                return ERROR;
            else
            {
                if (WHT_HC05_Driver_Check_OK(&buf, &length) == ERROR)
                    return ERROR;
                else
                {
                    char* p;

                    p = strstr(buf, "\r");
                    if (p != NULL)
                        *p = '\0';
                    p = strstr(buf, "\n");
                    if (p != NULL)
                        *p = '\0';
                    p = strstr(buf, ":");
                    if (p != NULL)
                        strcpy(BT_Info_Node[i].Name, p+1);
                }
            }
        }
    }
    return SUCCESS;
} 
static ErrorStatus WHT_HC05_Driver_Add_List_Node(const char* rx_buf, uint32_t rx_length)
{
    char* sbuf;

    if (BT_Info_List.Count == BT_Device_Info_Max_Count)
        return ERROR;
    if (strstr(rx_buf, "+INQ:") == NULL)
        return ERROR;
    do
    {
        sbuf = strstr(rx_buf, ",");
        if (sbuf != NULL)
            *sbuf = '\0';
        else
            break;
    } while (1);
    sbuf = strstr(rx_buf, ":");
    strcpy(BT_Info_Node[BT_Info_List.Count].MAC, &sbuf[1]);
    sbuf = strstr(rx_buf, "\0");
    strcpy(BT_Info_Node[BT_Info_List.Count].IAC, &sbuf[1]);
    sbuf = strstr(&sbuf[1], "\0");
    memcpy(BT_Info_Node[BT_Info_List.Count].Rssi, &sbuf[1], 4);
    BT_Info_List.Count++;
    return SUCCESS;
    /*对于重复搜索到的蓝牙,这里不做处理*/
}
static ErrorStatus WHT_HC05_Driver_Scan_Remote_Device(void)
{
    char* buf;
    uint32_t length;
    char AT_RLOE_Mode;

    if (WHT_HC05_Driver_Query_Cmd(BT_AT_Cmd_Get_Role_Mode, &buf, &length) == ERROR)  //查询当前蓝牙模式
        return ERROR;
    buf = strstr(buf, "+ROLE:");
    if (buf == NULL)
        return ERROR;
    else
        AT_RLOE_Mode = buf[7];
    if (WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Role_Mode, "1", 1) == ERROR)           //配置蓝牙为主模式
        return ERROR;
    if (WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Access_Code, "9e8b33\r\n", 8) == ERROR)//查询任意访问码的蓝牙设备
        return ERROR;
    if (WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Access_Mode, "1,1,5", 6) == ERROR)     //默认只扫描1台,因为HC05发送给单片机的数据不是连续数据流
        return ERROR;
    do/*由于HC05不会连续发送，会导致我的驱动误认为接收完成了，会关闭接收的。*/
    {
        if (WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Query_BT_Device) == ERROR)         //查询周边蓝牙设备
            return ERROR;
        if (WHT_HC05_Driver_Check_OK(&buf, &length) == ERROR)                     //查询扫描到蓝牙的信息
            return ERROR;
        else
        {
            if (WHT_HC05_Driver_Add_List_Node(buf, length) == ERROR)                 //蓝牙已经添加满了
                break;
        }
    } while (1);
    if (WHT_HC05_Driver_Execute_Cmd(BT_AT_Cmd_Stop_Query_BT_Device) == ERROR)        //取消查询周边蓝牙设备
        return ERROR;
    if (WHT_HC05_Driver_Get_Rname() == ERROR)                                        //查询扫描到的蓝牙mac对应的名字
        return ERROR;
    if (WHT_HC05_Driver_Set_Cmd(BT_AT_Cmd_Set_Role_Mode, &AT_RLOE_Mode, 1) == ERROR) //返回之前的模式
        return ERROR;
    return SUCCESS;
}

void WHT_HC05_Driver_Register(WHT_HC05_Driver_t* driver)
{
    WHT_HC05_Driver_Init();
    WHT_HC05_Driver_List_Init();

    driver->WHT_BT_Device_Info_List              = &BT_Info_List;
    driver->WHT_Get_BT_Connected_State           = WHT_HC05_Driver_Get_BT_Connected_State;
    driver->WHT_BT_Config                        = WHT_HC05_Driver_Init_Config;
    driver->WHT_BT_Execute_Cmd                   = WHT_HC05_Driver_Execute_Cmd;
    driver->WHT_BT_Query_Cmd                     = WHT_HC05_Driver_Query_Cmd;
    driver->WHT_BT_Set_Cmd                       = WHT_HC05_Driver_Set_Cmd;
    driver->WHT_Scan_Remote_Device               = WHT_HC05_Driver_Scan_Remote_Device;
    driver->WHT_Transparent_Transmission_Send    = WHT_HC05_Driver_Transparent_Transmission_Send;
    driver->WHT_Transparent_Transmission_Receive = WHT_HC05_Driver_Transparent_Transmission_Receive;
}
