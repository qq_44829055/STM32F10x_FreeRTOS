//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
/********************************************************
    说明：
    1、基于模拟GPIO控制74HC595。
    2、使用私有GPIO_BSP驱动。
    3、基于3线控制74HC595。
**********************************************************/
#ifndef __74HC595_H__
#define __74HC595_H__

#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../Delay/delay.h"


#define HC595_COUNT        4              //HC595级联个数
#define HC595_MAX_CH_COUNT (HC595_COUNT*8)//扩展IO总个数
#define HC595_DELAY_TIME   1              //HC595通信延迟时间

#define HC595_SCL_Port     PortB
#define HC595_SCL_Pin      Pin1

#define HC595_SDA_Port     PortB
#define HC595_SDA_Pin      Pin0

#define HC595_LOCK_Port    PortB
#define HC595_LOCK_Pin     Pin11



typedef struct
{
	void (*WHT_Init)(void);                                    //初始化
	void (*WHT_F5_All_Channel_Status)(void);                   //手动刷新API
	void (*WHT_Set_Channel_Status)(unsigned short ch, FunctionalState status);//需要手动刷新，0 <= ch < HC595_MAX_CH_COUNT
	void (*WHT_Set_All_Channel_Status)(FunctionalState status);//会自动刷新
}WHT_HC595_Driver_t;

extern WHT_HC595_Driver_t WHT_HC595_Driver;

#endif /*__74HC595_H__*/
