//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
#ifndef __OLED_DRIVER_H__
#define __OLED_DRIVER_H__

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "../../BSP/I2C_BSP/i2c_bus.h"
#include "../../Tool/Soft_I2C/soft_i2c_bus.h"
#include "../../Delay/delay.h"
#include "../Lcd/font.h"


/*开启软件模拟I2C*/
#define OLED_Soft_I2C

/*缓存大小设置*/
#define OLED_BUFF_SIZE        128*8

/*屏幕分辨率*/
#define OLED_SCREEN_Wide     128
#define OLED_SCREEN_High     64


/*命令*/
typedef enum
{
    OLED_Set_Addressing_Mode = 0x20,//设置内存寻址模式
    OLED_Set_Page            = 0x22,//设置起始页与结束页
    OLED_Set_Col             = 0x21,//设置起始列于结束列
    OLED_Set_LM              = 0x81,//设置亮度
    OLED_Write_Cmd           = 0x00,//写命令，后面跟着命令值
    OLED_Fill_Pixel          = 0x40,//填数据，后面跟着数据值
}WHT_OLED_Cmd_enum;

/*寻址模式*/
typedef enum
{
    Horizontal_Addressing_Mode = 0x00,//水平寻址模式
    Vertical_Addressing_Mode   = 0x01,//垂直寻址模式
    Page_Addressing_Mode       = 0x02,//页寻址模式
}WHT_OLED_Addressing_Mode_enum;

typedef enum
{
    OLED_7Bit_Addr0 = 0x78 >> 1,//通过调整0R电阻,屏可以0x78和0x7A两个地址 -- 默认0x78
    OLED_7Bit_Addr1 = 0x7A >> 1,
}WHT_OLED_Addr_enum;

/*屏幕参数*/
typedef struct
{
    const uint16_t X_Length;//屏幕x轴长度
    const uint16_t Y_Length;//屏幕y轴长度
    WHT_Font_Info_t* Font;  //字体选择
    uint8_t New_Col;        //最新列坐标
    uint8_t New_Page;       //最新页坐标
    uint8_t BK_Color;       //字体背景色
    uint8_t Font_Color;     //字体颜色
    uint8_t LM;             //屏幕亮度0~255
}WHT_OLED_Info_t;

typedef struct
{
    volatile WHT_OLED_Info_t* WHT_OLED_Info;
    uint8_t* WHT_Send_Buf;   //发送缓存
    uint8_t (*WHT_Scan_I2C_Slave_Count)(void);
    void (*WHT_OLED_Init)(void);          //初始化
    uint8_t(*WHT_Set_Font_Pos)(uint16_t row, uint16_t col); //指定字体显示在那行那列
    void (*WHT_Set_LM)(uint8_t lm_value); //设置亮度
    void (*WHT_Clear_Row)(uint8_t row);    //清空某行字体
    void (*WHT_Puts)(const char* s);       //ASCII字符显示
    void (*WHT_Write)(uint8_t* input_buf, uint32_t count);//这个临时提供给U8G2的
}WHT_OLED_Driver_t;

extern const WHT_OLED_Driver_t WHT_OLED_Driver;

void OLED_TEST_FUNC(void);

#endif /*__OLED_DRIVER_H__*/
