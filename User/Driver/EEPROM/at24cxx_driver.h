//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.3.0
/********************************************************
    说明：
    1、基于AT24Cxx的驱动。
    2、支持软件模拟I2C和硬件I2C。由宏AT24Cxx_Soft_I2C决定。
	3、基于FreeRTOS,基于i2c_bus.h和soft_i2c_bus.h。
    4、本驱动提供一个可修改大小的收发缓存。
	5、基于硬件I2C读写时会自动更具数据长度选择DMA模式和中断模式。
	6、基于FreeRTOS,自动让出CPU资源，并能及时响应读写完成。
**********************************************************/
#ifndef __AT24CXX_DRIVER_H__
#define __AT24CXX_DRIVER_H__

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "../../BSP/I2C_BSP/i2c_bus.h"
#include "../../Tool/Soft_I2C/soft_i2c_bus.h"

/*开启软件模拟I2C*/
//#define AT24Cxx_Soft_I2C

#define AT24Cxx_BUFFER_SIZE        (8*32)//缓存字节个数,min set AT24Cxx_Page_Byte

/*
AT24C01 页大小 8字节，共计16页
AT24C02 页大小 8字节，共计32页
AT24C04 页大小16字节，共计32页
AT24C08 页大小16字节，共计64页
AT24C16 页大小16字节，共计128页
*/
typedef enum
{
	AT24Cxx_Page_Byte  = 8, //根据使用的具体型号修改值
	AT24Cxx_Page_Count = 32,//根据使用的具体型号修改值
}WHT_AT24Cxx_Info_enum;

typedef enum//毫秒
{
	AT24Cxx_Busy_Over_Time = 10,//等待超时
}WHT_AT24Cxx_Over_Time_enum;

//AT24Cxx型号与地址枚举
typedef enum
{
    AT24C02_7Bit_Addr0 = 0x50|0,
    AT24C02_7Bit_Addr1 = 0x50|1,
	AT24C02_7Bit_Addr2 = 0x50|2,
	AT24C02_7Bit_Addr3 = 0x50|3,
	AT24C02_7Bit_Addr4 = 0x50|4,
	AT24C02_7Bit_Addr5 = 0x50|5,
	AT24C02_7Bit_Addr6 = 0x50|6,
	AT24C02_7Bit_Addr7 = 0x50|7,

	AT24C04_7Bit_Addr0 = 0x50|0,
	AT24C04_7Bit_Addr2 = 0x50|2,
	AT24C04_7Bit_Addr4 = 0x50|4,
	AT24C04_7Bit_Addr6 = 0x50|6,

	AT24C08_7Bit_Addr0 = 0x50|0,
	AT24C08_7Bit_Addr4 = 0x50|4,

	AT24C16_7Bit_Addr0 = 0x50|0,
}WHT_AT24Cxx_Addr_enum;

typedef struct//当前没有用哈
{
	char* Name;
	uint8_t Page_Size;
	uint8_t Page_Count;
	WHT_AT24Cxx_Addr_enum Device_Addr;
}WHT_AT24Cxx_Info_t;

/*AT24Cxx回调函数结构体*/
typedef struct
{
	uint8_t* WHT_TRx_Buffer; //收发缓存，大小为AT24Cxx_BUFFER_SIZE
	void (*WHT_Init)(void);
	uint8_t (*WHT_Scan_I2C_Slave_Count)(void);
	void (*WHT_Write_Data)(WHT_AT24Cxx_Addr_enum addr, uint8_t* const buffer, uint16_t start_addr,uint16_t count);
	void (*WHT_Read_Data)(WHT_AT24Cxx_Addr_enum addr,uint8_t* const buffer, uint16_t start_addr,uint16_t count);
}WHT_AT24Cxx_Driver_t;

extern WHT_AT24Cxx_Driver_t WHT_AT24Cxx_Driver;

#endif // !__AT24CXX_DRIVER_H__
