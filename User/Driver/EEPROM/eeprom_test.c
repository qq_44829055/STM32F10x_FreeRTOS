#include "at24cxx_driver.h"
#include "../Debug_Driver/debug_driver.h"





void EEPROM_TEST_FUNC(void)
{
    WHT_AT24Cxx_Driver.WHT_Init();

    uint8_t i2c_count = WHT_AT24Cxx_Driver.WHT_Scan_I2C_Slave_Count();
    printf("I2C Slave Count = %d\r\n", i2c_count);

    i2c_count = WHT_AT24Cxx_Driver.WHT_Scan_I2C_Slave_Count();
    printf("I2C Slave Count = %d\r\n", i2c_count);

    /*清零*/
    memset(WHT_AT24Cxx_Driver.WHT_TRx_Buffer, 0, AT24Cxx_BUFFER_SIZE);
    WHT_AT24Cxx_Driver.WHT_Write_Data(AT24C08_7Bit_Addr0, WHT_AT24Cxx_Driver.WHT_TRx_Buffer, 0, AT24Cxx_BUFFER_SIZE);
    WHT_AT24Cxx_Driver.WHT_Read_Data(AT24C08_7Bit_Addr0, WHT_AT24Cxx_Driver.WHT_TRx_Buffer, 0, AT24Cxx_BUFFER_SIZE);
    WHT_Debug_Driver.WHT_Printf(WHT_AT24Cxx_Driver.WHT_TRx_Buffer, AT24Cxx_BUFFER_SIZE);

    /*写字符串*/
    memcpy(WHT_AT24Cxx_Driver.WHT_TRx_Buffer, "666 i2c SOFT hello word is my at24c08 eeprom, new driver code and i2c_bsp\r\n", 76);
    WHT_AT24Cxx_Driver.WHT_Write_Data(AT24C08_7Bit_Addr0, WHT_AT24Cxx_Driver.WHT_TRx_Buffer, 5, AT24Cxx_BUFFER_SIZE-5);
    WHT_AT24Cxx_Driver.WHT_Read_Data(AT24C08_7Bit_Addr0, WHT_AT24Cxx_Driver.WHT_TRx_Buffer, 0, AT24Cxx_BUFFER_SIZE);
    WHT_Debug_Driver.WHT_Printf(WHT_AT24Cxx_Driver.WHT_TRx_Buffer, AT24Cxx_BUFFER_SIZE);
}
