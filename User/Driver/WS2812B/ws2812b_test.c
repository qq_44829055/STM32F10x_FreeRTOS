#include "ws2812b_driver.h"


void WS2812B_TEST_FUNC(void)
{
    WHT_WS2812B_Driver.WHT_Init();

    //uint32_t Send_RGB_Value[] = {RGB_Romantic_Rose, RGB_Elegant_Sky_Blue, RGB_Warm_beige, RGB_Mysterious_Lavender_Color, RGB_Vitality_Orange};
    uint32_t Send_RGB_Value[] = {RGB_Passionate_Coral, RGB_Quiet_light_green, RGB_Warm_Peach_Pink, RGB_Mysterious_Violet, RGB_Vitality_yellow_green};
    for (; ;)
    {
        for (uint8_t index = 0; index < sizeof(Send_RGB_Value) / sizeof(Send_RGB_Value[0]); index++)
        {
            #if 1
            for (size_t i = 0; i < WS2812B_COUNT; i+=4)
            {
                WHT_WS2812B_Driver.WHT_Set_LRGB(i, Send_RGB_Value[index]);
            }
            #else
            WHT_WS2812B_Driver.WHT_Set_All_LRGB(Send_RGB_Value[index]);
            #endif
            WHT_WS2812B_Driver.WHT_Start();
            vTaskDelay(2000 / portTICK_RATE_MS);
        }
    }
}
