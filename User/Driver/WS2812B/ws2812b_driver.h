#ifndef __WS2812B_DRIVER_H__
#define __WS2812B_DRIVER_H__

#include "FreeRTOS.h"
#include "task.h"
#include "../../BSP/Timer_BSP/timer_bsp.h"


#define WS2812B_COUNT      180 //灯个数
#define WS2812B_GRB_BIT    24  //每个灯有24bit的RGB数据
#define WS2812B_CYCLE_NS   1250//0码/1码的周期时间纳秒
#define WS2812B_TIME_0     29  //T0H(32%) = 1.25us * (29 / 90) = 0.40us, T0L(68%) = 1.25 - 0.40 = 0.85us
#define WS2812B_TIME_1     61  //T1H(68%) = 1.25us * (61 / 90) = 0.85us, T1L(32%) = 1.25 - 0.85 = 0.40us


typedef struct
{
	void (*WHT_Init)(void);
	void (*WHT_Set_LRGB)(uint16_t number, uint32_t lrgb_value);
	void (*WHT_Set_All_LRGB)(uint32_t lrgb_value);
	void (*WHT_Start)(void);
}WHT_WS2812B_Driver_t;

extern WHT_WS2812B_Driver_t WHT_WS2812B_Driver;

/*
浪漫红色：#FF1919
RGB值：R: 255, G: 0-50, B: 0-50
描述：这种颜色充满热情与浪漫，适合营造温馨、浓烈或激情的氛围。
宁静蓝色：#1932FF
RGB值：R: 0-50, G: 0-100, B: 255
描述：蓝色通常带来平静、清新和宁静的感觉，适合情侣间放松和享受安静时光。
温馨粉色：#FFAFAF
RGB值：R: 255, G: 150-200, B: 150-200
描述：粉色通常被认为是浪漫和温柔的象征，适合营造柔和舒适的氛围。
神秘紫色：#A419A4
RGB值：R: 128-200, G: 0-50, B: 128-200
描述：紫色可以带来神秘和梦幻的感觉，适合情侣间享受浪漫时刻。
柔和暖黄色：#FFE6B4
RGB值：R: 255, G: 220-240, B: 170-190
描述：暖黄色通常给人温暖和亲切的感觉，适合营造温馨舒适的氛围。

浪漫玫瑰色：#FF3296
RGB值：R: 255, G: 50, B: 150
描述：这种颜色带有深红的浪漫情调，仿佛玫瑰般的温馨与热情，非常适合情侣间的浪漫时刻。
淡雅天蓝色：#64C8FF
RGB值：R: 100, G: 200, B: 255
描述：这种颜色淡雅而清新，像天空般的清澈与宁静，可以营造出一个舒适而放松的氛围。
温馨米黄色：#F0E6B4
RGB值：R: 240, G: 230, B: 180
描述：米黄色带给人温暖和舒适的感觉，适合情侣间在温馨的氛围中度过美好时光。
神秘薰衣草色：#9664C8
RGB值：R: 150, G: 100, B: 200
描述：薰衣草色带有一种神秘而浪漫的气息，可以营造出一种梦幻般的氛围，非常适合情侣间的浪漫之夜。
活力橙色：#FF7800
RGB值：R: 255, G: 120, B: 0
描述：橙色充满活力和热情，可以为情侣间带来一种积极向上的氛围，适合一起享受快乐的时光。

热情珊瑚色：#FF7F50
RGB值：R: 255, G: 127, B: 80
描述：珊瑚色是介于红色和橙色之间的一种色彩，它既有红色的热情，又有橙色的活力，适合营造充满生机和活力的氛围。
宁静浅绿色：#96C896
RGB值：R: 150, G: 200, B: 150
描述：浅绿色是一种清新、宁静的色彩，能够带来一种放松和舒适的感觉，适合情侣间在安静的环境中享受亲密时光。
温馨桃粉色：#FFC8DC
RGB值：R: 255, G: 200, B: 220
描述：桃粉色是一种柔和、甜美的色彩，带有一种浪漫和温馨的气息，适合情侣间在浪漫的氛围中度过美好时光。
神秘紫罗兰色：#8246B4
RGB值：R: 130, G: 70, B: 180
描述：紫罗兰色是一种充满神秘和浪漫气息的色彩，它既有紫色的神秘感，又有粉色的温柔感，适合营造一种梦幻般的氛围。
活力黄绿色：#C8FF64
RGB值：R: 200, G: 255, B: 100
描述：黄绿色是一种充满生机和活力的色彩，它既有绿色的清新感，又有黄色的明亮感，适合情侣间在充满活力的氛围中享受快乐时光
*/

#define RGB_Romantic_Red              0xFF1919//浪漫红色
#define RGB_Quiet_Blue                0x1932FF//宁静蓝色
#define RGB_Warm_Pink                 0xFFAFAF//温馨粉色
#define RGB_Mysterious_Purple         0xA419A4//神秘紫色
#define RGB_Soft_Warm_Yellow          0xFFE6B4//柔和暖黄色

#define RGB_Romantic_Rose             0xFF3296//浪漫玫瑰色
#define RGB_Elegant_Sky_Blue          0x64C8FF//淡雅天蓝色
#define RGB_Warm_beige                0xF0E6B4//温馨米黄色
#define RGB_Mysterious_Lavender_Color 0x9664C8//神秘薰衣草色
#define RGB_Vitality_Orange           0xFF7800//活力橙色

#define RGB_Passionate_Coral          0xFF7F50//热情珊瑚色
#define RGB_Quiet_light_green         0x96C896//宁静浅绿色
#define RGB_Warm_Peach_Pink           0xFFC8DC//温馨桃粉色
#define RGB_Mysterious_Violet         0x8246B4//神秘紫罗兰色
#define RGB_Vitality_yellow_green     0xC8FF64//活力黄绿色

#endif
