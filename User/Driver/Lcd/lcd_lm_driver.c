//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
#include "lcd_lm_driver.h"

static volatile uint32_t* LCD_RST = NULL;//复位

static void WHT_LCD_LM_Driver_GPIO_Init(void)
{
	WHT_GPIO_BSP.WHT_Set_Clock(LCD_RST_PORT, ENABLE);
	WHT_GPIO_BSP.WHT_Config_Bit_Output(LCD_RST_PORT, LCD_RST_PIN, &LCD_RST);
	WHT_GPIO_BSP.WHT_Set_State(LCD_RST_PORT, LCD_RST_PIN, Low);
	WHT_GPIO_BSP.WHT_Set_Mode(LCD_RST_PORT, LCD_RST_PIN, Mode_Out_PP);
}
static const WHT_Timer_PWM_Out_Config_t PWM_config[1] =
{
    {
        .Channel = Timer_CH1,
        .Normal_Polarity = Low,
        .Normal_Idle_Polarity = Hig,
        .Normal_Output_GPIO_Port = LCD_BK_PORT,
        .Normal_Output_GPIO_Pin = LCD_BK_PIN,

        .Complementary_Output_State = DISABLE,
        .Brake_Output_State = DISABLE,
    },
};
static WHT_Timer_Config_t timer_config =
{
    .Name = Timer4,
    .Cycle_ns = 10000,
    .Mode = Timer_Output_Comparison,
    .Config_Count = sizeof(PWM_config) / sizeof(PWM_config[0]),
    .Config = &PWM_config,
};




void WHT_LCD_Backlight_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO ,ENABLE);//重映射使能AFIO时钟
	GPIO_PinRemapConfig(GPIO_Remap_TIM4,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO ,DISABLE);//重映射失能AFIO时钟
	WHT_LCD_LM_Driver_GPIO_Init();
	WHT_Timer_BSP.WHT_Config(&timer_config);
	WHT_Timer_BSP.WHT_Set_Duty_Cycle(&timer_config, PWM_config->Channel, 100);
	WHT_Timer_BSP.WHT_Start(&timer_config);
}
/*设置LCD复位*/
void WHT_LCD_Set_RST(WHT_GPIO_State_enum state)
{
	*LCD_RST = state;
}
/*设置LCD背光*/
void WHT_LCD_Set_Backlight(unsigned char value)
{
	WHT_Timer_BSP.WHT_Set_Duty_Cycle(&timer_config, PWM_config->Channel, value);
}
