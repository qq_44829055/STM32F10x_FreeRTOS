//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.1.1
#ifndef __LCD_LM_DRIVER_H__
#define __LCD_LM_DRIVER_H__

#include "../../BSP/Timer_BSP/timer_bsp.h"
#include "../../BSP/GPIO_BSP/gpio_bsp.h"


//复位引脚
#define  LCD_RST_PORT        PortE
#define  LCD_RST_PIN         Pin1

//背光引脚    
#define  LCD_BK_PORT         PortD
#define  LCD_BK_PIN          Pin12


extern void WHT_LCD_Backlight_Init(void);
extern void WHT_LCD_Set_RST(WHT_GPIO_State_enum state);//设置LCD复位
extern void WHT_LCD_Set_Backlight(unsigned char value);//设置LCD背光


#endif /*__LCD_LM_DRIVER_H__*/
