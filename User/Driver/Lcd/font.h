//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
#ifndef __FONT_ASCII_H__
#define __FONT_ASCII_H__

typedef struct 
{
    char* Name;               //名字
    unsigned short Sum_Size;  //占用大小
    unsigned short Pixel_Size;//字符像素点大小
    unsigned char Start_Value;//起始字符
    unsigned char End_Value;  //结束字符
    unsigned char Width;      //宽度
    unsigned char Height;     //高度
    unsigned char Width_Byte; //宽度字节个数
    unsigned char* Buffer;    //数据
}WHT_Font_Info_t;

extern const WHT_Font_Info_t Font_ASCII_5x8;
extern const WHT_Font_Info_t Font_ASCII_6x12;
extern const WHT_Font_Info_t Font_ASCII_8x16;
extern const WHT_Font_Info_t Font_ASCII_12x24;

#endif /*__FONT_ASCII_H__*/
