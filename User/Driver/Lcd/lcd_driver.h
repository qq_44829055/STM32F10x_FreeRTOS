//备注：拷贝代码请加上作者信息
//作者：王海涛
//邮箱：1126471088@qq.com
//版本：V0.0.2
#ifndef __LCD_DRIVER_H__
#define __LCD_DRIVER_H__

#include "./lcd_lm_driver.h"
#include "./lcd_xpt2046.h"
#include "../../BSP/FSMC_BSP/fsmc8080_bsp.h"
#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../Delay/delay.h"
#include "font.h"
#include <string.h>


/*开启任务延迟，特别大批量数据发送*/
#ifndef LCD_Task_Delay
#define LCD_Task_Delay
#endif // !LCD_Task_Delay

/*屏幕分辨率*/
#define LCD_SCREEN_Wide    320
#define LCD_SCREEN_High    240

/*缓存大小设置*/
#define LCD_Buffer_Size    320

/****************************颜色************************/
#define RGB565_RED       ((uint16_t)0xF800)    //红色
#define RGB565_MAROON    ((uint16_t)0x7800)    //深红色
#define RGB565_MAGENTA   ((uint16_t)0xF81F)    //品红
#define RGB565_YELLOW    ((uint16_t)0xFFE0)    //黄色
#define RGB565_GREEN     ((uint16_t)0x07E0)    //绿色
#define RGB565_DGREEN    ((uint16_t)0x03E0)    //深绿色
#define RGB565_OLIVE     ((uint16_t)0x7BE0)    //橄榄绿
#define RGB565_BLUE      ((uint16_t)0x001F)    //蓝色
#define RGB565_NAVY      ((uint16_t)0x000F)    //深蓝色
#define RGB565_CYAN      ((uint16_t)0x07FF)    //青色
#define RGB565_DCYAN     ((uint16_t)0x03EF)    //深青色
#define RGB565_PURPLE    ((uint16_t)0x780F)    //紫色
#define RGB565_BLACK     ((uint16_t)0x0000)    //黑色
#define RGB565_DGRAY     ((uint16_t)0x7BEF)    //深灰色
#define RGB565_WHITE     ((uint16_t)0xFFFF)    //白色
#define RGB565_LGRAY     ((uint16_t)0xC618)    //灰白色
/****************************颜色************************/

/*命令*/
typedef enum
{
    LCD_Read_Pixel = 0x0C,//读取像素格式
    LCD_Read_ID    = 0xD3,//读取ID
    LCD_Set_Pos_X  = 0x2A,//设置X轴
    LCD_Set_Pos_Y  = 0x2B,//设置Y轴
    LCD_Fill_Pixel = 0x2C,//填数据
}WHT_LCD_Cmd_enum;

/*显示模式*/
typedef enum
{
    Display_Mode0 = 0x00,
    Display_Mode1 = 0x01,
    Display_Mode2 = 0x02,
    Display_Mode3 = 0x03,
    Display_Mode4 = 0x04,
    Display_Mode5 = 0x05,//标准横屏
    Display_Mode6 = 0x06,
    Display_Mode7 = 0x07,
}WHT_LCD_Display_Mode_enum;

/*屏幕参数*/
typedef struct
{
    const uint16_t X_Length;//屏幕x轴水平长度
    const uint16_t Y_Length;//屏幕y轴垂直长度
    WHT_Font_Info_t* Font;  //字体选择
    uint16_t New_Pos_X;     //最新x水平坐标
    uint16_t New_Pos_Y;     //最新y垂直坐标
    uint16_t BK_Color;      //字体背景色
    uint16_t Font_Color;    //字体颜色
}WHT_LCD_Info_t;

typedef struct
{
    volatile WHT_LCD_Info_t* WHT_LCD_Info;                      //屏幕信息
    void (*WHT_LCD_Init)(void);                                 //初始化
    void (*WHT_LCD_Set_Display_Mode)(WHT_LCD_Display_Mode_enum display_mode);//设置扫描方向
    /*自带ASCII字体*/
    uint8_t(*WHT_LCD_Set_Font_Row)(uint16_t row, uint16_t pos_x);//指定字体显示在那行row最小0 pos_x最小0
    void (*WHT_LCD_Clear_Row)(uint8_t row);                      //清空某行字体
    void (*WHT_LCD_Puts)(const char* s);                         //ASCII字符显示
    /*颜色填充*/
    void (*WHT_LCD_Fill_Color)(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, const uint16_t* intput_buf);//区域填充颜色
    void (*WHT_LCD_Get_Reg_Value)(uint16_t reg, uint16_t* output_buf, uint32_t count);//获取寄存器值
}WHT_LCD_Driver_t;

#ifdef LCD_Task_Delay
#include "FreeRTOS.h"
#include "task.h"
#endif // LCD_Task_Delay

extern const WHT_LCD_Driver_t WHT_LCD_Driver;

#endif /*__LCD_DRIVER_H__*/
