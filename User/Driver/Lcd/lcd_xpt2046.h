#ifndef __LCD_XPT2046_H__
#define __LCD_XPT2046_H__

#include "../../BSP/GPIO_BSP/gpio_bsp.h"
#include "../../../Tool/Soft_SPI/soft_spi_bus.h"

/*XPT2046触摸屏引脚*/
#define SPI_CS_Port         PortD
#define SPI_CS_Pin          Pin13 
#define SPI_IRQ_Port        PortE
#define SPI_IRQ_Pin         Pin4


#define XPT2046_CHANNEL_X   0x90 //通道Y+的选择控制字	
#define XPT2046_CHANNEL_Y   0xd0 //通道X+的选择控制字
//触屏信号有效电平
#define XPT2046_IRQ_Enable  Low

#define TOUCH_PRESSED 		Hig
#define TOUCH_NOT_PRESSED	Low

//触摸消抖计数器阈值
#define Touch_Filter_Count  2

typedef enum
{
	XPT2046_STATE_RELEASE = 0,//触摸释放
	XPT2046_STATE_WAITING,	  //触摸按下
	XPT2046_STATE_PRESSED,	  //触摸按下
}Touch_State_enum;

/*屏幕坐标*/
typedef struct
{
    uint8_t State;
    uint16_t New_X;//最新的触摸值
    uint16_t New_Y;
}WHT_XPT2046_Pos_t;/*负数值表示无新数据*/

extern WHT_XPT2046_Pos_t WHT_XPT2046_Pos;
extern void WHT_LCD_XPT2046_Init(void);
extern void WHT_XPT2046_Touch_Even_Handler(uint8_t display_mode);
extern uint8_t WHT_XPT2046_Touch_Calibrate(uint8_t display_mode);

typedef struct         //校准系数结构体（最终使用）
{
	float dX_X;
	float dX_Y;
	float dX;
	float dY_X;
	float dY_Y;
	float dY;
}XPT2046_Touch_Parameters_t;

#endif
