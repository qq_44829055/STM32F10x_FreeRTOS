
//软定时器1句柄
static TimerHandle_t timer1_Handle = NULL;
//软定时器2句柄
static TimerHandle_t timer2_Handle = NULL;

static void timer1_hook(void* parameter);
static void timer2_hook(void* parameter);


//app启动任务句柄
static TaskHandle_t AppTaskStart_Handle = NULL;
//管理任务句柄
static TaskHandle_t Admin_Task_Handle = NULL;
//Debug任务句柄
static TaskHandle_t WHT_Debug_Task_Handle = NULL;

static void App_Start_Task(void* parameter);//启动其它任务的启动任务
static void Admin_Task(void* parameter);    //管理其它任务的管理任务
static void WHT_Deubg_Task(void* parameter);

int main(void)
{    
    printf("创建App_Start_Task任务,从而来创建其它工程任务\r\n");
    xTaskCreate((TaskFunction_t)App_Start_Task,   //任务函数
				(char *        )"App_Start_Task",  //任务名字
				(uint16_t      )128,               //任务堆栈大小
				(void *        )NULL,              //任务传参
				(UBaseType_t   )1,                 //任务优先级
				(TaskHandle_t *)&AppTaskStart_Handle);//任务控制块
    vTaskStartScheduler();
    while(1);
}
static void App_Start_Task(void *parameter)
{
    BaseType_t xreturn;

    printf("进入临界区保护\r\n");
    taskENTER_CRITICAL();

    printf("创建软定时器1,周期性20tick\r\n");
    timer1_Handle =xTimerCreate("Timer1 code",  // Just a text name, not used by the kernel.
                               (TickType_t)20,  // The timer period in ticks.
                               pdTRUE,          // The timers will auto-reload themselves when they expire.
                               (void*)6,        // Assign each timer a unique id equal to its array index.
                               timer1_hook      // Each timer calls the same callback when it expires.
                               );
    if(timer1_Handle != NULL)
        printf("软定时器1创建完成\r\n");
    printf("**************************************************\r\n");

    printf("创建软定时器2,单周期2000tick\r\n");
    timer2_Handle =xTimerCreate("Timer2 code",(TickType_t)2000,pdFALSE,(void*)2,timer2_hook);
    if(timer2_Handle != NULL)
        printf("软定时器2创建完成\r\n");
    printf("**************************************************\r\n");

    printf("创建Admin_Task任务管理器\r\n");
    xreturn =xTaskCreate((TaskFunction_t)Admin_Task,
					    (char *        )"Admin_Task",
					    (uint16_t      )128,
					    (void *        )NULL,
					    (UBaseType_t   )14,
					    (TaskHandle_t *)&Admin_Task_Handle);
    if(xreturn == pdPASS)//任务创建成功
        printf("Admin_Task创建完成\r\n");
    printf("**************************************************\r\n");

    vTaskDelete(AppTaskStart_Handle);//删除启动任务
    printf("Delete App_Start_Task\r\n");
    taskEXIT_CRITICAL();//退出临界区
}

static void Admin_Task(void* parameter)
{
    BaseType_t xreturn;

    xreturn = xTimerStart(timer1_Handle,0);
    if (xreturn == pdPASS)
        printf("定时器1立即启动\r\n");
    
    for(;;)//任务通知代替发送时间信号量
    {
        vTaskDelay(1500);
    }
}

static void WHT_Deubg_Task(void* parameter)
{
    uint32_t task_value;
    uint8_t* receive_buff = NULL;
    uint32_t receive_length;

    WHT_Delay_ms(3000);
    for (;;)
    {
        task_value = ulTaskNotifyTake(pdPASS,portMAX_DELAY);//退出函数时清除任务通知值
        if(task_value == pdTRUE)
        {
            WHT_Debug_Device.WHT_Gets(&receive_buff,&receive_length);
            switch (receive_buff[0])
            {
            case 'A':;break;
            case 'B':;break;
            case 'C':;break;
            default:
                printf("命令=%c 不存在\r\n",receive_buff[0]);
                break;
            }
        }
        else
            printf("led1 receive error\r\n");
        WHT_Debug_Device.WHT_Puts((const char*)receive_buff,receive_length);//原样发出
    }
}


static void timer1_hook(void* parameter)
{
    WHT_KEY_Driver_Scan();//按键扫描函数
    if (WHT_Debug_Device.WHT_Receive_Over_State() == SET)
        xTaskNotifyGive(WHT_Debug_Task_Handle);//通知Debug任务,开始处理接收到的数据
}
static void timer2_hook(void* parameter)
{
    //xTimerReset(timer2_Handle,0);//重新按照之前配置的时间再次装载一次
}
